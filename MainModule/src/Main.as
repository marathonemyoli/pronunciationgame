package
{

import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.models.GlobalParameters;
import com.emyoli.phonics.views.BaseView;
import com.emyoli.phonics.views.map.MapView;

import flash.desktop.NativeApplication;
import flash.display.Sprite;
import flash.display.StageDisplayState;
import flash.events.Event;
import flash.geom.Rectangle;
import flash.system.Capabilities;

import org.gestouch.core.Gestouch;
import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
import org.gestouch.extensions.starling.StarlingTouchHitTester;
import org.gestouch.input.NativeInputAdapter;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.events.Event;
import starling.textures.RenderTexture;

import starling.utils.RectangleUtil;
import starling.utils.ScaleMode;

import starling.utils.SystemUtil;

[SWF(width="540", height="960", frameRate="60", backgroundColor="#000000")]
public class Main extends Sprite
{
	private const MINIMAL_HD_ASSETS_SCREEN_WIDTH : Number = 800;


	private var mStarling:Starling;

	public function Main()
	{
		// We develop the game in a *fixed* coordinate system of 720*1280. The game might
		// then run on a device with a different resolution; for that case, we zoom the
		// viewPort to the optimal size for any display and load the optimal textures.
		stage.displayState = StageDisplayState.FULL_SCREEN;
		Starling.handleLostContext = true;
		var iOS:Boolean = SystemUtil.platform == "IOS";
		var screenSize:Rectangle = new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);

		GlobalParameters.StageWidth /= (screenSize.height / screenSize.width) / (GlobalParameters.StageHeight / GlobalParameters.StageWidth);

		var stageSize:Rectangle  = new Rectangle(0, 0, GlobalParameters.StageWidth, GlobalParameters.StageHeight);
		var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.SHOW_ALL, iOS);

		GlobalParameters.ASSET_SCALE_FACTOR = viewPort.width <= MINIMAL_HD_ASSETS_SCREEN_WIDTH ? 1 : 2;

		Starling.multitouchEnabled = true; // useful on mobile devices
		Starling.handleLostContext = true; // recommended everywhere when using AssetManager
		RenderTexture.optimizePersistentBuffers = iOS; // safe on iOS, dangerous on Android

		BaseView.HOME_BUTTON_DEFAULT_VIEW = MapView;

		mStarling = new Starling(MainView, stage, viewPort, null, "auto", "auto");
		mStarling.stage.stageWidth    = GlobalParameters.StageWidth;  // <- same size on all devices!
		mStarling.stage.stageHeight   = GlobalParameters.StageHeight; // <- same size on all devices!
		mStarling.enableErrorChecking = Capabilities.isDebugger;
		mStarling.simulateMultitouch  = false;
		mStarling.addEventListener(starling.events.Event.ROOT_CREATED, function():void
		{
			mStarling.removeEventListener(starling.events.Event.ROOT_CREATED, arguments.callee);
		});


		Gestouch.inputAdapter = new NativeInputAdapter(stage);
		Gestouch.addDisplayListAdapter(starling.display.DisplayObject, new StarlingDisplayListAdapter());
		Gestouch.addTouchHitTester(new StarlingTouchHitTester(mStarling), -1);


		mStarling.start();

		GlobalParameters.SCALE = screenSize.width / GlobalParameters.StageWidth;

		// When the game becomes inactive, we pause Starling; otherwise, the enter frame event
		// would report a very long 'passedTime' when the app is reactivated.

		if (!SystemUtil.isDesktop)
		{
			NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.ACTIVATE, function (e:*):void { mStarling.start(); });
			NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.DEACTIVATE, function (e:*):void { mStarling.stop(true); });
		}
    }
}
}
