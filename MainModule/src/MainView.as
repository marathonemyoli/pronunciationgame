package {
import com.creativebottle.starlingmvc.beans.Bean;
import com.creativebottle.starlingmvc.commands.Command;
import com.emyoli.phonics.commands.ShowInvitedMessageCommand;
import com.emyoli.phonics.controllers.ConfigController;
import com.emyoli.phonics.controllers.DelayedMessageController;
import com.emyoli.phonics.controllers.FixViewManager;
import com.emyoli.phonics.controllers.LocalizationController;
import com.emyoli.phonics.controllers.SoundController;
import com.emyoli.phonics.models.GlobalParameters;

import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.GetOnlinePlayers;
import com.emyoli.phonics.net.MyScoreBoard;
import com.emyoli.phonics.net.OnlinePlayers;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.controllers.MainController;
import com.emyoli.phonics.controllers.NavigationController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.factories.BeanInstanceFactory;
import com.emyoli.phonics.net.PronunciationService;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.renderers.FriendRendererPM;
import com.emyoli.phonics.utils.SharedObjectManager;
import com.emyoli.phonics.views.*;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.GameSetting;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.views.message.MessagePopupPM;
import com.emyoli.phonics.views.score.ScoreViewPM;
import com.emyoli.phonics.views.avatarSelection.AvatarSelectionViewPM;
import com.emyoli.phonics.views.friendsSelection.FriendsSelectionViewPM;
import com.emyoli.phonics.views.gameSettings.GameSettingsViewPM;
import com.emyoli.phonics.views.login.LoginViewPM;
import com.emyoli.phonics.views.map.MapViewPM;
import com.emyoli.phonics.views.settings.MainSettingsPM;
import com.emyoli.phonics.views.signup.SignupViewPM;

import flash.text.Font;

import starling.core.Starling;

import starling.display.DisplayObject;

import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class MainView extends Sprite
{
	import com.creativebottle.starlingmvc.StarlingMVC;
	import com.creativebottle.starlingmvc.config.StarlingMVCConfig;
	import com.creativebottle.starlingmvc.views.ViewManager;

	import starling.display.Sprite;

	[Embed(source="assets/fonts/HoltwoodOneSC.ttf",
			fontName = "Holtwood",
			fontFamily = "Holtwood",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
	public static const HoltwoodRegular : Class;

	[Embed(source="assets/fonts/ProximaNova-Bold.otf",
			fontName = "Proxima Nova",
			fontFamily = "Proxima Nova",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
	public static const ProximaNovaBold : Class;

	[Embed(source="assets/fonts/Rene Bieder - Quadon UltraBold.otf",
			fontName = "Quadon",
			fontFamily = "Quadon",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
	public static const QuadonUltraBold : Class;

	[Inject]
	public var netConnector : NetConnector;

	public var starlingMVC : StarlingMVC;

	public function MainView()
	{
		Font.registerFont(HoltwoodRegular);
		Font.registerFont(ProximaNovaBold);
		Font.registerFont(QuadonUltraBold);

		var config:StarlingMVCConfig = new StarlingMVCConfig();
		config.eventPackages = ["com.emyoli.phonics.events",
								"com.emyoli.phonics.net"];
		config.viewPackages = [
			"com.emyoli.phonics.views.avatarSelection",
			"com.emyoli.phonics.views.avatarSelectionPopup",
			"com.emyoli.phonics.views.friendsSelection",
			"com.emyoli.phonics.views.score",
			"com.emyoli.phonics.views.login",
			"com.emyoli.phonics.views.signup",
			"com.emyoli.phonics.views.map",
			"com.emyoli.phonics.views.settings",
			"com.emyoli.phonics.views.help",
			"com.emyoli.phonics.views.game3",
		];

		var beans:Array = [
			new AssetController(),
			new Bean(new GameModelRepo(), "gameModel"),
			new UserSetting(),
			new GameSetting(),

			new GetOnlinePlayers(),
			new OnlinePlayers(),
			new MyScoreBoard(),

            new DelayedMessageController(),
			new MainController(),
			new	AtlasParser(),
			new NetConnector(),
			new FixViewManager(this),
			new NavigationController(),
			new SharedObjectManager(),
			new LocalizationController(),
            new SoundController(),
			new ConfigController(),
			new PronunciationService(Starling.current.nativeStage),

			new Command("" + CmdType.InvitedToGame, ShowInvitedMessageCommand),

			new ScoreViewPM(),
			new LoginViewPM(),
			new SignupViewPM(),
			new FriendsSelectionViewPM(),
			new AvatarSelectionViewPM(),
			new MapViewPM(),
			new GameSettingsViewPM,
			new MainSettingsPM(),
			new MessagePopupPM(),
			new FriendRendererPM(),
            new Game3ViewPM(),
			new BeanInstanceFactory(),
			this
		];

		starlingMVC = new StarlingMVC(this, config, beans);

		GlobalParameters.test = starlingMVC;

		addEventListener(TouchEvent.TOUCH, handleTouch);
	}

	private function handleTouch(e:TouchEvent) : void
	{
		var touches : Vector.<Touch> = e.getTouches(this);
		var clicked : DisplayObject = e.currentTarget as DisplayObject;
		if (touches.length == 1)
		{
			var touch : Touch = touches[0];
			var event : GameTouchEvent
			if (touch.phase == TouchPhase.ENDED)
			{
				event = GameTouchEvent.clickFromTouchEvent(e);
				e.target.dispatchEvent(event);
			}

			if (touch.phase == TouchPhase.BEGAN)
			{
				event = GameTouchEvent.downFromTouchEvent(e);
				e.target.dispatchEvent(event);
			}
		}
	}


}
}
