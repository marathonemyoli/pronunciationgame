package com.emyoli.phonics.controllers {
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.utils.SharedObjectManager;
import com.emyoli.phonics.views.Game3View;
import com.emyoli.phonics.views.login.LoginViewPM;
import com.emyoli.phonics.views.signup.SignupView;

import starling.events.EventDispatcher;

public class MainController {

	[Inject]
	public var netConnector : NetConnector;

	[Inject]
	public var storage : SharedObjectManager;

	[Inject]
	public var loginPM : LoginViewPM;

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[EventHandler(event="ASSETS_LOADED")]
	public function handleAssetsLoaded(event : GameEvent) : void
	{
		//dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, GameSettingsView));
		netConnector.connect();
	}

	[EventHandler(event="SERVER_CONNECTED")]
	public function handleConnected(event : GameEvent) : void
	{
		var username : String = storage.getData("username") as String;
		var password : String = storage.getData("password") as String;
		if (username && password)
		{
			loginPM.login(username, password);
		}
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, SignupView));
//		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, Game3View));
	}
}
}
