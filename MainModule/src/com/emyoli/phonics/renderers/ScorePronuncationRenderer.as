/**
 * Created by alekseykabanov on 08.09.15.
 */
package com.emyoli.phonics.renderers {
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.models.IListRenderer;
import com.emyoli.phonics.net.MyScoreBoardInfo;

import org.osmf.layout.HorizontalAlign;

import starling.display.Sprite;
import starling.text.TextField;

public class ScorePronuncationRenderer extends Sprite implements IListRenderer {
    public static var MAX_HEIGHT : Number = 175;

    [Inject]
    public var assetController : AssetController;

    private var _id : int = 0;

    private var _nameLabel : TextField;

    private var _initialized = false;

    private var _divider : Sprite;
    private var _scoreGoldTextField : TextField;

    private var _name : String = "";
    private var _score : String = "";

    public function ScorePronuncationRenderer() : void
    {
        super();
    }

    public function set model(value : Object) : void
    {
        _name = value.name;
        _score = value.score;
        clear();
        refresh();
    }


    public function init() : void
    {
        initScore();
        initLabel();
        _initialized = true;
    }

    public function draw() : void
    {
        _divider = assetController.getSpriteCloneFrom("Friends", "FriendDivider", true, true);

        addChild(_divider);
    }

    private function initScore() : void
    {
        const gap : Number = 35;

        _scoreGoldTextField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 2.2, 400, MAX_HEIGHT, 0xFEC500, AssetController.FONT_QUADON, true);
        _scoreGoldTextField.nativeFilters = [AssetController.BUTTON_LABEL_SHADOW_FILTER, AssetController.BUTTON_LABEL_GLOW_FILTER];

        _scoreGoldTextField.x = 300;
        //_scoreGoldTextField.y = (MAX_HEIGHT - _scoreGoldTextField.height) / 2;
        _scoreGoldTextField.hAlign = HorizontalAlign.RIGHT

        addChild(_scoreGoldTextField);
    }

    private function initLabel() : void
    {
        _nameLabel = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 3, 225,
                MAX_HEIGHT, 0xFFFFFF, AssetController.FONT_NOVA);
        _nameLabel.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];

        _nameLabel.x = 20;

        addChild(_nameLabel);
    }

    /**
     * Refresh data in renderer
     */
    private function refresh() : void
    {
        if (!_initialized)
        {
            init();
        }
        refreshScore();
        refreshLabel();
    }

    /**
     * Refresh score value
     */
    private function refreshScore() : void
    {
        _scoreGoldTextField.text = "" + _score;
    }

    /**
     * Refresh data in name Label
     */
    private function refreshLabel() : void
    {
        _nameLabel.text = "" + _name;
    }

    /**
     * Prepare to load next model
     */
    private function clear() : void
    {

    }

    public function get id() : int
    {
        return _id;
    }

    public function set id(value : int) : void
    {
        _id = value;
    }

}
}
