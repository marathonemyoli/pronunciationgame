package com.emyoli.phonics.renderers
{
import com.emyoli.phonics.components.*;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.factories.BeanInstanceFactory;
import com.emyoli.phonics.models.IListRenderer;
import com.emyoli.phonics.net.AvailableUser;
import com.emyoli.phonics.net.InviteState;
import com.emyoli.phonics.net.Status;
import com.emyoli.phonics.net.UserInfo;
import com.emyoli.phonics.utils.Utils;
import com.netease.protobuf.Int64;

import starling.display.Sprite;
import starling.text.TextField;


/**
 * Visualization for Friend model
 * @author Oleg Knaus
 */
public class FriendRenderer extends Sprite implements IListRenderer {
	public static var MAX_HEIGHT : Number = 175;
	public static var PADDING : Number = 0;

	private const STATUS_TEXTS : Array = ["", "INVITE", "INVITED", "ACCEPTED", "REJECTED"];

	[Inject]
	public var assetController : AssetController;

    [Inject]
    public var factory : BeanInstanceFactory;

    [Inject]
	public var pm : FriendRendererPM;

	private var _model : AvailableUser;

	private var _id : int = 0;

	private var _picture : Sprite;
	private var _nameLabel : TextField;

	private var _width : Number;
	private var _height : Number;

	private var _initialized = false;

	private var _acceptedButton : Sprite;
	private var _inviteButton : TwoStateButton;
	private var _rejectedButton : Sprite;
	private var _avatarShadow : Sprite;
	private var _divider : Sprite;
	private var _avatar : Sprite;
	private var _buttonTextField : TextField;

	public function FriendRenderer() : void
	{
		super();
	}

	public function set model(value : Object) : void
	{
		if (_model == value)
		{
			return;
		}
		if (_model != null)
		{
			clear();
		}
		_model = new AvailableUser();
		_model.data = value as UserInfo;
		_model.status = Status.NOTINVITED;
		refresh();
	}


	public function init() : void
	{
		initPic();
		initButton();
		initLabel();
		_initialized = true;
	}

	public function draw() : void
	{
		const viewId : String = "Friends";

		_acceptedButton = assetController.getSpriteCloneFrom(viewId, "FriendAcceptedButton", false, true);

		const inviteUp : Sprite = assetController.getSpriteCloneFrom(viewId, "FriendInviteButton", false, true);

		_inviteButton = factory.getBeanInstance(TwoStateButton) as TwoStateButton;
		_inviteButton.init(inviteUp);

		_rejectedButton = assetController.getSpriteCloneFrom(viewId, "FriendRejectedButton", false, true);
		_avatarShadow = assetController.getSpriteCloneFrom("Avatar", "AvatarSelector", true);
		_divider = assetController.getSpriteCloneFrom(viewId, "FriendDivider", true, true);

		addChild(_avatarShadow);
		addChild(_divider);
		addChild(_rejectedButton);
		addChild(_acceptedButton);
		addChild(_inviteButton);

		_inviteButton.addEventListener(GameTouchEvent.CLICK, handleInviteClick);
	}
	
	private function initButton() : void
	{
		const gap : Number = 35;

		_buttonTextField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 0.75, _acceptedButton.width - gap,
				_acceptedButton.height, 0xFFFFFF, AssetController.FONT_HOLTWOOD, true);
		_buttonTextField.nativeFilters = [AssetController.BUTTON_LABEL_SHADOW_FILTER, AssetController.BUTTON_LABEL_BEVEL_FILTER,
									AssetController.BUTTON_LABEL_GLOW_FILTER];

		addChild(_buttonTextField);

		_buttonTextField.x = _acceptedButton.x + gap;
		_buttonTextField.y = _acceptedButton.y;
	}

	private function initLabel() : void
	{
		_nameLabel = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1.4, 225,
				MAX_HEIGHT, 0xFFFFFF, AssetController.FONT_NOVA);
		_nameLabel.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];

		_nameLabel.x = 200;

		addChild(_nameLabel);
	}

	private function initPic() : void
	{
		//Nothing
	}

	/**
	 * Refresh data in renderer (picture and label)
	 */
	private function refresh() : void
	{
		trace("_initialized " + _initialized);
		if (!_initialized)
		{
			init();
		}
		refreshPic();
		refreshButton();
		refreshLabel();
	}
	
	/**
	 * Reload picture
	 */
	private function refreshPic() : void
	{
		if (_avatar)
		{
			removeChild(_avatar);
		}

		_avatar = assetController.getSpriteCloneFrom("Avatar", _model.data.avatarname);
		if (!_avatar)
		{
			_avatar = assetController.getSpriteCloneFrom("Avatar", "Avatar1");
		}
		addChild(_avatar);
		_avatar.width = 145;
		_avatar.height = 145;
		_avatarShadow.width = 175;
		_avatarShadow.height = 175;
		_avatar.x = 15;
		_avatar.y = 15;
	}
	
	/**
	 * Refresh data in name Button
	 */
	private function refreshButton() : void
	{
		_buttonTextField.text = STATUS_TEXTS[_model.status];
		_acceptedButton.visible = _model.status == Status.CONFIRMED;
		_inviteButton.visible = _model.status == Status.NOTINVITED;
		_rejectedButton.visible = _model.status == Status.REJECTED;
	}

	/**
	 * Refresh data in name Label
	 */
	private function refreshLabel() : void
	{
		_nameLabel.text = _model.data.nickname;
	}
	
	/**
	 * Prepare to load next model
	 */
	private function clear() : void
	{

	}

	public function get id() : int
	{
		return _id;
	}

	public function set id(value : int) : void
	{
		_id = value;
	}

	[PreDestroy]
	public function handleDestroy () : void
	{
		if (_inviteButton)
		{
			_inviteButton.removeEventListener(GameTouchEvent.CLICK, handleInviteClick);
		}
	}

	private function handleInviteClick(event : GameTouchEvent) : void
	{
		pm.inviteFriend(_model.data.id);
	}

	[EventHandler (event="CmdType.InviteState", properties="data")]
	public function handleFriendConfirmed (data : InviteState) : void
	{
		if (data.invitedID == _model.data.id)
		{
			_model.status = data.status;
			refreshButton();
		}

	}

}

}