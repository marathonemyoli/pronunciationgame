package com.emyoli.phonics.renderers {
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.InviteToGame;
import com.emyoli.phonics.net.InvitedToGame;
import com.emyoli.phonics.net.NetConnector;
import com.netease.protobuf.Int64;

public class FriendRendererPM {

	[Inject]
	public var connector : NetConnector;

	[Inject (source="gameModel")]
	public var gameModel : GameModelRepo;

	public function FriendRendererPM()
	{
	}

	public function inviteFriend(id : Int64) : void
	{
		const message : InviteToGame = new InviteToGame();
		message.invitedID = id;
		message.inviterID = gameModel.userSetting.id;

		connector.sendMessage(message, CmdType.InviteToGame);
	}
}
}
