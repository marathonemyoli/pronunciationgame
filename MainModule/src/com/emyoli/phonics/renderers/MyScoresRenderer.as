package com.emyoli.phonics.renderers
{
import com.emyoli.phonics.components.*;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.models.IListRenderer;
import com.emyoli.phonics.net.AvailableUser;
import com.emyoli.phonics.net.GameType;
import com.emyoli.phonics.net.MyScoreBoardInfo;
import com.emyoli.phonics.net.Status;
import com.emyoli.phonics.net.UserInfo;
import com.emyoli.phonics.utils.Utils;

import org.osmf.layout.HorizontalAlign;

import starling.display.Sprite;
import starling.text.TextField;


/**
 * Visualization for myscores model
 * @author Oleg Knaus
 */
public class MyScoresRenderer extends Sprite implements IListRenderer {
	public static var MAX_HEIGHT : Number = 175;

	[Inject]
	public var assetController : AssetController;

	private var _model : MyScoreBoardInfo;

	private var _id : int = 0;

	private var _nameLabel : TextField;

	private var _initialized = false;

	private var _divider : Sprite;
	private var _scoreGoldTextField : TextField;

	public function MyScoresRenderer() : void
	{
		super();
	}

	public function set model(value : Object) : void
	{
		if (_model == value)
		{
			return;
		}
		if (_model != null)
		{
			clear();
		}
		_model = value as MyScoreBoardInfo;
		refresh();
	}


	public function init() : void
	{
		initScore();
		initLabel();
		_initialized = true;
	}

	public function draw() : void
	{
		_divider = assetController.getSpriteCloneFrom("Friends", "FriendDivider", true, true);

		addChild(_divider);
	}
	
	private function initScore() : void
	{
		const gap : Number = 35;

		_scoreGoldTextField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1.4, 300, MAX_HEIGHT, 0xFEC500, AssetController.FONT_HOLTWOOD, true);
		_scoreGoldTextField.nativeFilters = [AssetController.BUTTON_LABEL_SHADOW_FILTER, AssetController.BUTTON_LABEL_GLOW_FILTER];

		_scoreGoldTextField.x = 370;
		_scoreGoldTextField.y = (MAX_HEIGHT - _scoreGoldTextField.height) / 2;
		_scoreGoldTextField.hAlign = HorizontalAlign.RIGHT

		addChild(_scoreGoldTextField);
	}

	private function initLabel() : void
	{
		_nameLabel = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1.4, 225,
				MAX_HEIGHT, 0xFFFFFF, AssetController.FONT_NOVA);
		_nameLabel.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];

		_nameLabel.x = 20;

		addChild(_nameLabel);
	}

	/**
	 * Refresh data in renderer
	 */
	private function refresh() : void
	{
		if (!_initialized)
		{
			init();
		}
		refreshScore();
		refreshLabel();
	}
	
	/**
	 * Refresh score value
	 */
	private function refreshScore() : void
	{
		_scoreGoldTextField.text = "" + _model.score;
	}

	/**
	 * Refresh data in name Label
	 */
	private function refreshLabel() : void
	{
		_nameLabel.text = "" + _model.gametype;
	}
	
	/**
	 * Prepare to load next model
	 */
	private function clear() : void
	{

	}

	public function get id() : int
	{
		return _id;
	}

	public function set id(value : int) : void
	{
		_id = value;
	}
}

}