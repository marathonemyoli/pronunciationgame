package com.emyoli.phonics.messages {
import com.netease.protobuf.Message;

public class SendRequestMessage {

	private var _data : Message;
	private var _commandType : int;

	public function SendRequestMessage()
	{
	}

	public static function getMessage (data : Message, commandType : int) : SendRequestMessage
	{
		const message : SendRequestMessage = new SendRequestMessage();

		message._data = data;
		message._commandType = commandType;

		return message;
	}

	public function get data() : Message
	{
		return _data;
	}

	public function get commandType() : int
	{
		return _commandType;
	}
}
}
