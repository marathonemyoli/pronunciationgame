package com.emyoli.phonics.views.friendsSelection {
import com.creativebottle.starlingmvc.events.BeanEvent;
import com.creativebottle.starlingmvc.events.BeanEvent;
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.AvailableFriendsList;
import com.emyoli.phonics.net.AvailableUser;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.GameCreate;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.net.GetAvailableFriendsList;
import com.emyoli.phonics.net.GetAvatarName;
import com.emyoli.phonics.net.GetOnlinePlayers;
import com.emyoli.phonics.net.InviteState;
import com.emyoli.phonics.net.InvitedToGame;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.net.OnlinePlayers;
import com.emyoli.phonics.net.ResponseToInvite;
import com.emyoli.phonics.net.SetAvatarName;
import com.emyoli.phonics.net.Signup;
import com.emyoli.phonics.net.Status;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.views.map.MapView;
import com.netease.protobuf.Int64;

import starling.events.EventDispatcher;

public class FriendsSelectionViewPM {

	private const DEFAULT_AVATAR_NAME : String = "avatar1";

	[Inject]
	public var netConnector : NetConnector;

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[Inject (source="gameModel")]
	public var gameModel : GameModelRepo;

	public var onlinePlayers : OnlinePlayers;

	private var agreedFriends : Vector.<Number> = new Vector.<Number>();

	public function FriendsSelectionViewPM()
	{
	}

	public function start() : void
	{
		//dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, MapView));

		/*var message : GameCreate = new GameCreate();
		message.invitedPlayers = new Array();
		for each (var player : AvailableUser in onlinePlayers)
		{
			if (agreedFriends.indexOf(player.data.id.toNumber()) != -1)
			{
				message.invitedPlayers.push(player.data);
			}
		}

		agreedFriends = new <Number>[];

		message.type = gameModel.gameSetting.type;

		netConnector.sendMessage(message, CmdType.GameCreate);*/
	}

	[ViewAdded]
	public function handleViewAdded (view : FriendsSelectionView) : void
	{
		netConnector.sendMessage(new GetOnlinePlayers(), CmdType.GetOnlinePlayers);
	}

	[EventHandler (event="CmdType.InviteState", properties="data")]
	public function handleFriendConfirmed (data : InviteState) : void
	{
		if (data.status == Status.CONFIRMED)
		{
			agreedFriends.push(data.invitedID.toNumber());		}


	}

}
}
