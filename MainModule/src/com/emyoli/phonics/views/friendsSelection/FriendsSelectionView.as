package com.emyoli.phonics.views.friendsSelection
{
import com.emyoli.phonics.TestData;
import com.emyoli.phonics.components.BaseListComponent;
import com.emyoli.phonics.net.AvailableFriendsList;
import com.emyoli.phonics.net.GetAvailableFriendsList;
import com.emyoli.phonics.net.OnlinePlayers;
import com.emyoli.phonics.renderers.FriendRenderer;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.views.avatarSelection.*;
import com.emyoli.phonics.views.signup.*;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.views.login.*;
import com.emyoli.phonics.views.*;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.utils.NativeText;

import flash.events.MouseEvent;

import starling.core.Starling;

import starling.display.Button;
import starling.display.Sprite;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

import starling.text.TextField;

public class FriendsSelectionView extends BaseView
{
	[Inject]
	public var pm : FriendsSelectionViewPM;

	public var friendList : BaseListComponent;

	private var _startButton : Sprite;
	private var _footer : Sprite;

	public function FriendsSelectionView()
	{
		super();
	}

	[Inject (source="gameModel.onlinePlayers", bind="true")]
	public function set availablePlayersList (value : OnlinePlayers) : void
	{
		if (pm && value && value.data && value.data.length)
		{
			pm.onlinePlayers = value;
			initFriends();
		}
	}


	[PostConstruct]
	public override function init() : void
	{
		super.init();

		parseTextures("Friends");

		_headerText.text = "SELECT FRIENDS";

		getSprite("FriendAcceptedButton").visible = false;
		getSprite("FriendInviteButton").visible = false;
		getSprite("FriendRejectedButton").visible = false;
		getSprite("FriendAvatarShadow").visible = false;
		getSprite("FriendDivider").visible = false;
		getSprite("FriendAvatar").visible = false;
		_divider.visible = false;

		_startButton = getButton("StartGameButton", "GreenButton_pressed");

		_footer = getSprite("Footer");
		Utils.proportionsRelativeIncreaseWidth(_footer);
		_upperLayer.addChild(_footer);
		_upperLayer.addChild(_startButton);
		_upperLayer.addChild(getTextField("StartGame"));
		getTextField("StartGame").text = "Start Game";

		_startButton.addEventListener(GameTouchEvent.CLICK, handleStartButtonTouched);

		initFriends();
	}

	private function initFriends () : void
	{
		if (friendList)
		{
			_contentLayer.removeChild(friendList);
		}

		friendList = beanFactory.getBeanInstance(BaseListComponent) as BaseListComponent;

		if (pm.onlinePlayers)
		{
			friendList.initData(720, 1180, pm.onlinePlayers.data, FriendRenderer);
			_contentLayer.addChild(friendList);
		}

		friendList.x = 0;
		friendList.y = 100;
	}


	private function handleStartButtonTouched(event : GameTouchEvent) : void
	{
		pm.start();
	}
}
}
