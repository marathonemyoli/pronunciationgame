package com.emyoli.phonics.views.help
{
import com.emyoli.phonics.views.settings.*;
import com.emyoli.phonics.components.DropDownList;
import com.emyoli.phonics.components.ToggleSwitch;
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.views.score.*;
import com.emyoli.phonics.components.ButtonGroup;
import com.emyoli.phonics.components.TwoStateButton;
import com.emyoli.phonics.models.ProportionsScaleMode;
import com.emyoli.phonics.net.MyScoreBoard;
import com.emyoli.phonics.renderers.MyScoresRenderer;
import com.emyoli.phonics.components.BaseListComponent;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.views.*;

import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

public class HelpView extends BaseView
{
	public static const ATLAS_ID : String = "help";

	private var _helpDropDown : DropDownList;
	private var _contentText : TextField;
	private const HELP_TEXT1 : String = "Help text1\nHelp text1";
	private const HELP_TEXT2 : String = "Help text2\nHelp text2";

	public function HelpView(state : int = 0)
	{
		super();
	}

	[PostConstruct]
	public override function init() : void
	{
		super.init();

		parseTextures(ATLAS_ID);

		_divider.visible = false;

		_headerText.text = "HELP";

		/**
		 * DropDown
		 */

		_helpDropDown = getDropDown("HELP list frame_btn", "revealList_top", "revealList_midle", "revealList_bottom", "HELP list frame", "revealList_separator", "game3_settings");
		_contentLayer.addChild(_helpDropDown);
		_helpDropDown.initData([{text:'Help1', value:HELP_TEXT1},{text:'Help2', value:HELP_TEXT2}]);
		_helpDropDown.addEventListener(GameEvent.ITEM_SELECTED, handleDropChange);

		_contentText = getTextField("HELP content");
		_contentText.text = HELP_TEXT1;

		_contentLayer.addChild(getTextField("HELP list frame"));

	}

	private function handleDropChange(event : GameEvent) : void
	{
		_contentText.text = event.data.value as String;
	}


}
}
