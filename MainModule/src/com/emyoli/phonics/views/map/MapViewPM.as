package com.emyoli.phonics.views.map
{
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.net.Login;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.views.avatarSelectionPopup.AvatarSelectionPopup;
import com.emyoli.phonics.views.help.HelpView;
import com.emyoli.phonics.views.login.LoginView;
import com.emyoli.phonics.views.score.ScoreView;
import com.emyoli.phonics.views.gameSettings.GameSettingsView;
import com.emyoli.phonics.views.settings.MainSettingsView;

import starling.events.EventDispatcher;

public class MapViewPM
{
	[Inject]
	public var assetController : AssetController;

	[Inject (source="gameModel.userSetting", bind="true")]
	public var userSetting : UserSetting;

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	public function MapViewPM()
	{
	}

	public function startGame(gameId : int) : void
	{
		if (gameId == 3)
		{
			dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, GameSettingsView));
		}
	}

	public function openScoreView() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, ScoreView, ScoreView.STATE_MY_SCORE));
	}

	public function openAvatarPopup() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.OPEN_POPUP, AvatarSelectionPopup));
	}

	public function openSettingsView() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, MainSettingsView));
	}

	public function showLoginWindow() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, LoginView));
	}

	public function showHelpWindow() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, HelpView));
	}
}
}
