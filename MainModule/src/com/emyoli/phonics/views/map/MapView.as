package com.emyoli.phonics.views.map {
import com.creativebottle.starlingmvc.events.BeanEvent;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.factories.BeanInstanceFactory;
import com.emyoli.phonics.net.GetAvatarName;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.views.BaseView;
import com.emyoli.phonics.views.avatarSelectionPopup.AvatarSelectionPopup;
import com.emyoli.phonics.views.map.MapLayer;

import starling.core.Starling;

import starling.display.Sprite;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class MapView extends BaseView
{
	[Inject]
	public var pm : MapViewPM;

	private var _settingsButton : Sprite;
	private var _helpButton : Sprite;
	private var _loginButton : Sprite;
	private var _scoreButton : Sprite;
	private var _mainFooter : Sprite;
	private var _avatar : Sprite;
	private var _mapLayer : MapLayer;

	private var _currentTouchId : int;
	private var _touchStart : Number;

	[EventHandler (event="GameEvent.AVATAR_SELECTED", properties="data")]
	public function handleAvatarSet (data : GetAvatarName) : void
	{
		updateAvatar(data.name);
	}

	[Inject (source="gameModel.userSetting", bind="true")]
	public function set user (data : UserSetting) : void
	{
		if (pm && data)
		{
			updateAvatar(data.avatarname);
		}
	}

	public function MapView()
	{
		super();
	}

	[PostConstruct]
	public override function init() : void
	{
		super.init();

		parseTextures("Main");

		_headerText.text = "THE PHONICS GAME";

		_settingsButton = getButton("MainSettingsButton", "MainSettingsButton_pressed", "Main");
		_helpButton = getButton("MainHelpButton", "MainHelpButton_pressed", "Main");
		_loginButton = getButton("MainLoginButton", "MainLoginButton_pressed", "Main");
		_scoreButton = getButton("MainScoreButton", "MainScoreButton_pressed", "Main");
		_mainFooter = getSprite("MainFooter");

		Utils.proportionsRelativeIncreaseWidth(_mainFooter, true);

		for each (var buttonSprite : Sprite in [_settingsButton, _helpButton, _loginButton, _scoreButton, getSprite("MainAvatar")])
		{
			Utils.proportionsRelativeSetJustifyX(buttonSprite);
		}

		_upperLayer.addChild(_mainFooter);
		_upperLayer.addChild(_settingsButton);
		_upperLayer.addChild(_helpButton);
		_upperLayer.addChild(_loginButton);
		_upperLayer.addChild(_scoreButton);

		_scoreButton.addEventListener(GameTouchEvent.CLICK, handleScoreClick);
		_settingsButton.addEventListener(GameTouchEvent.CLICK, handleSettingsClick)
		_loginButton.addEventListener(GameTouchEvent.CLICK, handleLoginClick)
		_helpButton.addEventListener(GameTouchEvent.CLICK, handleHelpClick)

		updateAvatar();

		_background.removeFromParent();
		_divider.removeFromParent();

		initMap();
	}

	private function initMap() : void
	{
		_mapLayer = new MapLayer;
		_contentLayer.addChild(_mapLayer);

		Utils.proportionsRelativeMoveCenterX(_contentLayer, true);

		_mapLayer.addEventListener(TouchEvent.TOUCH, handleMapTouch)
	}

	private function updateAvatar(name : String = "") : void
	{
		if (_avatar)
		{
			_upperLayer.removeChild(_avatar);
		}

		const avatar : Sprite = getSprite("MainAvatar");
		_avatar = pm.assetController.getAvatar(name);

		_avatar.height = avatar.height;
		_avatar.width = avatar.width;
		_avatar.x = avatar.x;
		_avatar.y = avatar.y;

		_upperLayer.addChild(_avatar);
		_avatar.addEventListener(GameTouchEvent.CLICK, handleAvatarClick);


	}

	private function handleMapTouch(e : TouchEvent) : void
	{
		for (var i : int = 0; i < e.touches.length; i++)
		{
			if (e.touches[i].phase == TouchPhase.BEGAN)
			{
				_currentTouchId = e.touches[i].id;
				_touchStart = e.touches[i].globalX;
			}
			else if (e.touches[i].id == _currentTouchId)
			{
				if (e.touches[i].phase == TouchPhase.MOVED)
				{
					if (_currentTouchId != -1)
					{
						var newContainerX : Number = _mapLayer.x + (e.touches[i].globalX - _touchStart);
						if (newContainerX > 0)
						{
							newContainerX = 0;
						} else if (newContainerX < -(_mapLayer.width - stage.stageWidth))
						{
							newContainerX = -(_mapLayer.width - stage.stageWidth);
						}
						_mapLayer.x = newContainerX;
						_touchStart = e.touches[i].globalX;
					}
				}
				else if (e.touches[i].phase == TouchPhase.ENDED)
				{
					_currentTouchId = -1;
				}
			}
		}
	}

	private function handleHelpClick(event : GameTouchEvent) : void
	{
		pm.showHelpWindow();
	}

	private function handleLoginClick(event : GameTouchEvent) : void
	{
		pm.showLoginWindow();
	}

	private function handleAvatarClick(event : GameTouchEvent) : void
	{
		pm.openAvatarPopup();
	}

	private function handleScoreClick(event : GameTouchEvent) : void
	{
		pm.openScoreView();
	}

	private function handleSettingsClick(event : GameTouchEvent) : void
	{
		pm.openSettingsView();
	}
}
}
