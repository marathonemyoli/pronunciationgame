package com.emyoli.phonics.views.map
{
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameTouchEvent;

import flash.geom.Rectangle;

import starling.display.Image;

import starling.display.Sprite;
import starling.text.TextField;

public class MapLayer extends Sprite
{
	[Inject]
	public var pm : MapViewPM;

	private const BACKGROUND_SCALE : Number = 2560 / 2048; //Resolution of background has been reduced because of recommended texture size 2048
	private const PART_SALE : Number = 3962 / 2 / 2560;
	private const BUCKET_POSTFIX : String = "_clean";
	private const TOP_LAYER_PREFIX : String = "topLayer_";


	private var _background : Sprite;
	private var _buckets : Sprite;
	private var _upperLayer : Sprite;

	[Inject]
	public var assetController : AssetController;

	public function MapLayer()
	{
		super();
	}

	[PostConstruct]
	public function init () : void
	{
		initBackground();
		initBuckets();
		initUpperLayer();
	}

	private function initUpperLayer() : void
	{
		_upperLayer = new Sprite();
		addChild(_upperLayer);

		var item : Sprite;
		for (var i : int = 1; i <= 12; i++)
		{
			item = assetController.getSpriteFrom("topLayer", TOP_LAYER_PREFIX + i);
			item.touchable = false;

			_upperLayer.addChild(item);
		}
	}

	private function initBuckets() : void
	{
		_buckets = new Sprite();
		addChild(_buckets);

		for (var i : int = 1; i <= 13; i++)
		{
			const bucket : Sprite = assetController.getSpriteFrom("buckets", i + BUCKET_POSTFIX);
			var label : TextField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1.3,
					bucket.width, AssetController.DEFAULT_TEXT_HEIGHT, 0xFFFFFF, AssetController.FONT_QUADON, true);
			label.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];
			label.text = "" + i;
			label.x = bucket.x;
			label.y = bucket.y;
			bucket.addChild(label);
			bucket.addEventListener(GameTouchEvent.CLICK, handleBucketClick)

			_buckets.addChild(bucket);
		}

	}

	private function handleBucketClick(event : GameTouchEvent) : void
	{
		const id : String = (event.currentTarget as Sprite).name.split(BUCKET_POSTFIX)[0];
		pm.startGame(int(id));
	}

	private function initBackground() : void
	{
		const mapBG1 : Image = assetController.getImage("MapBackground_1");
		const mapBG2 : Image = assetController.getImage("MapBackground_2");
		mapBG2.x = mapBG1.width * PART_SALE;

		_background = new Sprite();
		_background.addChild(mapBG1);
		_background.addChild(mapBG2);
		_background.scaleX = _background.scaleY = BACKGROUND_SCALE;
		_background.clipRect = new flash.geom.Rectangle(0, 0, (mapBG1.width + mapBG2.width) * PART_SALE, mapBG1.height);
		addChild(_background);
	}
}
}
