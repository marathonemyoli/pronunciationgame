package com.emyoli.phonics.views.avatarSelectionPopup {
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.models.ProportionsScaleMode;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.utils.NativeText;
import com.emyoli.phonics.views.AtlasParser;
import com.emyoli.phonics.views.BasePopupView;
import com.emyoli.phonics.views.avatarSelection.AvatarSelectionViewPM;

import starling.display.Sprite;

public class AvatarSelectionPopup extends BasePopupView {

	private const AVATAR_PREFIX : String = "Avatar";

	[Inject]
	public var pm : AvatarSelectionViewPM;

	[Inject]
	public var atlasParser : AtlasParser;
	
	private var _button : Sprite;

	private var _nameField : NativeText;

	private var _selectedAvatar : String;

	private var _selector : Sprite;

	public function AvatarSelectionPopup()
	{
		super();
	}

	[EventHandler (event="GameEvent.USER_SETTING_CHANGE", properties="data")]
	public function handleUserSettingChange (data : UserSetting) : void
	{
		if (_nameField)
		{
			_nameField.text = data.nickname;
		}
	}

	[PostConstruct]
	public override function init () : void
	{
		_drawHeader = false;
		_proportionsScaleMode = ProportionsScaleMode.RIGHT;

		super.init();

		parseTextures("Avatar_popup", true);

		_upperLayer.addChild(getSprite("PopupHeader"));

		initPopupHeaderTextField();

		_headerText.text = "CHANGE AVATAR";

		/**
		 * Init left corner avatar
		 */
		const currentAvatar : Sprite = getSprite("Avatar");
		const leftCornerAvatar : Sprite = assetController.getAvatar();
		leftCornerAvatar.width = currentAvatar.width;
		leftCornerAvatar.height = currentAvatar.height;
		leftCornerAvatar.x = currentAvatar.x;
		leftCornerAvatar.y = currentAvatar.y;
		_upperLayer.addChild(leftCornerAvatar);

		/////////////////////////////////////////////////////

		_button = getButton("DoneButton", "GreenButton_pressed");
		_button.addEventListener(GameTouchEvent.CLICK, handleSelectButtonTouched);
		getTextField("DoneButton").text = "DONE!";

		_nameField = getTextInput("NickNameHolder");
		_nameField.x += 25;
		_nameField.text = pm.userSettings.nickname;

		_background.x = 76;

		/**
		 * Replace avatar selector with one from Avatar screen
		 */
		const currentSelector : Sprite = getSprite("AvatarSelector");
		_selector = assetController.getSpriteCloneFrom("Avatar", "AvatarSelector");
		_selector.width = currentSelector.width;
		_selector.height = currentSelector.height;
		currentSelector.parent.removeChild(currentSelector);
		_bgLayer.addChild(_selector);

		/**
		 * Position dividers
		 */
		_divider.visible = false;
		var dividers : Sprite = new Sprite();
		_bgLayer.addChild(dividers);
		for (var i : int = 0; i < 6; i++)
		{
			const divider : Sprite = getSprite("AvatarDivider" + i);
			dividers.addChild(divider);

			if (i == 5)
			{
				divider.visible = false;
			}
		}

		///////////////////////////////////

		setupAvatars();

		////////////////////////////////////

		setupPopupShadow();

		selectAvatar(int(pm.userSettings.avatarname.split(AVATAR_PREFIX)[1]));
	}



	private function setupAvatars() : void
	{
		for (var i : int = 0; i < 6; i++)
		{
			var ava : Sprite = getAvatar(i+1);

			if (pm.userSettings.gender == Gender.GIRL)
			{
				var girlAvatar : Sprite = assetController.getSpriteCloneFrom("Avatar", AVATAR_PREFIX + (i+1), true, false);
				girlAvatar.x = ava.x;
				girlAvatar.y = ava.y;
				girlAvatar.width = ava.width;
				girlAvatar.height = ava.height;
				girlAvatar.name = ava.name;
				ava.parent.addChild(girlAvatar);
				ava.parent.removeChild(ava);

				ava = girlAvatar;
			}
			ava.addEventListener(GameTouchEvent.CLICK, handleAvatarSelected);
		}
	}

	private function selectAvatar (number : int) : void
	{
		var avatar : Sprite = getAvatar(number);
		_selector.x = avatar.x + (avatar.width - _selector.width >> 1);
		_selector.y = avatar.y + (avatar.height - _selector.height >> 1);

		_selectedAvatar = AVATAR_PREFIX + number;
	}

	private function getAvatar(number : int) : Sprite
	{
		return getSprite(AVATAR_PREFIX + number);
	}

	private function handleAvatarSelected(event : GameTouchEvent) : void
	{
		var id : int = int((event.currentTarget as Sprite).name.split(AVATAR_PREFIX)[1]);
		selectAvatar(id);
	}

	private function handleSelectButtonTouched(event : GameTouchEvent) : void
	{
		pm.select(_selectedAvatar, _nameField.text);
	}


}
}
