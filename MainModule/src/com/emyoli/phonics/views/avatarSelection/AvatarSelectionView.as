package com.emyoli.phonics.views.avatarSelection
{
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.views.signup.*;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.views.*;
import com.emyoli.phonics.utils.NativeText;

import starling.core.Starling;
import starling.display.Sprite;

public class AvatarSelectionView extends BaseView
{
	private const AVATAR_PREFIX : String = "Avatar";

	[Inject]
	public var pm : AvatarSelectionViewPM;

	[Inject]
	public var signupPM : SignupViewPM;

	[Inject]
	public var atlasParser : AtlasParser;

	private var _nameField : NativeText;
	private var _button : Sprite;
	private var _selectedAvatar : String;
	private var _selector : Sprite;
	private var _avatars : Array = [];

	public function AvatarSelectionView()
	{
		super();
	}

	[PostConstruct]
	public override function init() : void
	{
		super.init();

		parseTextures("Avatar");

		_headerText.text = "SELECT AVATAR";

		_button = getButton("DoneButton", "GreenButton_pressed");
		_button.addEventListener(GameTouchEvent.CLICK, handleSelectButtonTouched);
		getTextField("DoneButton").text = "DONE!";

		_nameField = getTextInput("NickNameHolder");
		_nameField.x += 30;

		/**
		 * Position dividers
		 */
		_divider.visible = false;
		var dividers : Sprite = new Sprite();
		_bgLayer.addChild(dividers);
		for (var i : int = 0; i < 4; i++)
		{
			const divider : Sprite = getSprite("AvatarDivider" + i);
			dividers.addChild(divider);
		}

		_selector = getSprite("AvatarSelector");
		Utils.proportionsRelativeIncreaseWidth(_selector);
		_bgLayer.addChild(_selector);
		///////////////////////////////////

		setupAvatars();

		////////////////////////////////////

		dividers.x = stage.stageWidth - dividers.width >> 1;
		Utils.proportionsRelativeMoveCenterX(dividers, true);

		selectAvatar(1);
	}

	private function setupAvatars() : void
	{
		for (var i : int = 0; i < 6; i++)
		{
			var ava : Sprite = getAvatarSource(i+1);

			if (signupPM.userSettings.gender == Gender.BOY)
			{
				var boyAvatar : Sprite = assetController.getSpriteCloneFrom("Avatar_popup", AVATAR_PREFIX + (i+1));
				boyAvatar.x = ava.x;
				boyAvatar.y = ava.y;
				boyAvatar.width = ava.width;
				boyAvatar.height = ava.height;
				boyAvatar.name = ava.name;
				ava.parent.addChild(boyAvatar);
				ava.parent.removeChild(ava);
				ava = boyAvatar;
				_avatars[ava.name.split(AVATAR_PREFIX)[1]] = ava;

				Utils.proportionsRelativeSetJustifyX(ava);
			}

			_avatars[ava.name.split(AVATAR_PREFIX)[1]] = ava;
			ava.addEventListener(GameTouchEvent.CLICK, handleAvatarSelected);
		}
	}

	private function selectAvatar (number : int) : void
	{
		var avatar : Sprite = getAvatar(number);
		_selector.x = avatar.x + (avatar.width - _selector.width)/2;
		_selector.y = avatar.y + (avatar.height - _selector.height)/2;

		_selectedAvatar = AVATAR_PREFIX + number;
	}

	private function getAvatar(number : int) : Sprite
	{
		return _avatars[number];
	}

	private function getAvatarSource(number : int) : Sprite
	{
		return getSprite(AVATAR_PREFIX + number);
	}

	private function handleAvatarSelected(event : GameTouchEvent) : void
	{
		var id : int = int((event.currentTarget as Sprite).name.split(AVATAR_PREFIX)[1]);
		selectAvatar(id);
	}

	private function handleSelectButtonTouched(event : GameTouchEvent) : void
	{
		signupPM.selectNicknameAvatarAndSend(_nameField.text, _selectedAvatar);
	}

}
}
