package com.emyoli.phonics.views.avatarSelection {
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.net.GetAvatarName;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.net.SetAvatarName;
import com.emyoli.phonics.net.SetUserSetting;
import com.emyoli.phonics.net.Signup;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.utils.SharedObjectManager;
import com.emyoli.phonics.views.avatarSelectionPopup.AvatarSelectionPopup;
import com.emyoli.phonics.views.friendsSelection.FriendsSelectionView;

import starling.events.EventDispatcher;

public class AvatarSelectionViewPM {

	private const DEFAULT_AVATAR_NAME : String = "avatar1";

	[Inject]
	public var netConnector : NetConnector;

	[Inject]
	public var mainView : MainView;


	[Dispatcher]
	public var dispatcher : EventDispatcher;

	private var _userSettings : UserSetting;

	[Inject (source="gameModel.userSetting", bind="true")]
	public function set userSetting (value : UserSetting) : void
	{
		_userSettings = value;
	}

	public function get userSettings() : UserSetting
	{
		return _userSettings;
	}

	public function AvatarSelectionViewPM()
	{
	}

	public function SignupViewPM()
	{
	}

	public function select(avatar : String, name : String) : void
	{
		const model : SetAvatarName = new SetAvatarName();
		model.name = avatar;
		netConnector.sendMessage(model, CmdType.SetAvatarName);

		const model2 : SetUserSetting = new SetUserSetting();
		_userSettings.nickname = name;
		model2.data = _userSettings;
		netConnector.sendMessage(model2, CmdType.SetUserSetting);
	}

	[EventHandler (event="GameEvent.AVATAR_SELECTED", properties="data")]
	public function handleAvatarSet (data : GetAvatarName) : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.CLOSE_POPUP, AvatarSelectionPopup));
	}

}
}
