package com.emyoli.phonics.views.login {
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.Login;
import com.emyoli.phonics.net.LoginResp;
import com.emyoli.phonics.net.Logout;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.views.avatarSelection.AvatarSelectionView;
import com.emyoli.phonics.views.friendsSelection.FriendsSelectionView;
import com.emyoli.phonics.views.map.MapView;
import com.emyoli.phonics.views.signup.SignupView;

import starling.events.EventDispatcher;


public class LoginViewPM {

	[Inject]
	public var netConnector : NetConnector;

	[Inject(source="gameModel")]
	public var gameModel : GameModelRepo;

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	public function LoginViewPM()
	{
	}

	public function login(login : String, pass : String) : void
	{
		if (gameModel.loggedIn)
		{
			netConnector.sendMessage(new Logout(), CmdType.Logout);
		}

		var lr : Login = new Login();
		lr.username = login;
		lr.password = pass;

		netConnector.sendMessage(lr, CmdType.Login);
	}

	[EventHandler (event="NetLoginEvent.LOGIN", properties="response")]
	public function handleLoginSuccess (data : LoginResp) : void
	{
		if (data.id.toNumber())
		{
			dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, MapView));
		}else
		{
			openSignupWindow();
		}
	}

	public function openSignupWindow() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, SignupView));
	}

}
}
