package com.emyoli.phonics.views.login
{
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.views.*;
import com.emyoli.phonics.utils.NativeText;

import starling.display.Image;

import starling.display.Sprite;
import starling.events.TouchEvent;

import starling.text.TextField;

public class LoginView extends BaseView
{
	[Inject]
	public var pm : LoginViewPM;

	private var _userNameField : NativeText;
	private var _passField : NativeText;
	private var _button : Sprite;
	private var _userNameLabel : TextField;
	private var _passLabel : TextField;

	public function LoginView()
	{
		super();
	}

	[PostConstruct]
	public override function init() : void
	{
		_homeButtonClass = LoginView;

		super.init();

		parseTextures("Login");

		_button = getButton("LoginButton", "GreenButton_pressed");
		_button.addEventListener(GameTouchEvent.CLICK, handleButtonTouched);

		_userNameField = getTextInput("Name");
		_passField = getTextInput("User");
		_userNameLabel = getTextField("Name");
		_userNameLabel.text = "Name:";

		_passLabel = getTextField("User");
		_passLabel.text = "User:";
		getTextField("Done").text = "DONE!";
		getTextField("Header").text = "LOGIN";
	}

	private function handleButtonTouched(event : TouchEvent) : void
	{
		pm.login(_userNameField.text, _passField.text);
	}
}
}
