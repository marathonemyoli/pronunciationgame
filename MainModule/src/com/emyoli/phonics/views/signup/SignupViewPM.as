package com.emyoli.phonics.views.signup {
import com.emyoli.phonics.components.BaseListComponent;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.net.Signup;
import com.emyoli.phonics.net.SignupResp;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.utils.SharedObjectManager;
import com.emyoli.phonics.views.avatarSelection.AvatarSelectionView;
import com.emyoli.phonics.views.friendsSelection.FriendsSelectionView;
import com.emyoli.phonics.views.login.LoginView;
import com.emyoli.phonics.views.map.MapView;

import starling.events.EventDispatcher;

public class SignupViewPM {

	[Inject]
	public var netConnector : NetConnector;

	[Inject]
	public var assetController : AssetController;

	[Inject (source='gameModel')]
	public var gameModel : GameModelRepo;

	[Inject]
	public var storage : SharedObjectManager;

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	private const DEFAULT_AVATAR_NAME : String = "avatar1";

	private const DEFAULT_LANG : String = "EN";

	public var userSettings : UserSetting;

	public function SignupViewPM()
	{
	}

	public function signup(name : String, family : String, phone : String, password : String, gender : int, age : int) : void
	{

		userSettings = new UserSetting();
		userSettings.firstname = name;
		userSettings.familyname = family;
		userSettings.gender = gender;
		userSettings.age = age;
		userSettings.password = phone;
		userSettings.language = DEFAULT_LANG;

		openAvatarWindow();
	}

	public function selectNicknameAvatarAndSend (nickName : String, avatar : String) : void
	{
		userSettings.nickname = nickName;
		userSettings.username = nickName;
		userSettings.avatarname = avatar;

		const model : Signup = new Signup();
		model.data = userSettings;

		gameModel.userSetting = userSettings;

		storage.setData("username", userSettings.username);
		storage.setData("password", userSettings.password);
		storage.save();

		netConnector.sendMessage(model, CmdType.Signup);
		//dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, MapView));
	}

	public function openLoginWindow() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, LoginView));
	}

	public function openAvatarWindow() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, AvatarSelectionView));
	}

	[EventHandler (event="NetSignupEvent.SIGNUP", properties="response")]
	public function handleLoginSuccess (data : SignupResp) : void
	{
		if (data.id)
		{
			dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, MapView));
		}
	}
}
}
