package com.emyoli.phonics.views.signup
{
import com.creativearea.FormValidate;
import com.creativearea.ValidationRulesBuilder;
import com.creativearea.ValidationRulesBuilder;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.events.MessageEvent;
import com.emyoli.phonics.models.MessageModel;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.utils.errorGet;
import com.emyoli.phonics.utils.localeGet;
import com.emyoli.phonics.views.*;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.utils.NativeText;

import starling.display.Image;
import starling.display.Sprite;

import starling.text.TextField;

public class SignupView extends BaseView
{
	[Inject]
	public var pm : SignupViewPM;

	private var _nameField : NativeText;
	private var _familyField : NativeText;
	private var _userField : NativeText;
	private var _genderField : NativeText;
	private var _ageField : NativeText;

	private var _signupButton : Sprite;
	private var _loginButton : Sprite;
	private var _boyButtonLabel : TextField;
	private var _boyText : TextField;
	private var _girlButtonLabel : TextField;
	private var _girlText : TextField;
	private var _boyButton : Image;
	private var _girlButton : Image;

	private var _gender : int = Gender.BOY;

	public function SignupView(param : * = null)
	{
		super();
	}

	[PostConstruct]
	public override function init() : void
	{
		_homeButtonClass = SignupView;

		super.init();

		parseTextures("Signup");

		_loginButton = getButton("LoginButton", "OrangeButton_pressed");
		_loginButton.addEventListener(GameTouchEvent.CLICK, handleLoginButtonTouched);

		_signupButton = getButton("SignUpButton", "GreenButton_pressed");
		_signupButton.addEventListener(GameTouchEvent.CLICK, handleSignupButtonTouched);

		getTextField("Name").text = "Name:";
		getTextField("Family").text = "Family:";
		getTextField("User").text = "Password:";
		getTextField("Gender").text = "Gender:";
		getTextField("Age").text = "Age:";
		getTextField("Member").text = "Already a member?";

		getTextField("Signup").text = "SIGN UP";
		getTextField("Login").text = "LOGIN";

		/**
		 * Gender toggle
		 */
		_boyButtonLabel = getTextField("Boy_buttonLabel");
		_boyButtonLabel.fontSize = AssetController.DEFAULT_FONT_SIZE;
		_boyText = getTextField("Boy_textFieldBlack");
		_boyText.text = "Boy";
		_boyButtonLabel.text = "BOY";

		_girlButtonLabel = getTextField("Girl_buttonLabel");
		_girlButtonLabel.fontSize = AssetController.DEFAULT_FONT_SIZE;
		_girlText = getTextField("Girl_textFieldBlack");
		_girlButtonLabel.text = "GIRL";
		_girlText.text = "Girl";

		_boyButton = getImage("Slider_Boy");
		_girlButton = getImage("Slider_Girl");

		_boyButton.addEventListener(GameTouchEvent.CLICK, handleGenderClick);
		_girlButton.addEventListener(GameTouchEvent.CLICK, handleGenderClick);

		_girlButton.visible = false;
		_girlButtonLabel.visible = false;
		_boyText.visible = false;

		///////////////////////////////////

		_nameField = getTextInput("Name");
		_familyField = getTextInput("Family");
		_userField = getTextInput("User");
		_ageField = getTextInput("Age");

		///////////////////////////////////

		_headerText.text = "SIGN UP";
		
		initValidationRules();
	}

	private function toggleGender() : void
	{
		_gender = _gender == Gender.BOY ? Gender.GIRL : Gender.BOY;

		_girlButton.visible = _gender == Gender.GIRL;
		_girlButtonLabel.visible = _gender == Gender.GIRL;
		_girlText.visible = _gender == Gender.BOY;
		_boyButton.visible = _gender == Gender.BOY;
		_boyButtonLabel.visible = _gender == Gender.BOY;
		_boyText.visible = _gender == Gender.GIRL;
	}

	private function handleGenderClick(event : GameTouchEvent) : void
	{
		toggleGender();
	}

	private function handleSignupButtonTouched(event : GameTouchEvent) : void
	{
		if (_validator.validate() )
		{
			pm.signup(_nameField.text, _familyField.text, _userField.text , _userField.text, _gender, int(_ageField.text));
		} else
		{
			dispatcher.dispatchEvent(new MessageEvent(
					MessageEvent.CREATE_MESSAGE,
					Utils.buildErrorMessage(_validator.getErrors()[0])));
			trace(_validator.getErrors().join(', '));
		}


	}

	private function handleLoginButtonTouched(event : GameTouchEvent) : void
	{
		pm.openLoginWindow();
	}

	/**
	 * Validation
	 */

	private function initValidationRules() : void
	{
		validationRules = ValidationRulesBuilder.build()
				.addRule("Name")
					.addRequired(errorGet("signup.signup_button.empty"))
					.addMinLength(4, errorGet("login.button.nickname.length"))
					.addMaxLength(6, errorGet("login.nickname.length"))
				.parent
				.addRule("Family").addRequired(errorGet("signup.signup_button.empty")).parent
	}
}
}
