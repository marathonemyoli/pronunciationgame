package com.emyoli.phonics.views.score
{
import com.emyoli.phonics.components.ButtonGroup;
import com.emyoli.phonics.components.TwoStateButton;
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.models.ProportionsScaleMode;
import com.emyoli.phonics.net.MyScoreBoard;
import com.emyoli.phonics.net.PronunciationService;
import com.emyoli.phonics.net.PronunciationServiceEvent;
import com.emyoli.phonics.renderers.MyScoresRenderer;
import com.emyoli.phonics.components.BaseListComponent;
import com.emyoli.phonics.renderers.ScorePronuncationRenderer;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.views.*;

import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;

public class ScoreView extends BaseView
{
	private const HEADERS : Array = ["MY SCORES", "LEADERBOARD", "PRONUNCIATION"];
    private const LIST_RENDERERS : Array = [MyScoresRenderer, "LEADERBOARD", ScorePronuncationRenderer];

	public static const STATE_MY_SCORE : int = 0;
	public static const STATE_LEADERBOARD : int = 1;
	public static const STATE_PRONUNCIATION : int = 2;

	public static const ATLAS_ID : String = "scores_all";


	[Inject]
	public var pm : ScoreViewPM;

    [Inject]
    public var recognitionService : PronunciationService;

	public var list : BaseListComponent;

	private var _currentState : int = 0;

	private var _sertificateButton : TwoStateButton;
	private var _footer : Sprite;

	private var _myScoreButton : TwoStateButton;
	private var _myPronunciationButton : TwoStateButton;
	private var _myLeaderboardButton : TwoStateButton;
	private var _buttonBar : ButtonGroup;

    private var _scorePronunciation : Array;

	public function ScoreView(state : int = 0)
	{
		super();

		this.state = state;
	}

	[Inject (source="gameModel.myScore", bind="true")]
	public function set myScore (value : MyScoreBoard) : void
	{
		if (pm && value && value.info && value.info.length)
		{
			pm.myScore = value;
			initList();
		}
	}


	[PostConstruct]
	public override function init() : void
	{
		_proportionsScaleMode = ProportionsScaleMode.NONE;

		super.init();

		parseTextures(ATLAS_ID);

		_divider.visible = false;
		for (var i : int = 1; i <= 6; i++)
		{
			getSprite("SCORES_divider" + i + "_rendererClass").visible = false;
		}

		/**
		 * ButtonBar
		 */
		_myScoreButton = getButton("SCORES_myScores_btn_normal_rendererClass", "SCORES_myScores_btn_selected_rendererClass", ATLAS_ID);
		_myLeaderboardButton = getButton("SCORES_leadersBoard_btn_normal_rendererClass", "SCORES_leadersBoard_btn_selected_rendererClass", ATLAS_ID);
		_myPronunciationButton = getButton("SCORES_pronunciation_btn_normal_rendererClass", "SCORES_pronunciation_btn_selected_rendererClass", ATLAS_ID);
		_buttonBar = ButtonGroup.createGroupFrom(new <TwoStateButton>[_myScoreButton, _myLeaderboardButton, _myPronunciationButton]);
		_buttonBar.init();
		_buttonBar.addEventListener(GameEvent.ITEM_SELECTED, handleTabSelected);

        var delta : Number = (stage.stageWidth - (_myScoreButton.width + _myLeaderboardButton.width + _myPronunciationButton.width)) / 2;
        _myScoreButton.x += delta;
        _myLeaderboardButton.x += delta;
        _myPronunciationButton.x += delta;

        Utils.proportionsRelativeIncreaseWidth(getSprite("SCORES_btns_back_rendererClass"), false);

		/**
		 * Footer
		 */
		const upState : Sprite = assetController.getSpriteCloneFrom("Friends", "StartGameButton", false);
		_sertificateButton = beanFactory.getBeanInstance(TwoStateButton) as TwoStateButton;
		_sertificateButton.init(upState ,assetController.getSpriteCloneFrom("Base", "GreenButton_pressed"));
		_sertificateButton.x = upState.x;
		_sertificateButton.y = upState.y;
		Utils.proportionsRelativeMoveCenterX(_sertificateButton);
		upState.x = upState.y = 0;
		beanFactory.initBeanInstance(_sertificateButton);
		_sertificateButton.text = "CERTIFICATE";
		_sertificateButton.addEventListener(GameTouchEvent.CLICK, handleSertificateButtonTouched);
		_footer = assetController.getSpriteCloneFrom("Friends", "Footer", false);
		Utils.proportionsRelativeIncreaseWidth(_footer, false);
		///////////////////////////////////////////////////////////////////////////////

		_upperLayer.addChild(_footer);
		_upperLayer.addChild(_sertificateButton);

        recognitionService.addEventListener(PronunciationServiceEvent.SCORES, handleRecognized);

		refreshState();
	}

    private function handleRecognized(event : PronunciationServiceEvent) : void
    {
        trace(event.data);
        _scorePronunciation = [];
        for(var key:String in event.data["Phonemes"])
        {
            _scorePronunciation.push({score : int(event.data["Phonemes"][key]["CurrentScore"] * 100 / 5) + "%", name : key});
        }
        if(_currentState == STATE_PRONUNCIATION)
        {
            initList();
        }
    }

	public function set state (state : int) : void
	{
		_currentState = state;
		if (pm)
		{
			refreshState();
		}
	}

	private function refreshState() : void
	{
        recognitionService.getScores();
		pm.requestMyScore();
		initTitle();
		_footer.visible = _sertificateButton.visible = _currentState == STATE_MY_SCORE;
	}

	private function initTitle() : void
	{
		_headerText.text = HEADERS[_currentState];
	}

	private function initList () : void
	{
		if (list)
		{
			_bgLayer.removeChild(list);
		}

		list = new BaseListComponent; //beanFactory.getBeanInstance(BaseListComponent) as BaseListComponent;

        var aArr : Array = [];

        switch (_currentState){
            case STATE_MY_SCORE:
                if(pm.myScore)
                {
                    aArr = pm.myScore.info;
                }
                break;
            case STATE_LEADERBOARD:
                break;
            case STATE_PRONUNCIATION:
                    aArr = _scorePronunciation;
                break;
        }

		list.initData(720, 1400, aArr, LIST_RENDERERS[_currentState]);
		_bgLayer.addChild(list);

		list.x = 0;
		list.y = 100;
	}

	private function handleTabSelected(event : GameEvent) : void
	{
		_currentState = _buttonBar.selectedIndex;
		refreshState();
	}

	private function handleSertificateButtonTouched(event : GameTouchEvent) : void
	{

	}
}
}
