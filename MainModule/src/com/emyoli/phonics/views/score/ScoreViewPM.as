package com.emyoli.phonics.views.score {
import com.emyoli.phonics.net.GetMyScoreBoard;
import com.emyoli.phonics.net.MyScoreBoard;
import com.emyoli.phonics.views.friendsSelection.*;
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.GetAvailableFriendsList;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.views.map.MapView;

import starling.events.EventDispatcher;

public class ScoreViewPM {

	private const DEFAULT_AVATAR_NAME : String = "avatar1";

	[Inject]
	public var netConnector : NetConnector;

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[Inject (source="gameModel.userSetting", bind="true")]
	public var userSetting : UserSetting;

	public var myScore : MyScoreBoard;

	public function ScoreViewPM()
	{
	}

	public function requestMyScore() : void
	{
		netConnector.sendMessage(new GetMyScoreBoard(), CmdType.GetMyScoreBoard);
	}
}
}
