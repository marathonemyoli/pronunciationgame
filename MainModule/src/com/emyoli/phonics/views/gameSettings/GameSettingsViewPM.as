package com.emyoli.phonics.views.gameSettings
{
import com.awar.ags.api.Message;
import com.emyoli.phonics.components.BaseListComponent;
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.models.Game3Settings;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.GameCreate;
import com.emyoli.phonics.net.GameSetting;
import com.emyoli.phonics.net.GameType;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.views.Game3View;
import com.emyoli.phonics.views.friendsSelection.FriendsSelectionView;
import com.emyoli.phonics.views.friendsSelection.FriendsSelectionViewPM;

import starling.events.EventDispatcher;

public class GameSettingsViewPM
{
	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[Inject]
	public var connector : NetConnector;

	[Inject (source="gameModel")]
	public var gameModel : GameModelRepo;



	public function GameSettingsViewPM()
	{
	}

	public function startSinglePlayer(settings : Game3Settings) : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, Game3View, settings));
	}

	public function saveEnteredSettings (gameType : int = GameType.LOTO) : void
	{
		var gameSetting : GameSetting = new GameSetting();
		gameSetting.type = gameType;
		gameSetting.name = "LOTO";
		gameModel.gameSetting = gameSetting;
	}

	public function startMultiPlayer(settings : Game3Settings) : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW, FriendsSelectionView, settings));
	}
}
}
