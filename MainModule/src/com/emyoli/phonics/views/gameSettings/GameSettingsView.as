package com.emyoli.phonics.views.gameSettings
{
import com.emyoli.phonics.components.ButtonGroup;
import com.emyoli.phonics.components.DropDownList;
import com.emyoli.phonics.components.TwoStateButton;
import com.emyoli.phonics.controllers.ConfigController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.models.Game3Settings;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.utils.localeGet;
import com.emyoli.phonics.views.BaseView;

import starling.text.TextField;

public class GameSettingsView extends BaseView
{
	[Inject]
	public var pm : GameSettingsViewPM;

    [Inject]
	public var config : ConfigController;

	private const ATLAS_ID : String = "game3_settings";

	private var _button16 : TwoStateButton;
	private var _button24 : TwoStateButton;
	private var _button32 : TwoStateButton;
	private var _buttonGroup : ButtonGroup;
	private var _multiplayerButton : TwoStateButton;
	private var _singleplayerButton : TwoStateButton;
	private var _timeDropDown : DropDownList;
	private var _revealDropDown : DropDownList;
	private var _card16Text : TextField;
	private var _card24Text : TextField;
	private var _card36Text : TextField;

	public function GameSettingsView()
	{
		super();
	}

	[PostConstruct]
	public override function init() : void
	{
		super.init();

		parseTextures(ATLAS_ID);

		_button16 = getButton("cards16_normal_btn", "cards16_selected_btn", ATLAS_ID);
		_button16.toggle = true;

		_button24 = getButton("cards24_normal_btn", "cards24_selected_btn", ATLAS_ID);
		_button24.toggle = true;

		_button32 = getButton("cards36_normal_btn", "cards36_selected_btn", ATLAS_ID);
		_button32.toggle = true;

		_card16Text = getTextField("cards16");
		_card24Text = getTextField("cards24");
		_card36Text = getTextField("cards36");
		_card16Text.fontSize /= 1.6;
		_card24Text.fontSize /= 1.6;
		_card36Text.fontSize /= 1.6;
		_card16Text.text = "cards";
		_card24Text.text = "cards";
		_card36Text.text = "cards";

		getTextField("instructions3").text = "Which game do you want to play?";

		getTextField("gameTime").text = "Game Time:";
		getTextField("revealCards").text = "Reveal Cards:";

		_multiplayerButton = getButton("multiplayer_normal_btn", "multiplayer_selected_btn", ATLAS_ID);
		_singleplayerButton = getButton("singleplayer_normal_btn", "singleplayer_selected_btn", ATLAS_ID);
		getTextField("singleplayer").text = "SINGLEPLAYER";
		getTextField("multiplayer").text = "MULTIPLAYER";

		_revealDropDown = getDropDown("revealTime_drop", "revealList_top", "revealList_midle", "revealList_bottom", "revealList", "revealList_separator");
        _revealDropDown.persistStateTarget = this;
        _contentLayer.addChild(_revealDropDown);
        var aRevealArray = getArrayObjectList("game3.settings.reveal", "game3.settings.reveal", "game3.settings.no_reveal");
		_revealDropDown.initData(aRevealArray);

		_timeDropDown = getDropDown("gameTime_drop", "revealList_top", "revealList_midle", "revealList_bottom", "gameTimeList", "revealList_separator");
        _timeDropDown.persistStateTarget = this;
        _contentLayer.addChild(_timeDropDown);
        var aTimeArray = getArrayObjectList("game3.settings.time", "game3.settings.time", "game3.settings.unlimited");
        _timeDropDown.initData(aTimeArray);

		_buttonGroup = ButtonGroup.createGroupFrom(new <TwoStateButton>[_button16, _button24 ,_button32]);
		beanFactory.initBeanInstance(_buttonGroup);
        _buttonGroup.persistStateTarget = this;
		_buttonGroup.init(int(config.getString("game3_settings.buttonGroupDefaultIndex")));

		_bgLayer.addChild(getSprite("separator1"));
		_bgLayer.addChild(getSprite("separator2"));
		_bgLayer.addChild(getSprite("separator3"));

		_headerText.text = "SETTINGS";
		_divider.visible = false;

		_singleplayerButton.addEventListener(GameTouchEvent.CLICK, handleSinglePlayerClick);
		_multiplayerButton.addEventListener(GameTouchEvent.CLICK, handleMultiPlayerClick);
	}

    private function getArrayObjectList(inNameConfig : String, inNameDefault : String, inNameZero : String = "") : Array
    {
        var returnArray : Array = new Array();
        var arrConfig : Array = config.getArray(inNameConfig);
        for each(var aValue : int in arrConfig)
        {
            var aStr:String = localeGet(inNameDefault, [aValue, Utils.getNumberPostfix(aValue)]);
            if(aValue == 0 && inNameZero != "")
            {
                aStr = localeGet(inNameZero);
            }
            returnArray.push({text:aStr, value:aValue})
        }
        return returnArray;
    }

	private function handleMultiPlayerClick(event : GameTouchEvent) : void
	{
		pm.saveEnteredSettings();
		pm.startMultiPlayer(new Game3Settings(_timeDropDown.selectedItem["value"], _revealDropDown.selectedItem["value"], _buttonGroup.selectedIndex));
	}

	private function handleSinglePlayerClick(event : GameTouchEvent) : void
	{
		pm.startSinglePlayer(new Game3Settings(_timeDropDown.selectedItem["value"], _revealDropDown.selectedItem["value"], _buttonGroup.selectedIndex));
	}
}
}
