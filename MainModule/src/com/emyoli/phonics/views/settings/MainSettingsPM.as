package com.emyoli.phonics.views.settings
{
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.net.UserInfo;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.views.avatarSelectionPopup.AvatarSelectionPopup;

import starling.events.EventDispatcher;

public class MainSettingsPM
{
	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[Inject]
	public var netConnector : NetConnector;

	[Inject (source="gameModel")]
	public var gameModel : GameModelRepo;

	public function MainSettingsPM()
	{
	}

	public function changeAvatar() : void
	{
		dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.OPEN_POPUP, AvatarSelectionPopup));
	}

	public function changeLanguage(s : String) : void
	{
		const model : UserSetting = gameModel.userSetting;
		if (model.language != s)
		{
			model.language = s;
			netConnector.sendMessage(model, CmdType.SetUserSetting);
		}
	}

	public function changeSoundEffectProperty(value : Boolean) : void
	{

	}

	public function changeInstructionsProperty(value : Boolean) : void
	{
		
	}
}
}
