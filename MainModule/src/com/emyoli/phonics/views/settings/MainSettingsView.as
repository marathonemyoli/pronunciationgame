package com.emyoli.phonics.views.settings
{
import com.emyoli.phonics.components.DropDownList;
import com.emyoli.phonics.components.ToggleSwitch;
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.views.score.*;
import com.emyoli.phonics.components.ButtonGroup;
import com.emyoli.phonics.components.TwoStateButton;
import com.emyoli.phonics.models.ProportionsScaleMode;
import com.emyoli.phonics.net.MyScoreBoard;
import com.emyoli.phonics.renderers.MyScoresRenderer;
import com.emyoli.phonics.components.BaseListComponent;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.views.*;

import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;

public class MainSettingsView extends BaseView
{
	public static const ATLAS_ID : String = "settings";

	[Inject]
	public var pm : MainSettingsPM;

	private var _teachersSettingsButton : TwoStateButton;
	private var _languageDropDown : DropDownList;
	private var _soundEffectToggle : ToggleSwitch;
	private var _instructionsToggle : ToggleSwitch;
	private var _avatarsButton : TwoStateButton;

	public function MainSettingsView(state : int = 0)
	{
		super();
	}

	[PostConstruct]
	public override function init() : void
	{
		super.init();

		parseTextures(ATLAS_ID);

		_divider.visible = false;

		_headerText.text = "SETTINGS";

		/**
		 * SoundEffect toggle
		 */

		_soundEffectToggle = getToggleSwitch("sfx_slide", "sfx_slideBtn_leftt", "sfx_slideBtn_right");
        _soundEffectToggle.persistStateTarget = this;
		_soundEffectToggle.setTextOnOff("On", "Off");
		_contentLayer.addChild(_soundEffectToggle);
		_soundEffectToggle.addEventListener(GameTouchEvent.CLICK, handleSoundEffectClick);

		/**
		 * Language toggle
		 */

		_instructionsToggle = getToggleSwitch("Instructions_slide", "Instructions_slideBtn_left", "Instructions_slideBtn_right");
		_instructionsToggle.persistStateTarget = this;
		_instructionsToggle.setTextOnOff("On", "Off");
		_contentLayer.addChild(_instructionsToggle);
		_instructionsToggle.addEventListener(GameTouchEvent.CLICK, handleInstructionsClick);

		/**
		 * Buttons
		 */
		_teachersSettingsButton = getButton("Teacher_btn", "GreenButton_pressed");
		_avatarsButton = getButton("avatar_btn", "GreenButton_pressed");

		_avatarsButton.addEventListener(GameTouchEvent.CLICK, handleAvatarChangeClick);
		_teachersSettingsButton.addEventListener(GameTouchEvent.CLICK, handleTeacherClick);

		/**
		 * DropDown
		 */

		_languageDropDown = getDropDown("language_list", "revealList_top", "revealList_midle", "revealList_bottom", "lolanguage", "revealList_separator", "game3_settings");
        _languageDropDown.persistStateTarget = this;
        _contentLayer.addChild(_languageDropDown);
		_languageDropDown.initData([{text:'English', value:'EN'},{text:'Chinese', value:'CN'}]);
		_languageDropDown.addEventListener(GameEvent.ITEM_SELECTED, handleLangChange);
		_upperLayer.addChild(getSprite("language_button"));
		getSprite("language_button").touchable = false;

		/**
		 * Texts
		 */

		getTextField("Teacher").text = "TEACHERS SETTINGS";
		getTextField("avatar").text = "CHANGE AVATAR";
		getTextField("Teacher").fontSize /= 2
		getTextField("avatar").fontSize /= 2;

		getTextField("instructions").text = "Instructions";
		getTextField("sfx").text = "Sound Effects";
		getTextField("language").text = "Language";
	}

	private function handleSoundEffectClick(event : GameTouchEvent) : void
	{
		pm.changeSoundEffectProperty(_soundEffectToggle.selected);
	}

	private function handleInstructionsClick(event : GameTouchEvent) : void
	{
		pm.changeInstructionsProperty(_instructionsToggle.selected);
	}

	private function handleLangChange(event : GameEvent) : void
	{
		pm.changeLanguage(event.data as String);
	}

	private function handleTeacherClick(event : GameTouchEvent) : void
	{
		
	}

	private function handleAvatarChangeClick(event : GameTouchEvent) : void
	{
		pm.changeAvatar();
	}

}
}
