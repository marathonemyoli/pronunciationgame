
package com.emyoli.phonics.factories
{
import com.creativebottle.starlingmvc.events.BeanEvent;

import starling.events.EventDispatcher;

public class BeanInstanceFactory
{
	[Dispatcher]
	public var dispatcher : EventDispatcher;

	public function getBeanInstance (className : Class) : *
	{
		var instance : * = new className;
		dispatcher.dispatchEvent(new BeanEvent(BeanEvent.ADD_BEAN, instance));
		return instance;
	}

	public function initBeanInstance (instance : Object) : void
	{
		dispatcher.dispatchEvent(new BeanEvent(BeanEvent.ADD_BEAN, instance));
	}

}
}
