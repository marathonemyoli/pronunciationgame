package com.emyoli.phonics.utils {
import com.emyoli.phonics.models.GlobalParameters;
import com.emyoli.phonics.models.MessageModel;

import flash.geom.Point;
import flash.geom.Rectangle;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.display.Image;
import starling.display.Sprite;
import starling.display.Stage;
import starling.events.KeyboardEvent;

public class Utils {
	public function Utils()
	{
	}

	public static function trimExtension(string : String) : String
	{
		return string.substr(0, string.indexOf("."));
	}

	/**
	 * Moves displayObject depending on half of the difference between asset stage width and real stage width
	 * @param displayObject
	 * @param reverse Move object to opposite direction
	 */
	public static function proportionsRelativeMoveCenterX(displayObject : DisplayObject, reverse : Boolean = false) : void
	{
		var stage : Stage = Starling.current.stage;
		const proportionDifference : Number = stage.stageWidth /  GlobalParameters.AssetWidth;
		displayObject.x +=  (proportionDifference - 1) * GlobalParameters.AssetWidth / 2 * (reverse ? - 1 : 1);
	}

	/**
	 * Attaches displayObject to right side
	 * @param displayObject
	 * @param reverse Move object to opposite direction
	 */
	public static function proportionsRelativeMoveRightX(displayObject : DisplayObject, reverse : Boolean = false) : void
	{
		var stage : Stage = Starling.current.stage;
		displayObject.x = stage.stageWidth - GlobalParameters.AssetWidth;
	}

	/**
	 * Calculate position of object in percents (in case that AssetWidth = 100%), set x as a current stageWidth*percents
	 * @param displayObject
	 * @param expectCentered apply x compensation for objects in centered containers
	 */
	public static function proportionsRelativeSetJustifyX(displayObject : DisplayObject, expectCentered : Boolean = true) : void
	{
		var stage : Stage = Starling.current.stage;
		const percentage : Number = (displayObject.x + displayObject.width/2) /  GlobalParameters.AssetWidth;
		displayObject.x = stage.stageWidth * percentage - displayObject.width/2;
		if (expectCentered)
		{
			proportionsRelativeMoveCenterX(displayObject, true);
		}
	}

	/**
	 * Multiply width by scale
	 * @param displayObject
	 * @param expectCentered apply x compensation for objects in centered containers
	 */
	public static function proportionsRelativeIncreaseWidth(displayObject : DisplayObject, expectCentered : Boolean = true) : void
	{
		var stage : Stage = Starling.current.stage;
		const scale : Number = stage.stageWidth /  GlobalParameters.AssetWidth;
		displayObject.width *= scale;

		if (expectCentered)
		{
			proportionsRelativeMoveCenterX(displayObject, true);
		}
	}

	public static function convertToNativeStageCordinates(source : Point) : Point
	{
		var starlingViewPort:flash.geom.Rectangle = Starling.current.viewPort;
		var scale : Number = Starling.contentScaleFactor;
		var newX:Number = (source.x - starlingViewPort.x) * scale;
		var newY:Number = (source.y - starlingViewPort.y) * scale;
		return new Point(newX, newY);
	}

	public static function getSpriteClone (sprite : Sprite, zeroCoordinates : Boolean = true) : Sprite
	{
		var image : Image = sprite.getChildAt(0) as Image;
		if (image)
		{
			var clone : Sprite = new Sprite();
			clone.addChild(new Image(image.texture));
			clone.clipRect = sprite.clipRect;
			clone.x = zeroCoordinates ? 0 : sprite.x;
			clone.y = zeroCoordinates ? 0 : sprite.y;
			clone.pivotX = sprite.pivotX;
			clone.pivotY = sprite.pivotY;

			return clone;
		}

		return null;
	}

	public static function copyCoordinateAndSize(from : Sprite, to : Sprite) : void
	{
		copyCoordinates(from, to);
		copySize(from, to);
	}

	public static function copyCoordinates(from : Sprite, to : Sprite) : void
	{
		to.x = from.x;
		to.y = from.y;
	}

	public static function copySize(from : Sprite, to : Sprite) : void
	{
		to.width = from.width;
		to.height = from.height;
	}

	public static function buildErrorMessage(error : String) : MessageModel
	{
		return MessageModel.buildForNotification(localeGet("error"), error, localeGet("ok"));
	}

    public static function secondsToTimecode(seconds:Number):String
    {
        var minutes:Number          = Math.floor(seconds/60);
        var remainingSec:Number     = seconds % 60;
        var remainingMinutes:Number = minutes % 60;
        var hours:Number            = Math.floor(minutes/60);
        var floatSeconds:Number     = Math.floor((remainingSec - Math.floor(remainingSec))*100);
        remainingSec                = Math.floor(remainingSec);

        return (hours > 0 ? hours + ":" : "") + getTwoDigits(remainingMinutes) + ":" + getTwoDigits(remainingSec);
    }

	public static function timeCodeToSecondsMinutesArray(time:String):Array
    {
        const array : Array = time.split(":");
        return [int(array[0]), int(array[1])]
    }

    public static function secondsToSeparateSecondsAndMinutesArray(seconds:Number):Array
    {
        return Utils.timeCodeToSecondsMinutesArray(Utils.secondsToTimecode(seconds));
    }

    private static function getTwoDigits(number:Number):String
    {
        if (number < 10)
        {
            return "0" + number;
        }
        else
        {
            return number + "";
        }
    }

    public static function getNumberPostfix(inNumber : Number, inArrPostfix : Array = null):String
    {
        var aPostfix : Array = ["", "s"];
        if(inArrPostfix != null && inArrPostfix.length >= 2)
        {
            aPostfix = inArrPostfix;
        }
        if(Math.abs(inNumber) == 1)
        {
            return aPostfix[0];
        }
        return aPostfix[1];
    }

    public static function setControllKeyboard(inDisplayObject : DisplayObject, inWatch1 : DisplayObject, inWatch2 : DisplayObject) : void
    {
        inDisplayObject.addEventListener(KeyboardEvent.KEY_DOWN, function (e : KeyboardEvent)
        {
            var aStep : int = 10;
            var ss : Object = inWatch1;
            var s1 : Object = inWatch2;
            trace("keyCode", e.keyCode);

            if (e.keyCode == 38)
            {
                ss.y-=aStep;
            } else if (e.keyCode == 39)
            {
                ss.x+=aStep;
            } else if (e.keyCode == 40)
            {
                ss.y+=aStep;
            } else if (e.keyCode == 37)
            {
                ss.x-=aStep;
            }

            if (e.keyCode == 87)
            {
                s1.y-=aStep;
            } else if (e.keyCode == 83)
            {
                s1.y+=aStep;
            } else if (e.keyCode == 65)
            {
                s1.x+=aStep;
            } else if (e.keyCode == 68)
            {
                s1.x-=aStep;
            }
            trace("ss", ss.x, ss.y);
            trace("s1", s1.x, s1.y);
        });
    }
}
}
