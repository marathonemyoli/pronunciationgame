package com.emyoli.phonics.utils {
import flash.net.SharedObject;

public class SharedObjectManager {

	private static const CLEAR_ON_START : Boolean = false;

	public static var SHARED_OBJECT_NAME : String = "phonicsSO";

	public var so : SharedObject;

	[PostConstruct]
	public function setup() : void
	{
		so = SharedObject.getLocal(SHARED_OBJECT_NAME);
		if (CLEAR_ON_START)
		{
			clear();
			save();
		}
	}

	public function clear () : void
	{
		so.clear();
	}

	public function getData(_string : String) : Object
	{
		var _data : Object = so.data[_string];
		return _data;
	}

	public function setData(_key : String, _val : Object) : void
	{
		so.data[_key] = _val;
		save();
	}

	public function save() : void
	{
		so.flush();
	}

	public function hasData(s : String) : Boolean
	{
       return so.data.hasOwnProperty(s);
	}
}
}
