package com.emyoli.phonics.utils {
import com.emyoli.phonics.controllers.LocalizationController;

public function errorGet (value : String, parameters : Array = null) : String
{
	return LocalizationController.localeManager.getString(LocalizationController.ERORR_BUNDLE, value, parameters)
}
}
