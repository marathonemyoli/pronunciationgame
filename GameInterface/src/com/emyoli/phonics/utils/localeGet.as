package com.emyoli.phonics.utils {
import com.emyoli.phonics.controllers.LocalizationController;

public function localeGet (value : String, parameters : Array = null) : String
	{
		return LocalizationController.localeManager.getString(LocalizationController.TEXT_BUNDLE, value, parameters);
	}
}
