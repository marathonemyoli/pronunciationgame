package com.emyoli.phonics.interfaces {
import com.emyoli.phonics.views.BaseView;

public interface IPersistState {

    function get persistStateTarget () : BaseView;
    function set persistStateTarget (value : BaseView) : void;
}
}
