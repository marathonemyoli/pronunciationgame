
package com.emyoli.phonics.views {
import com.emyoli.phonics.controllers.AssetController;

import starling.filters.BlurFilter;
import starling.text.TextField;

public class BasePopupView extends BaseView {

	public function BasePopupView()
	{
		_drawHeader = false;

		super();
	}


	protected function setupPopupShadow() : void
	{
		this.filter = BlurFilter.createDropShadow(5, 2.75, 0, 0.35, 1);
	}

	protected function initPopupHeaderTextField () : void
	{
		/**
		 * Header text
		 */
		_headerText = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE,
				AssetController.DEFAULT_TEXT_WIDTH, AssetController.DEFAULT_TEXT_HEIGHT, 0xFFFFFF, AssetController.FONT_QUADON);
		_headerText.x = 200 +  HEADER_LABEL_GAP;
		_headerText.y = 15 + _headerText.height / 2;
		_headerText.nativeFilters = [AssetController.HEADER_LABEL_SHADOW_FILTER];
		//_textFields["Header"] = _headerText;

		_upperLayer.addChild(_headerText);
	}
}
}
