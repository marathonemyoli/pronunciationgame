package com.emyoli.phonics.views
{
public class AtlasParser extends BaseView
{
	public function AtlasParser()
	{
	}

	override protected function parseTextures (atlasName : String, visibleInstance : Boolean = true, additionalPostfixes : Vector.<String> = null) : void
	{
		super.parseTextures(atlasName, false, additionalPostfixes);
	}

	public function parse (name : String) : void
	{
		parseTextures(name);
	}
}
}
