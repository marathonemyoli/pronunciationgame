package com.emyoli.phonics.views
{
import com.creativearea.FormValidate;
import com.creativearea.ValidationRulesBuilder;
import com.emyoli.phonics.components.DropDownList;
import com.emyoli.phonics.components.ToggleSwitch;
import com.emyoli.phonics.components.TwoStateButton;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.events.NavigationEvent;
import com.emyoli.phonics.factories.BeanInstanceFactory;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.models.GlobalParameters;
import com.emyoli.phonics.models.ProportionsScaleMode;
import com.emyoli.phonics.utils.NativeText;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.utils.localeGet;

import flash.display.DisplayObject;
import flash.geom.Point;
import flash.geom.Rectangle;

import starling.core.Starling;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;

import starling.display.Image;

import starling.display.Sprite;
import starling.events.Event;
import starling.events.EventDispatcher;
import starling.text.TextField;
import starling.textures.Texture;
import starling.textures.TextureAtlas;

public class BaseView extends Sprite
{
	public static var HOME_BUTTON_DEFAULT_VIEW : Class;

	protected const HEADER_LABEL_GAP : Number = 10;

	private const TEXTS_POSTFIX : String = "_texts";

	private const TEXT_FIELD : String = "textField";
	private const TEXT_FIELD_BLACK : String = "textFieldBlack";
	private const TEXT_HEADER : String = "headerText";
	private const TEXT_INPUT : String = "textInput";
	private const TEXT_BUTTON_LABEL : String = "buttonLabel";

	private const DEFAULT_BUTTON_CONTAINER : String = "Base";

	private var _textFields : Object = {};
	private var _textInputs : Object = {};
	private var _sprites : Object = {};
	private var _images : Object = {};

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[Inject]
	public var beanFactory : BeanInstanceFactory;

	[Inject]
	public var assetController : AssetController;

	[Inject (source="gameModel")]
	public var gameModel : GameModelRepo;

	private var _nativeChilds : Vector.<flash.display.DisplayObject> = new Vector.<flash.display.DisplayObject>();

	private var _graphicsXML : XML

	protected var _atlasName : String;
	protected var _drawHeader : Boolean = true;
	protected var _drawBackground : Boolean = true;
	protected var _proportionsScaleMode : int = ProportionsScaleMode.MIDDLE;
	protected var _homeButtonClass : Class;

	protected var _divider : Image;
	protected var _background : Image;

	protected var _header : Sprite;
	protected var _headerDivider : Sprite;
	protected var _homeButton : Sprite;

	protected var _headerText : TextField;
	protected var _backgroundSprite : Sprite;
	protected var _bgLayer : Sprite;
	protected var _contentLayer : Sprite;
	protected var _upperLayer : Sprite;

	protected var _validator : FormValidate;

    public var data : Object = null;

			public function BaseView()
	{
		super();

		addEventListener(Event.ADDED_TO_STAGE, handleViewAdded);
		addEventListener(Event.REMOVED_FROM_STAGE, handleViewRemoved);
	}

	public function init() : void
	{
		initLayers();
	}

	private function initLayers() : void
	{
		_bgLayer = new Sprite();
		_contentLayer = new Sprite();
		_upperLayer = new Sprite();

		switch (_proportionsScaleMode)
		{
			case ProportionsScaleMode.MIDDLE:
				Utils.proportionsRelativeMoveCenterX(_upperLayer);
				Utils.proportionsRelativeMoveCenterX(_contentLayer);
				Utils.proportionsRelativeMoveCenterX(_bgLayer);
				break;
			case ProportionsScaleMode.RIGHT:
				Utils.proportionsRelativeMoveRightX(_upperLayer);
				Utils.proportionsRelativeMoveRightX(_contentLayer);
				Utils.proportionsRelativeMoveRightX(_bgLayer);
				break;
		}

		super.addChild(_bgLayer);
		super.addChild(_contentLayer);
		super.addChild(_upperLayer);

		if (_drawBackground)
		{
			initBackground();
		}

		if (_drawHeader)
		{
			initHeader();
		}
	}

	protected function initBackground () : void
	{
		_background = assetController.getImage("Background");
		_backgroundSprite = new Sprite();
		_backgroundSprite.addChild(_background);
		_backgroundSprite.height = Starling.current.stage.stageHeight;
		_divider = assetController.getImage("Divider");
		_bgLayer.addChild(_backgroundSprite);
		_bgLayer.addChild(_divider);

		switch (_proportionsScaleMode)
		{
			case ProportionsScaleMode.MIDDLE:
				_backgroundSprite.width = Starling.current.stage.stageWidth;
				Utils.proportionsRelativeMoveCenterX(_backgroundSprite, true);
				break;
			case ProportionsScaleMode.RIGHT:
				_backgroundSprite.width = GlobalParameters.AssetWidth;
				break;
			case ProportionsScaleMode.LEFT:
				_backgroundSprite.width = GlobalParameters.AssetWidth;
				break;
			case ProportionsScaleMode.NONE:
				_backgroundSprite.width = GlobalParameters.StageWidth;
				break;
		}
	}

	protected function initHeader () : void
	{
		_header = assetController.getSpriteCloneFrom("Base", "Header", false);
		_headerDivider = assetController.getSpriteCloneFrom("Base", "HeaderDivider", false);
		_homeButton = assetController.getSpriteCloneFrom("Base", "HomeButton", false);

		//_upperLayer.clipRect = new flash.geom.Rectangle(0, 0, Starling.current.stage.stageWidth, _header.texture.height);

		/**
		 * Header text
		 */
		_headerText = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE,
				AssetController.DEFAULT_TEXT_WIDTH, AssetController.DEFAULT_TEXT_HEIGHT, 0xFFFFFF, AssetController.FONT_QUADON);
		_headerText.x = 105 + HEADER_LABEL_GAP;
		_headerText.y = 5 + _headerText.height / 2;
		_headerText.nativeFilters = [AssetController.HEADER_LABEL_SHADOW_FILTER];
		_textFields["Header"] = _headerText;

		switch (_proportionsScaleMode)
		{
			case ProportionsScaleMode.MIDDLE:
				Utils.proportionsRelativeMoveCenterX(_headerText, true);
				Utils.proportionsRelativeMoveCenterX(_homeButton, true);
				Utils.proportionsRelativeMoveCenterX(_headerDivider, true);
				Utils.proportionsRelativeMoveCenterX(_header, true);
				_header.width = stage.stageWidth;
				break;

			case ProportionsScaleMode.NONE:
				_header.width = stage.stageWidth;
				break;
		}

		_upperLayer.addChild(_header);
		_upperLayer.addChild(_headerDivider);
		_upperLayer.addChild(_homeButton);
		_upperLayer.addChild(_headerText);

		_homeButton.addEventListener(GameTouchEvent.CLICK, handleHomeClick);
	}

	protected function parseTextures (atlasName : String, visibleInstance : Boolean = true, additionalPostfixes : Vector.<String> = null) : void
	{
		_atlasName = atlasName;

		name = atlasName;

		trace("// Atlas " + _atlasName + "\n\n");

		if (!additionalPostfixes)
		{
			parseTexture (atlasName, visibleInstance);
		}
		for each (var postfix : String in additionalPostfixes)
		{
			parseTexture (atlasName + "_" + postfix, visibleInstance);
		}

		parseTexts(atlasName, visibleInstance);

		trace("\n\n");

		assetController.addLibrary(atlasName, _sprites, _textFields, _textInputs, _images);
	}

	protected function parseTexture (atlasName : String, visibleInstance : Boolean = true) : void
	{
		//trace("// Texture keys:\n");
		var atlas : TextureAtlas = assetController.assetsManager.getTextureAtlas(atlasName);
		_graphicsXML = assetController.assetsManager.getXml(atlasName);
		var texture : Texture;
		for each (var node : XML in _graphicsXML.children())
		{
			//trace(node.@name + " = \n");
			var skip : Boolean = false;
			for each (var textPostfix : String in [TEXT_BUTTON_LABEL, TEXT_FIELD, TEXT_FIELD_BLACK,TEXT_HEADER, TEXT_INPUT])
			{
				if (String(node.@name).search(textPostfix) != -1)
				{
					skip = true;
				}
			}
			if (!skip)
			{
				texture = atlas.getTexture(node.@name);
				var image : Image = new Image(texture);
				var sprite : Sprite = new Sprite();
				sprite.addChild(image);
				sprite.clipRect = new flash.geom.Rectangle(-getNodeCoordinate(node, "@frameX"), -getNodeCoordinate(node, "@frameY"), texture.width, texture.height);
				sprite.pivotX = -getNodeCoordinate(node, "@frameX");
				sprite.pivotY = -getNodeCoordinate(node, "@frameY");
				sprite.x = sprite.pivotX;
				sprite.y = sprite.pivotY;
				sprite.name = node.@name;
				if (visibleInstance)
				{
					_contentLayer.addChild(sprite);
				}
				_sprites[String(node.@name)] = sprite;
				_images[String(node.@name)] = image;
			}
		}
	}

	protected function parseTexts (atlasName : String, visibleInstance : Boolean = true) : void
	{

		var textXml : XML = assetController.assetsManager.getXml(atlasName + TEXTS_POSTFIX);
		if (!textXml)
		{
			textXml = _graphicsXML;
		}

		for each (var node : XML in textXml.children())
		{
			const name : String = String(node.@name);
			const nameParts : Array = name.split("_");
			const textType : String = nameParts.pop();
			const textName : String = nameParts.join("_");
			var textField : Object = null; /*Can be starling/flash DisplayObject*/
			var starlingText : TextField;
			switch (textType)
			{
				case TEXT_FIELD :
					starlingText = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE, getNodeCoordinate(node, "@width"),
							getNodeCoordinate(node, "@height"), 0xFFFFFF, AssetController.FONT_NOVA);
					starlingText.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];
					textField = starlingText;
					break;
				case TEXT_FIELD_BLACK :
					textField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE, getNodeCoordinate(node, "@width"),
							getNodeCoordinate(node, "@height"), 0x03364b, AssetController.FONT_NOVA, true);
					break;
				case TEXT_HEADER :
					starlingText = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE, getNodeCoordinate(node, "@width"),
							getNodeCoordinate(node, "@height"), 0xFFFFFF, AssetController.FONT_QUADON);
					starlingText.nativeFilters = [AssetController.HEADER_LABEL_SHADOW_FILTER];
					textField = starlingText;
					break;
				case TEXT_INPUT :
					textField = assetController.getTextInput(AssetController.DEFAULT_FONT_SIZE, getNodeCoordinate(node, "@width"), 0x03364b, AssetController.FONT_NOVA);
					break;
				case TEXT_BUTTON_LABEL :
					starlingText = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1.25, getNodeCoordinate(node, "@width"),
							getNodeCoordinate(node, "@height"), 0xFFFFFF, AssetController.FONT_HOLTWOOD, true);
					starlingText.nativeFilters = [AssetController.BUTTON_LABEL_SHADOW_FILTER, AssetController.BUTTON_LABEL_BEVEL_FILTER,
						AssetController.BUTTON_LABEL_GLOW_FILTER];
					textField = starlingText;
					break;

				default :
					break;
			}

			if (textField)
			{
				trace(_atlasName + "." + name + " = \n");

				textField.x = -getNodeCoordinate(node, "@frameX");
				textField.y = -getNodeCoordinate(node, "@frameY");

				if (textField is NativeText)
				{
					if (visibleInstance)
					{
						var relativePoint : Point = _contentLayer.localToGlobal(new Point(textField.x, textField.y));
						var stagePoint : Point = Utils.convertToNativeStageCordinates(relativePoint);
						textField.x = stagePoint.x;
						textField.y = stagePoint.y + 20 * Starling.contentScaleFactor;
						addNativeChild(textField as NativeText);
					}
					_textInputs[textName] = textField;
				}else
				{
					if (visibleInstance)
					{
						_contentLayer.addChild(textField as starling.display.DisplayObject);
					}
					textField.touchable = false;
					_textFields[textName] = textField;
					_textFields[name] = textField;
				}
			}

		}
	}

	private function getNodeCoordinate(node : XML, s : String) : Number
	{
		return Number(node[s]) / GlobalParameters.ASSET_SCALE_FACTOR;
	}

	protected function getSprite(id : String) : Sprite
	{
		return _sprites[id] as Sprite;
	}

	protected function getImage(id : String) : Image
	{
		return _images[id] as Image;
	}

	protected function getTextField(id : String) : TextField
	{
		return _textFields[id] as TextField;
	}

	protected function getTextInput(id : String) : NativeText
	{
		return _textInputs[id] as NativeText;
	}

	protected function addNativeChild (displayObject : flash.display.DisplayObject) : void
	{
		_nativeChilds.push(displayObject);
		Starling.current.nativeOverlay.addChild(displayObject);
	}

	protected function getButton(id : String, pressedStateId : String = "", pressedStateContainer : String = "") : TwoStateButton
	{
		const upState : Sprite = _sprites[id] as Sprite;
		var pressedState : Sprite;
		if (pressedStateContainer && pressedStateContainer == _atlasName)
		{
			pressedState = getSprite(pressedStateId);
		}else
		{
			pressedState = pressedStateId
					? assetController.getSpriteCloneFrom(pressedStateContainer ? pressedStateContainer : DEFAULT_BUTTON_CONTAINER, pressedStateId)
					: null;
		}

		if (pressedState && id != pressedStateId)
		{
			pressedState.x = 0;
			pressedState.y = 0;
			pressedState.width = upState.width;
			pressedState.height = upState.height;
		}
		var container : DisplayObjectContainer = upState.parent;
		var childId : int = container.getChildIndex(upState);
		var button : TwoStateButton = beanFactory.getBeanInstance(TwoStateButton) as TwoStateButton;
		button.init(upState, pressedState);

		button.x = upState.x;
		button.y = upState.y;

		upState.x = 0;
		upState.y = 0;


		container.addChildAt(button, childId);

		return button;
	}

	protected function getDropDown(id : String, upperRenderer : String, middleRenderer : String, bottomRenderer : String, titleField : String, separator : String, container : String = "") : DropDownList
	{
		if (!container)
		{
			container = _atlasName;
		}

		const upStatePlaceHolder : Sprite = _sprites[id] as Sprite;
		const title : TextField = _textFields[titleField] as TextField;
		var upState : Sprite = assetController.getSpriteCloneFrom(container, id, true, true);
		if (!upState)
		{
			upState = upStatePlaceHolder;
		}
		const upperSprite : Sprite = assetController.getSpriteCloneFrom(container, upperRenderer, true, true);
		const middleSprite : Sprite = assetController.getSpriteCloneFrom(container, middleRenderer, true, true);
		const bottomSprite : Sprite = assetController.getSpriteCloneFrom(container, bottomRenderer, true, true);
		const separatorSprite : Sprite = assetController.getSpriteCloneFrom(container, separator, true, true);

		var dropDown : DropDownList = beanFactory.getBeanInstance(DropDownList) as DropDownList;

		if (upStatePlaceHolder)
		{
			dropDown.x = upStatePlaceHolder.x;
			dropDown.y = upStatePlaceHolder.y;
			upState.x = upState.y = 0;
		}
		dropDown.init(upState, title, upperSprite, middleSprite, bottomSprite, separatorSprite);
		return dropDown;
	}

	protected function getToggleSwitch (id : String, toggleOnState : String, toggleOffState : String, container : String = "") : ToggleSwitch
	{
		if (!container)
		{
			container = _atlasName;
		}

		var placeHolderSprite : Sprite =  _sprites[id] as Sprite;
		var bgSprite : Sprite =  assetController.getSpriteCloneFrom(container, id, false, true);
		var onSprite : Sprite =  assetController.getSpriteCloneFrom(container, toggleOnState, false, true);
		var offSprite : Sprite =  assetController.getSpriteCloneFrom(container, toggleOffState, false, true);

        var button : ToggleSwitch = beanFactory.getBeanInstance(ToggleSwitch) as ToggleSwitch;
        button.init(onSprite, offSprite, bgSprite);

		if (placeHolderSprite)
		{
			onSprite.x -= placeHolderSprite.x;
			onSprite.y -= placeHolderSprite.y;
			offSprite.x -= placeHolderSprite.x;
			offSprite.y -= placeHolderSprite.y;
			button.x = placeHolderSprite.x;
			button.y = placeHolderSprite.y;
			placeHolderSprite.visible = false;

			bgSprite.x = bgSprite.y = 0;
		}

		return button;
	}

	protected function handleViewRemoved (event : Event) : void
	{
		for each (var child : flash.display.DisplayObject in _nativeChilds)
		{
			Starling.current.nativeOverlay.removeChild(child);
		}
	}

	protected function handleViewAdded (event : Event) : void
	{
		for each (var child : flash.display.DisplayObject in _nativeChilds)
		{
			Starling.current.nativeOverlay.addChild(child);
		}
	}

	protected function handleHomeClick(event : GameTouchEvent) : void
	{
		event.stopImmediatePropagation();
		if (!_homeButtonClass || !(this is _homeButtonClass))
		{
			dispatcher.dispatchEvent(new NavigationEvent(NavigationEvent.NAVIGATE_TO_VIEW,
					_homeButtonClass ? _homeButtonClass : HOME_BUTTON_DEFAULT_VIEW));
		}
	}

	public final override function addChild (displayObject : starling.display.DisplayObject) : starling.display.DisplayObject
	{
		throw new Error("Add child into one of three layers: _backgroundLayer, _contentLayer, _upperLayer");
	}

	public function set validationRules (values : ValidationRulesBuilder) : void
	{
		values.validationObject.msg = values.validationObject.msg ?  values.validationObject.msg : localeGet("error");
		_validator = new FormValidate(_textInputs, values.validationObject);
	}
}
}
