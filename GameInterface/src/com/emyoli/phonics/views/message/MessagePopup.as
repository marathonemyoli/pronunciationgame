package com.emyoli.phonics.views.message {
import com.emyoli.phonics.components.TwoStateButton;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.events.MessageEvent;
import com.emyoli.phonics.models.MessageModel;
import com.emyoli.phonics.models.ProportionsScaleMode;
import com.emyoli.phonics.utils.localeGet;
import com.emyoli.phonics.views.BasePopupView;

import flash.events;
import flash.events.TimerEvent;

import flash.events.TouchEvent;
import flash.utils.Timer;

import org.gestouch.core.Gestouch;
import org.gestouch.input.NativeInputAdapter;

import starling.core.Starling;

import starling.display.Sprite;
import starling.events.Event;
import starling.events.TouchEvent;

import starling.text.TextField;
import starling.utils.HAlign;

public class MessagePopup extends BasePopupView {

	private var _contentText : TextField;

	[Inject]
	public var pm : MessagePopupPM;

	private var _model : MessageModel;

	private var _okButton : TwoStateButton;
	private var _cancelButton : TwoStateButton;
	private var _closeButton : Sprite;
    private var _hideTimer : Timer;

	public function MessagePopup(model : MessageModel = null)
	{
		super();

        _model = model;

        addEventListener(Event.ADDED_TO_STAGE, handleAdded);
	}

	[PostConstruct]
	public override function init() : void
	{
		_proportionsScaleMode = ProportionsScaleMode.NONE;
		_drawHeader = false;
		_drawBackground = false;

		super.init();

		parseTextures("message", true, new <String>["blue", "green", "red"]);

		_okButton = getButton("message_okBtn_normal", "message_okBtn_selected", _atlasName);
		_contentLayer.addChild(_okButton);

		/**
		 * CancelButton
		 */
		const upState : Sprite = assetController.getSpriteCloneFrom(_atlasName, "message_okBtn_normal", false);
		const downState : Sprite = assetController.getSpriteCloneFrom(_atlasName, "message_okBtn_selected", false);
		_cancelButton = new TwoStateButton();
		_cancelButton.init(upState, downState);
		_contentLayer.addChild(_cancelButton);
		_cancelButton.y = _okButton.y;
		_cancelButton.visible = false;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		_closeButton = getSprite("message_closeBtn");
		_contentLayer.addChild(_closeButton);

		_okButton.addEventListener(GameTouchEvent.CLICK, handleOkClick);
		_cancelButton.addEventListener(GameTouchEvent.CLICK, handleCancelClick);
		_closeButton.addEventListener(GameTouchEvent.CLICK, handleCancelClick);
		addEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);

		//(Gestouch.inputAdapter as NativeInputAdapter).onDispose();

		_headerText = getTextField("message_title");
		_headerText.fontSize *= 1.4;
		_headerText.nativeFilters = [AssetController.HEADER_LABEL_SHADOW_FILTER];

		_contentText = getTextField("message_body");
		_contentText.hAlign = HAlign.CENTER;
		_contentText.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];

		(new <String>[MessageModel.TYPE_BLUE, MessageModel.TYPE_GREEN, MessageModel.TYPE_RED]).forEach(function(value : String, ...rest){
            getSprite(value).visible = false;
        });

        setupMessageWindow();
        setupCloseTimer();
	}

    private function setupCloseTimer() : void
    {
        if (_model.hideTime)
        {
            _hideTimer = new Timer(_model.hideTime * 1000, 1);
            _hideTimer.addEventListener(TimerEvent.TIMER_COMPLETE, handleHideTimer);
            _hideTimer.start();
        }
    }

    private function handleHideTimer(event : TimerEvent) : void
    {
        _hideTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, handleHideTimer);
        pm.ok(_model, data);
        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CLOSE, _model, this));
    }

	private function setupMessageWindow () : void
	{
		_cancelButton.visible = _model.showCancel;

		if (_model.showCancel)
		{
			_cancelButton.x = width * 3/4 - _cancelButton.width/2;
			_okButton.x = width * 1/4 - _okButton.width/2;
		}

		_okButton.text = _model.buttonOkLabel ? _model.buttonOkLabel : localeGet("ok");
		_cancelButton.text = _model.buttonCancelLabel ?  _model.buttonCancelLabel :  localeGet("cancel");;
		_headerText.text = _model.headerText;
		_contentText.text = _model.contentText;

		_okButton.visible = _model.buttonOkLabel;

		getSprite(_model.messageType).visible = true;
	}

	private function handleCancelClick(event : GameTouchEvent) : void
	{
		event.stopImmediatePropagation();
		pm.cancel(_model, data);
		dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CLOSE, _model, this));
	}

	private function handleOkClick(event : GameTouchEvent) : void
	{
		event.stopImmediatePropagation();
		pm.ok(_model, data);
		dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CLOSE, _model, this));
	}

	private function handleRemovedFromStage(event : Event) : void
	{
		_okButton.removeEventListener(GameTouchEvent.CLICK, handleOkClick);
		_cancelButton.removeEventListener(GameTouchEvent.CLICK, handleCancelClick);
		_closeButton.removeEventListener(GameTouchEvent.CLICK, handleCancelClick);
		removeEventListener(Event.REMOVED_FROM_STAGE, handleCancelClick);

		//(Gestouch.inputAdapter as NativeInputAdapter).init();
	}

    private function handleAdded(event : Event) : void
    {
        removeEventListener(Event.ADDED_TO_STAGE, handleAdded);

        const backGround : Sprite = getSprite(MessageModel.TYPE_BLUE);
        x = (stage.stageWidth - backGround.width) / 2 - backGround.x;
        y = (stage.stageHeight - backGround.height) / 2 - backGround.y;
    }

    public function set model(value : MessageModel) : void
    {
        _model = value;
    }
}
}
