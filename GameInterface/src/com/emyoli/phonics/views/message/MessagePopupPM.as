
package com.emyoli.phonics.views.message {
import com.emyoli.phonics.events.MessageEvent;
import com.emyoli.phonics.models.MessageModel;

import starling.events.EventDispatcher;

public class MessagePopupPM {

	[Dispatcher]
	public var dispatcher: EventDispatcher;

	public function MessagePopupPM()
	{
	}


	public function cancel(model : MessageModel, data : Object) : void
	{
		dispatcher.dispatchEvent(new MessageEvent(MessageEvent.NO_SELECTED, model, data));
	}

	public function ok(model : MessageModel, data : Object) : void
	{
		dispatcher.dispatchEvent(new MessageEvent(MessageEvent.YES_SELECTED, model, data));
	}
}
}
