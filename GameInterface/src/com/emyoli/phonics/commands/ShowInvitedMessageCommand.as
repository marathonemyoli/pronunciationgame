package com.emyoli.phonics.commands {
import com.emyoli.phonics.events.MessageEvent;
import com.emyoli.phonics.models.MessageModel;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.InvitedToGame;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.net.ResponseToInvite;

import starling.events.Event;
import starling.events.EventDispatcher;


public class ShowInvitedMessageCommand {
	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[Inject]
	public var connector : NetConnector;

	[Execute]
	public function execute(event : Event) : void
	{
		var data : InvitedToGame = event.data as InvitedToGame;

		var message : MessageModel = new MessageModel();
		message.headerText = "Invitation";
		message.contentText = "Somebody invited you, ok?";
		message.buttonCancelLabel = "No";
		message.buttonOkLabel = "Yes";
		message.showCancel = true;

		dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CREATE_MESSAGE, message, data));
	}

	[EventHandler(event="MessageEvent.YES_SELECTED", properties="messageModel, data")]
	public function yesMessageHandler(messageModel : MessageModel, data : InvitedToGame) : void
	{
		const response : ResponseToInvite = new ResponseToInvite();
		response.accepted = true;
		response.gameID = data.game.id;

		connector.sendMessage(response, CmdType.ResponseToInvite);
	}

	[EventHandler(event="MessageEvent.NO_SELECTED", properties="messageModel, data")]
	public function noMessageHandler(messageModel : MessageModel, data : InvitedToGame) : void
	{
		const response : ResponseToInvite = new ResponseToInvite();
		response.accepted = false;
		response.gameID = data.game.id;

		connector.sendMessage(response, CmdType.ResponseToInvite);
	}

}

}
