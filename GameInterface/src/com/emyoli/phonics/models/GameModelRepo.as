package com.emyoli.phonics.models {
import com.creativebottle.starlingmvc.binding.Bindings;
import com.emyoli.phonics.net.AvailableFriendsList;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.GameSetting;
import com.emyoli.phonics.net.GetAvatarName;
import com.emyoli.phonics.net.LoginResp;
import com.emyoli.phonics.net.MyScoreBoard;
import com.emyoli.phonics.net.OnlinePlayers;
import com.emyoli.phonics.net.SignupResp;
import com.emyoli.phonics.net.UserInfo;
import com.emyoli.phonics.net.UserSetting;
import starling.events.EventDispatcher;

public class GameModelRepo{

	[Bindings]
	public var bindings : Bindings;

	[Inject]
	public var userSetting : UserSetting;


	[Inject]
	public var onlinePlayers : OnlinePlayers;

	[Inject]
	public var myScore : MyScoreBoard;

	[Bindable]
	public var loggedIn : Boolean = false;

	public var gameSetting : GameSetting;

	public function GameModelRepo()
	{

	}

	[EventHandler (event="NetLoginEvent.LOGIN", properties="response")]
	public function handleSignupSuccess (data : LoginResp) : void
	{
		userSetting = data.data;
		loggedIn = true;
	}

	[EventHandler (event="CmdType.Logout", properties="response")]
	public function handleLogout (data : LoginResp) : void
	{
		loggedIn = false;
	}

	[EventHandler (event="NetSignupEvent.SIGNUP", properties="response")]
	public function handleLoginSuccess (data : SignupResp) : void
	{
		userSetting = data.data;
		loggedIn = true;
	}

	[EventHandler (event="GameEvent.AVATAR_SELECTED", properties="data")]
	public function handleAvatarSet (data : GetAvatarName) : void
	{
		userSetting.avatarname = data.name;
		bindings.invalidate(this, "userSetting");
	}

	[EventHandler (event="CmdType.OnlinePlayers", properties="data")]
	public function handleAvailableFriendsList (data : OnlinePlayers) : void
	{
		for each (var user : UserInfo in data.data)
		{
			if (user.id == userSetting.id)
			{
				data.data.splice(data.data.indexOf(user), 1);
			}
		}
		onlinePlayers = data;
	}

	[EventHandler (event="GameEvent.USER_SETTING_CHANGE", properties="data")]
	public function handleUserSettingChange (data : UserSetting) : void
	{
		userSetting = data;
	}

	[EventHandler (event="CmdType.MyScoreBoard", properties="data")]
	public function handleMyScoreChange (data : MyScoreBoard) : void
	{
		//myScore = TestData.GET_MYSCORE_LIST;
	}

}
}
