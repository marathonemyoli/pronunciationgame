package com.emyoli.phonics.components {

import com.emyoli.phonics.controllers.SoundController;

import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.utils.localeGet;

import flash.events.KeyboardEvent;

import flash.geom.Matrix3D;
import flash.text.TextField;
import flash.ui.Keyboard;

import org.gestouch.events.GestureEvent;

import org.gestouch.gestures.SwipeGesture;
import org.gestouch.gestures.SwipeGestureDirection;
import org.gestouch.gestures.TapGesture;

import starling.animation.Juggler;
import starling.core.Starling;
import starling.display.DisplayObject;

import starling.display.Image;

import starling.display.Sprite3D;
import starling.events.Event;
import starling.text.TextField;
import starling.textures.Texture;
import starling.utils.HAlign;
import starling.utils.VAlign;


public class GameCube extends Sprite3D {

    public static const EVENT_REMOVED : String = "EVENT_REMOVED";
    public static const EVENT_SELECTED : String = "EVENT_SELECTED";
    public static const EVENT_DESELECT : String = "EVENT_DESELECT";

	public static const SIZE : Number = 300;

	[Inject]
	public var assetController : AssetController;

	[Juggler]
	public var juggler : Juggler;

    [Inject]
    public var soundController : SoundController;

	private var _leftSide : Sprite3D;
	private var _rightSide : Sprite3D;
	private var _upperSide : Sprite3D;
	private var _bottomSide : Sprite3D;
	private var _middleSide : Sprite3D;
	private var _middleBackSide : Sprite3D;

	private var _objects : Vector.<Sprite3D>
	private var _selected : Boolean;

    private var _state : int = STATE_INACTIVE;

    public static const STATE_INACTIVE : int = 0;
    public static const STATE_FRONT : int = 1;
    public static const STATE_LEFT : int = 2;
    public static const STATE_BACK : int = 3;
    public static const STATE_RIGHT : int = 4;
    public static const STATE_WIN : int = 5;
    public static const STATE_REMOVE : int = 6;

    private var _rendering : Boolean = false;

    public var data : Object = null;

    public var sayWord : String = "";
	private const TAG_FONT_RED : String = "<font color='#dc3f0c'>";
	private const TAG_R : String = "<r>";
	private const TAG_FONT_CLOSED : String = "</font>";
	private const TAG_R_CLOSED : String = "</r>";

	public function GameCube(inObj : Object)
	{
		super();
        data = inObj;

        var aTF:flash.text.TextField = new flash.text.TextField();
        aTF.htmlText = data.text;
        sayWord = aTF.text;
	}

	[PostConstruct]
	public function init() : void
	{
        _rendering = false;
		var backTexture : Texture = assetController.assetsManager.getTexture("cubeBack_back");
		var baseTexture : Texture = assetController.assetsManager.getTexture("cubeFace_back");
		var playTexture : Texture = assetController.assetsManager.getTexture("cubePlay_icon");
		var recordTexture : Texture = assetController.assetsManager.getTexture("cubeRecord_icon");

		var customImageTexture : Texture = assetController.assetsManager.getTexture(data["visual"]);



		var leftSideImage : Image = new Image(baseTexture);
		var rightSideImage : Image = new Image(baseTexture);
		var upperSideImage : Image = new Image(baseTexture);
		var bottomSideImage : Image = new Image(baseTexture);
		var middleBackSideImage : Image = new Image(baseTexture);
		var middleSideImage : Image = new Image(backTexture);



		var playImage : Image = new Image(playTexture);
		var recordImage : Image = new Image(recordTexture);
		var customImage : Image = new Image(customImageTexture);

        (new <Image>[leftSideImage, rightSideImage, upperSideImage, bottomSideImage
            , middleBackSideImage, middleSideImage, playImage, recordImage, customImage]).forEach(function(value : Image, ...rest){
            value.width = SIZE;
            value.height = SIZE;
        });

		_leftSide = new Sprite3D();
		_rightSide = new Sprite3D();
		_upperSide = new Sprite3D();
		_bottomSide = new Sprite3D();
		_middleSide = new Sprite3D();
		_middleBackSide = new Sprite3D();

		_leftSide.addChild(leftSideImage);
		_leftSide.addChild(playImage);
		_rightSide.addChild(rightSideImage);
		_upperSide.addChild(upperSideImage);
		_bottomSide.addChild(bottomSideImage);
		_bottomSide.addChild(recordImage);
		_middleSide.addChild(middleSideImage);
		_middleBackSide.addChild(middleBackSideImage);

		_upperSide.addChild(customImage);

		/*_leftSide.alignPivot();
		_rightSide.alignPivot();
		_upperSide.alignPivot();
		_bottomSide.alignPivot();
		_middleSide.alignPivot();
		_middleBackSide.alignPivot();*/

		_leftSide.addChild(getMiddleSprite(_leftSide));
		_rightSide.addChild(getMiddleSprite(_rightSide));
		_upperSide.addChild(getMiddleSprite(_upperSide));
		_bottomSide.addChild(getMiddleSprite(_bottomSide));
		_middleSide.addChild(getMiddleSprite(_middleSide));
		_middleBackSide.addChild(getMiddleSprite(_middleBackSide));

		_leftSide.rotationZ = Math.PI / 2;
		_leftSide.rotationX = Math.PI / 2;
		_leftSide.x = -SIZE / 2;
		_leftSide.y = -SIZE / 2;
		_leftSide.z = -SIZE / 2;

		_rightSide.rotationZ = Math.PI / 2;
		_rightSide.rotationX = Math.PI / 2;
		_rightSide.scaleX = -1;
		_rightSide.x = SIZE / 2;
		_rightSide.y = SIZE / 2;
		_rightSide.z = -SIZE / 2;


		_upperSide.rotationX = Math.PI / 2;
        _upperSide.scaleX = -1;
		_upperSide.x = SIZE / 2;
		_upperSide.y = -SIZE / 2;
		_upperSide.z = -SIZE / 2;

		_bottomSide.rotationX = Math.PI / 2;
		_bottomSide.x = -SIZE / 2;
		_bottomSide.y = SIZE / 2;
		_bottomSide.z = -SIZE / 2;

		_middleSide.y = -SIZE / 2;
		_middleSide.x = -SIZE / 2;
		_middleSide.z = -SIZE / 2;

		_middleBackSide.y = -SIZE / 2;
		_middleBackSide.x = -SIZE / 2;
		_middleBackSide.z = SIZE / 2;

		addChild(_middleBackSide);
		addChild(_leftSide);
		addChild(_rightSide);
		addChild(_upperSide);
		addChild(_bottomSide);
		addChild(_middleSide);

		_middleBackSide.name = "back";
		_middleSide.name = "front";

		_objects = new <Sprite3D>[_middleBackSide, _leftSide, _rightSide, _upperSide, _bottomSide, _middleSide];

		initWord();

		addEventListener(Event.ENTER_FRAME, handleEnterFrame);

        dispatchEvent(new Event(Event.COMPLETE));
	}



	private function getMiddleSprite(container : Sprite3D) : DisplayObject
	{
		var sprite : Sprite3D = new Sprite3D();
		sprite.name = "middle";
		sprite.x = container.width / 2;
		sprite.y = container.height / 2;
		return sprite;
	}

	public function handleTapGesture(event : GestureEvent) : Boolean
	{
        switch (_state){
            case STATE_FRONT:
                return false;
                break;
            case STATE_LEFT:
                soundController.play(data["audio"]);
                break;
        }
        return true;
	}

    //------------------------------------------------------------------------------

	public function rotateRight() : void
	{
        setVisible(true);
		juggler.tween(this, 0.5, {	rotationY : Math.round((this.rotationY + -Math.PI / 2)/ (Math.PI / 2)) * (Math.PI / 2), onComplete: afterAnimation});
        _state++;
        if(_state > STATE_RIGHT)
        {
            _state = STATE_FRONT;
        }
	}

	public function rotateLeft() : void
	{
        setVisible(true);
		juggler.tween(this, 0.5, {	rotationY : Math.round((this.rotationY + Math.PI / 2) / (Math.PI / 2)) * (Math.PI / 2), onComplete: afterAnimation});
        _state--;
        if(_state < STATE_FRONT)
        {
            _state = STATE_RIGHT;
        }
	}
	public function selectCube() : void
	{
        var reSize : Number = 1.8;
        setVisible(true);
		juggler.tween(this, 0.5, {	rotationX : -Math.PI / 2, scaleX : reSize, scaleY : reSize, scaleZ : reSize, onComplete: function(){
            afterAnimation();
            dispatchEvent(new Event(EVENT_SELECTED));
        }});
        _state = STATE_FRONT;
    }

    public function bay() : void
    {
        var reSize : Number = 0.001;
        setVisible(true);
        juggler.tween(this, 0.5, {	scaleX : reSize, scaleY : reSize, scaleZ : reSize, onComplete: afterAnimation});
        _state = STATE_REMOVE;
    }

    public function deselectCube() : void
    {
        setVisible(true);
        juggler.tween(this, 0.5, {	rotationX : 0, rotationY : 0, scaleX : 1, scaleY : 1, scaleZ : 1, onComplete: afterAnimation});

        _state = STATE_INACTIVE;
    }

    public function winCube() : void
    {
        setVisible(true);
        juggler.tween(this, 0.5, {scaleX : 1, scaleY : 1, scaleZ : 1, onComplete: afterAnimation});

        _state = STATE_WIN;
    }

    private function afterAnimation():void
    {
        setVisible(false);
        switch (_state)
        {
            case STATE_FRONT:
                _bottomSide.visible = true;
                break;
            case STATE_LEFT:
                _leftSide.visible = true;
                soundController.play(data["audio"]);
                break;
            case STATE_RIGHT:
                _rightSide.visible = true;
                break;
            case STATE_BACK:
                _upperSide.visible = true;
                break;
            case STATE_REMOVE:
                dispatchEvent(new Event(EVENT_REMOVED));
                break;
            case STATE_INACTIVE:
                dispatchEvent(new Event(EVENT_DESELECT));
                _middleSide.visible = true;
                break;
            case STATE_WIN:
                dispatchEvent(new Event(EVENT_DESELECT));
                _bottomSide.visible = true;
                break;

        }
    }

    private function setVisible(inVisible : Boolean) : void
    {
        _rendering = inVisible;
        _objects.forEach(function (inSprite3D : Sprite3D, ...rest)
        {
            inSprite3D.visible = inVisible;
        });
    }

	private function handleEnterFrame(event : Event) : void
	{
        if(!_rendering) return;
		if (!_middleSide.parent || !_bottomSide.parent)
		{
			return
		}

		_objects = _objects.sort(function (x : Sprite3D, y : Sprite3D) : Number
		{
			if (!x.parent || !y.parent)
			{
				return 0;
			}
			var xMatrix : Matrix3D = new Matrix3D();
			var yMatrix : Matrix3D = new Matrix3D();
			x.getChildByName("middle").getTransformationMatrix3D(parent, xMatrix);
			y.getChildByName("middle").getTransformationMatrix3D(parent, yMatrix);


			return xMatrix.position.z > yMatrix.position.z ? -1 : 1;
		});

		while (numChildren)
		{
			removeChildAt(0);
		}

		for (var i : int = 0; i < _objects.length; i++)
		{
			var sprite : Sprite3D = _objects[i];
			addChild(sprite);
		}
	}

	public function initWord () : void
	{
		var textField : starling.text.TextField = assetController.getTextField(125, SIZE, SIZE, 0x41381F, AssetController.FONT_NOVA);
		textField.hAlign = HAlign.CENTER;
		textField.vAlign = VAlign.CENTER;

		textField.isHtmlText = true;
		textField.text = (data.text as String).replace(TAG_R, TAG_FONT_RED).replace(TAG_R_CLOSED, TAG_FONT_CLOSED);

		_bottomSide.addChild(textField);

		var tapAndGoField : starling.text.TextField = assetController.getTextField(50, 225, 90, 0x41381F, AssetController.FONT_NOVA);
		tapAndGoField.hAlign = HAlign.LEFT;
		tapAndGoField.vAlign = VAlign.CENTER;
		tapAndGoField.x = SIZE * .25;
		tapAndGoField.y = SIZE * .70;
		tapAndGoField.text = localeGet('game.cube.tap&say');

		var chinesField : starling.text.TextField = assetController.getTextField(100, SIZE, SIZE, 0x41381F, AssetController.FONT_NOVA);
		chinesField.hAlign = HAlign.CENTER;
		chinesField.vAlign = VAlign.CENTER;
        chinesField.text = data.translation;

		_rightSide.addChild(chinesField);

		_bottomSide.addChild(tapAndGoField);

	}
    public function getTrueBounds(inDis:starling.display.DisplayObject, resultRect:flash.geom.Rectangle=null):flash.geom.Rectangle
    {
        if (resultRect == null) resultRect = new flash.geom.Rectangle();
        if(inDis is Sprite3D)
        {
            for(var aIndex:int=0; aIndex<(inDis as Sprite3D).numChildren; aIndex++)
            {
                resultRect = getTrueBounds((inDis as Sprite3D).getChildAt(aIndex), resultRect);
            }
        }else{
            if(inDis is starling.text.TextField) return resultRect;

            if (resultRect == null) resultRect = new flash.geom.Rectangle();
            resultRect = resultRect.union(inDis.getBounds(this.parent));
        }
        return resultRect;
    }

}
}
