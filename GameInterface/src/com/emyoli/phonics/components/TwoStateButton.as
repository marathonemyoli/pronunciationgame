package com.emyoli.phonics.components {
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.interfaces.IPersistState;
import com.emyoli.phonics.utils.SharedObjectManager;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.views.BaseView;

import starling.display.Sprite;
import starling.text.TextField;

public class TwoStateButton extends Sprite implements IPersistState
{
	[Inject]
	public var assetController : AssetController;

    [Inject]
    public var sharedObjectManager : SharedObjectManager;

	protected var _upState : Sprite;
	protected var _downState : Sprite;

	protected var _toggle : Boolean = false;
	protected var _selected : Boolean = false;
	protected var _background : Sprite;
	protected var _notDeselectable : Boolean = false;
	protected var _label : TextField;

	protected var _stopPropagation : Boolean = true;

    private var _persistStateTarget : BaseView;

	public function TwoStateButton()
	{
		super();
	}

    public function init(upState : Sprite, downsState : Sprite = null, background : Sprite = null) : void
	{
        _upState = upState;
        _downState = downsState;
        _background = background;

		name = _upState.name;

        readPersistedState();

		if (_background)
		{
			addChild(_background);
		}

		if (_downState)
		{
			addChild(_downState);
		}

		addChild(_upState);
		
		addEventListener(GameTouchEvent.TOUCH_BEGIN, handleTouchBegin);
		addEventListener(GameTouchEvent.CLICK, handleTouchEnd);
	}

	private function handleTouchEnd(event : GameTouchEvent) : void
	{
		if (_stopPropagation)
		{
			event.stopPropagation();
		}

		if (!_toggle)
		{
			_upState.alpha = 1;
		}
	}

	private function handleTouchBegin(event : GameTouchEvent) : void
	{
		if (_stopPropagation)
		{
			event.stopPropagation();
		}

		if (!_toggle)
		{
			_upState.alpha = 0;
		}
		else if(!(_selected && _notDeselectable))
		{
			selected = !_selected;
			updateToggleState();
		}
	}

	protected function updateToggleState() : void
	{
		_upState.alpha = Number(!_selected);
        writeState();
	}

    private function readPersistedState() : void
    {
		if (_persistStateTarget && sharedObjectManager.hasData(_persistStateTarget.name +"." + name))
        {
			var value : Object = sharedObjectManager.getData(_persistStateTarget.name + "." + name);
            _selected = value as Boolean;
        }
    }

    private function writeState() : void
    {
        if (name && _persistStateTarget)
        {
            sharedObjectManager.setData(_persistStateTarget.name +"." + name, selected);
        }
    }

	public function get toggle() : Boolean
	{
		return _toggle;
	}

	public function set toggle(value : Boolean) : void
	{
		_toggle = value;
	}

	public function get selected() : Boolean
	{
		return _selected;
	}

	public function set selected(value : Boolean) : void
	{
		if (_toggle)
		{
			_selected = value;
			updateToggleState();
		}
	}

	public function set notDeselectable(notDeselectable : Boolean) : void
	{
		_notDeselectable = notDeselectable;
	}

	public function set text(text : String) : void
	{
		if (!_label)
		{
			_label = createLabel();
		}
		_label.text = text;
	}

	protected function createLabel(explicitWidth : Number = 0) : TextField
	{
		var label : TextField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1.25, explicitWidth ? explicitWidth : width,
				height, 0xFFFFFF, AssetController.FONT_HOLTWOOD, true);
		label.nativeFilters = [AssetController.BUTTON_LABEL_SHADOW_FILTER, AssetController.BUTTON_LABEL_BEVEL_FILTER,
			AssetController.BUTTON_LABEL_GLOW_FILTER];
		label.y = (height - label.height) / 2;

		addChild(label);

		return label;
	}

    public function get persistStateTarget() : BaseView
    {
        return _persistStateTarget;
    }

    public function set persistStateTarget(value : BaseView) : void
    {
        _persistStateTarget = value;
        if (_persistStateTarget)
        {
            readPersistedState();
            writeState();
        }
    }
}
}
