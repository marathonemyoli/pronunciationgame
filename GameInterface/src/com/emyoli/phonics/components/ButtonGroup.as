package com.emyoli.phonics.components
{
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.interfaces.IPersistState;
import com.emyoli.phonics.utils.SharedObjectManager;
import com.emyoli.phonics.views.BaseView;

import flashx.textLayout.events.SelectionEvent;

import starling.events.Event;

import starling.events.EventDispatcher;

import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class ButtonGroup extends EventDispatcher implements IPersistState
{
    [Inject]
    public var sharedObjectManager : SharedObjectManager;

	private var _buttons : Vector.<TwoStateButton>;
    
    private var _persistStateTarget : BaseView;
    private var _selectedIndex : int = -1;

    private var _name : String;

	public function ButtonGroup()
	{

	}

	public function init (defaultId : int = 0) : void
	{
        initName();

        readPersistedState();

        if (_selectedIndex == -1)
        {
            _selectedIndex = defaultId;
        }

		for each (var button : TwoStateButton in _buttons)
		{
			button.addEventListener(TouchEvent.TOUCH, handleTouch);
			button.toggle = true;
			button.notDeselectable = true;
		}
		_buttons[_selectedIndex].selected = true;
	}

    private function initName() : void
    {
        _name = "";
        _buttons.forEach(function (value : TwoStateButton, ...rest) : void
        {
            _name += value.name;
        })
    }

	private function handleTouch(event : TouchEvent) : void
	{
		if (event.touches[0].phase == TouchPhase.BEGAN)
		{
			const selectedButton : TwoStateButton = event.currentTarget as TwoStateButton;
			for each (var button : TwoStateButton in _buttons)
			{
				if (button != selectedButton && button.selected)
				{
					button.selected = false;
				}
			}
			selectedButton.selected = true;
            writeState();
			dispatchEvent(new GameEvent(GameEvent.ITEM_SELECTED, event.bubbles, selected));
		}
	}

	public function get selected () : TwoStateButton
	{
		var selectedArray : Vector.<TwoStateButton> = _buttons.filter(function (button : TwoStateButton, ...rest) : Boolean{
			return button.selected;
		});
		return selectedArray.length ? selectedArray[0] : null;
	}

	public function get selectedIndex () : int
	{
		return _buttons.indexOf(selected as TwoStateButton);
	}

	public static function createGroupFrom (buttons : Vector.<TwoStateButton>) : ButtonGroup
	{
		const buttonGroup : ButtonGroup = new ButtonGroup();
		buttonGroup._buttons = buttons;
		return buttonGroup;
	}

    private function readPersistedState() : void
    {
        if (_persistStateTarget && sharedObjectManager.hasData(_persistStateTarget.name +"." + _name))
        {
             var value : Object = sharedObjectManager.getData(_persistStateTarget.name +"." + _name);
            _selectedIndex = value as int;
        }
    }

    private function writeState() : void
    {
        if (_name && _persistStateTarget)
        {
            sharedObjectManager.setData(_persistStateTarget.name +"." + _name, selectedIndex);
        }
    }

    public function get persistStateTarget() : BaseView
    {
        return _persistStateTarget;
    }

    public function set persistStateTarget(value : BaseView) : void
    {
        _persistStateTarget = value;
        if (_persistStateTarget)
        {
            readPersistedState();
            writeState();
        }
    }
}
}
