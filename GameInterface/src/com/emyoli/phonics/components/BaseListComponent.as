package com.emyoli.phonics.components
{
import com.emyoli.phonics.factories.BeanInstanceFactory;
import com.emyoli.phonics.models.IListRenderer;

import starling.core.Starling;
import starling.display.DisplayObject;

import starling.display.Sprite;
import starling.events.Event;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

/**
 * @author Oleg Knaus
 */
public class BaseListComponent extends Sprite
{
	[Inject]
	public var beanFactory : BeanInstanceFactory;

	private var renderers : Vector.<IListRenderer>;
	private var listModels : Vector.<Object>;
	private var _container : Sprite;

	private var _width : Number;
	private var _height : Number;

	private var _currentTouchId : int = -1;
	private var _touchStartY : Number;
	private var _containerY : Number = 0;
	private var _mask : Sprite;
	private var _renderersCount : int = 8;
	private var _rendererClass : Class;
    private var _listData : Array;



	public function BaseListComponent () : void
	{
		super();
	}

	public function initData(width : Number, height : Number, listData : Array, rendererClass : Class) : void
    {
        _width = width;
        _height = height;
        _listData = listData;
        _rendererClass = rendererClass;
    }

    [PostConstruct]
    public function init() : void
    {

		this.listModels = new <Object>[];
		for each (var el : Object in _listData)
		{
			this.listModels.push(el);
		}

		initMask();
		initContainers();
		refreshPositions();

		addEventListener(Event.ADDED_TO_STAGE, handleAdded);
		addEventListener(Event.REMOVED_FROM_STAGE, handleRemoved);
	}

	private function handleRemoved(event : Event) : void
	{
		removeEventListener(Event.REMOVED_FROM_STAGE, handleAdded);
		removeScroll();
	}

	private function handleAdded(event : Event) : void
	{
		removeEventListener(Event.ADDED_TO_STAGE, handleAdded);
		initScroll();
	}

	/**
	 * Set renderers on its positions depending on id
	 */
	private function refreshPositions(delta : Number = 0) : void
	{
		for (var i : int = 0; i < _renderersCount; i++)
		{
			var renderer : IListRenderer = renderers[i];

			if (renderer.y + _rendererClass.MAX_HEIGHT < 0 && delta > 0 &&
					renderer.id + renderers.length < listModels.length)
			{
				renderer.id += renderers.length;
			} else if (renderer.y >= _height && delta < 0 &&
					renderer.id >= renderers.length)
			{
				renderer.id -= renderers.length;
			}

			renderer.model = listModels[renderer.id];
			renderer.y = _containerY + renderer.id * _rendererClass.MAX_HEIGHT;
		}
	}

	/**
	 * initialize mask Sprite to limit number of visible renderers
	 */
	private function initMask() : void
	{

	}

	/**
	 * Initialize touch scroller
	 */
	private function initScroll() : void
	{
		/*
		Starling.current.stage.addEventListener(MouseEvent.MOUSE_DOWN, handleTouchBegin);
		Starling.current.stage.addEventListener(MouseEvent.MOUSE_UP, handleTouchEnd);
		 */
		Starling.current.stage.addEventListener(TouchEvent.TOUCH, handleTouchBegin);
	}

	private function removeScroll () : void
	{
		Starling.current.stage.removeEventListener(TouchEvent.TOUCH, handleTouchBegin);
	}

	/**
	 * Handle Touch events
	 * @param    e
	 */
	private function handleTouchBegin(e : TouchEvent) : void
	{
		for (var i : int = 0; i < e.touches.length; i++)
		{
			//if touch is current touch
			if (e.touches[i].phase == TouchPhase.BEGAN)
			{
				_currentTouchId = e.touches[i].id;// e.touchPointID;
				_touchStartY = e.touches[i].globalY - _containerY;
			}
			else if (e.touches[i].id == _currentTouchId)
			{
				if (e.touches[i].phase == TouchPhase.MOVED || e.touches[i].phase == TouchPhase.ENDED)
				{
					if (_currentTouchId != -1)
					{
						var newContainerY : Number = e.touches[i].globalY - _touchStartY;
						if (newContainerY > 0)
						{
							newContainerY = 0;
						} else if (newContainerY < _height - listModels.length * _rendererClass.MAX_HEIGHT)
						{
							newContainerY = _height - listModels.length * _rendererClass.MAX_HEIGHT;
						}
						var delta : Number = _containerY - newContainerY;
						_containerY = newContainerY;
						refreshPositions(delta);
					}
				}

				if (e.touches[i].phase == TouchPhase.ENDED)
				{
					_currentTouchId = -1;
				}
			}
		}
	}

	/**
	 * Initialize renderers
	 */
	private function initContainers() : void
	{
		_container = new Sprite();
		addChild(_container);
		//_container.mask = _mask;

		renderers = new Vector.<IListRenderer> ();

		if (listModels.length < _renderersCount)
		{
			_renderersCount = listModels.length;
		}

		for (var i : int; i < _renderersCount; i++)
		{
			var renderer : IListRenderer = beanFactory.getBeanInstance(_rendererClass) as IListRenderer;
			renderer.id = i;
			renderer.draw();
			renderer.init();
			renderers.push(renderer);
			_container.addChild(renderer as DisplayObject);
		}
	}

}
}