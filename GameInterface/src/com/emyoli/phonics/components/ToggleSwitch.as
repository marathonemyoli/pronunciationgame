package com.emyoli.phonics.components {
import com.emyoli.phonics.controllers.AssetController;

import starling.display.Sprite;
import starling.text.TextField;

public class ToggleSwitch extends TwoStateButton {
	private var _textOn : String;
	private var _textOff : String;

	private var _blackLabelOn : TextField;
	private var _blackLabelOff : TextField;

	private var _labelOn : TextField;
	private var _labelOff : TextField;



	public function ToggleSwitch()
	{
		super();
	}

	public override function init (upState : Sprite, downsState : Sprite = null, background : Sprite = null) : void
	{
		super.init(upState, downsState, background);
		_toggle = true;
	}

	public function setTextOnOff (on : String, off : String) : void
	{
		_blackLabelOn = createLabelBlack(width/2);
		_blackLabelOff = createLabelBlack(width/2);

		_labelOn = createLabel(width/2);
		_labelOff = createLabel(width/2);

		_blackLabelOff.x = width/2;
		_labelOff.x = width/2;

		_blackLabelOn.text = _labelOn.text = on;
		_blackLabelOff.text = _labelOff.text = off;

		_labelOn.fontSize /= 1.4;
		_labelOff.fontSize /= 1.4;

		updateToggleState();
	}

	protected override function updateToggleState () : void
	{
		super.updateToggleState()

		_blackLabelOff.alpha = _labelOn.alpha = _upState.alpha = Number(_selected);
		_blackLabelOn.alpha = _labelOff.alpha = _downState.alpha = Number(!_selected);

	}

	private function createLabelBlack(explicitWidth : Number = 0) : TextField
	{

		var labelBlack : TextField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1.25, explicitWidth ? explicitWidth : width,
				height, 0, AssetController.FONT_NOVA, true);

		labelBlack.y = (height - labelBlack.height) / 2;

		addChild(labelBlack);
		return labelBlack;
	}
}
}
