package com.emyoli.phonics.components
{
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.interfaces.IPersistState;
import com.emyoli.phonics.utils.SharedObjectManager;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.views.BaseView;

import starling.display.DisplayObject;

import starling.display.DisplayObjectContainer;

import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

public class DropDownList extends Sprite implements IPersistState {
	public static const GAP : Number = -10;

	[Inject]
	public var assetController : AssetController;

	[Inject]
	public var sharedObjectManager : SharedObjectManager;

	private var _dataProvider : Array;
	private var _renderers : Vector.<Sprite> = new <Sprite>[];

	private var _upState : Sprite;
	private var _title : TextField;
	private var _upperSprite : Sprite;
	private var _middleSprite : Sprite;
	private var _bottomSprite : Sprite;
	private var _rendererContainer : Sprite;
	private var _separatorSprite : Sprite;

	private var _selectedIndex : int = 0;
	private var _separators : Sprite;
	private var _theStage : DisplayObjectContainer;

	private var _persistStateTarget : BaseView;

	public function DropDownList()
	{
		super();
	}

	public function init (upState : Sprite, title : TextField, upperSprite : Sprite, middleSprite : Sprite, bottomSprite : Sprite, separatorSprite : Sprite) : void
	{
		_upState = upState;
		_title = title;
		_upperSprite = upperSprite;
		_middleSprite = middleSprite;
		_bottomSprite = bottomSprite;
		_separatorSprite = separatorSprite;

        name = upState.name;
	}

	public function initData(dataProvider : Array) : void
	{
		_dataProvider = dataProvider;

        readPersistedState();

		_title.x -= _upState.x;
		_title.y -= _upState.y;

		_upState.x = 0;
		_upState.y = 0;

		_separators = new Sprite();
		_separators.x = 2;
		_separators.y = -2;

		_rendererContainer = new Sprite();
		_rendererContainer.y = _upState.height;
		_rendererContainer.x = (_upState.width - _upperSprite.width) / 2;
		_rendererContainer.visible = false;

		addChild(_upState);
		addChild(_rendererContainer);


		initItemRenderers();
		refreshLabel();

		_rendererContainer.addChild(_separators);

		addEventListener(Event.ADDED_TO_STAGE, handleAdded);
		addEventListener(Event.REMOVED_FROM_STAGE, handleRemoved);

		_upState.addEventListener(GameTouchEvent.CLICK, handleTitleClick)
	}

	private function handleTitleClick(event : GameTouchEvent) : void
	{
		toggleContentVisibilty();
	}

	private function handleRemoved(event : Event) : void
	{
		_theStage.removeEventListener(GameTouchEvent.CLICK, handleOuterClick);
		_theStage = null;
	}

	private function handleAdded(event : Event) : void
	{
		_theStage = stage;
		_theStage.addEventListener(GameTouchEvent.CLICK, handleOuterClick);
	}

	private function initItemRenderers() : void
	{
		_renderers.push(_upperSprite);

		var i : int;

		if (_dataProvider.length > 2)
		{
			for (i = 1; i < _dataProvider.length - 1; i++)
			{
				var clone : Sprite = Utils.getSpriteClone(_middleSprite);
				_renderers.push(clone);
			}
		}

		_renderers.push(_bottomSprite);


		for (i = 0 ; i < _dataProvider.length; i++)
		{
			var textField : TextField = getRendererTextField();
			textField.x = _renderers[i].pivotX + 20;
			textField.y = _renderers[i].pivotY + (_renderers[i].height - textField.height) / 2;
			textField.text = _dataProvider[i].text;
			_renderers[i].addChild(textField);
			_renderers[i].y = i ? (i * (_renderers[i-1].height + GAP)) : 0;
			_renderers[i].addEventListener(GameTouchEvent.CLICK, handleItemSelect);
			_rendererContainer.addChild(_renderers[i]);

			if (i)
			{
				const separatorClone : Sprite = Utils.getSpriteClone(_separatorSprite);
				separatorClone.x = _renderers[i].x;
				separatorClone.y = _renderers[i].y;
				_separators.addChild(separatorClone);
			}
		}
	}

	private function getRendererTextField() : TextField
	{
		var textField : TextField = assetController.getTextField(AssetController.DEFAULT_FONT_SIZE * 1, _title.width,
				_title.height, 0xFFFFFF, AssetController.FONT_NOVA);
		textField.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];
		return textField;
	}

	private function handleItemSelect(event : GameTouchEvent) : void
	{
		_selectedIndex = _renderers.indexOf(event.currentTarget as Sprite);
		refreshLabel();
        writeState();
		dispatchEvent(new GameEvent(GameEvent.ITEM_SELECTED, event.bubbles, selectedItem));
	}

	private function handleOuterClick(event : GameTouchEvent) : void
	{
		if (!_upState.contains(event.target as DisplayObject))
		{
			hideContent();
		}
	}

	private function hideContent() : void
	{
		_rendererContainer.visible = false;
	}

	private function toggleContentVisibilty() : void
	{
		_rendererContainer.visible = !_rendererContainer.visible;
	}

	private function refreshLabel() : void
	{
		_title.text = _dataProvider[_selectedIndex].text;
	}

    private function readPersistedState() : void
    {
		if (_persistStateTarget && sharedObjectManager.hasData(_persistStateTarget.name +"." + name))
        {
        	var value : Object = sharedObjectManager.getData(_persistStateTarget.name +"." + name);
            _selectedIndex = value as int;
        }
    }

    private function writeState() : void
    {
        if (name && _persistStateTarget)
        {
            sharedObjectManager.setData(_persistStateTarget.name +"." + name, _selectedIndex);
        }
    }

	public function get selectedItem () : Object
	{
		return _dataProvider[_selectedIndex];
	}

    public function get persistStateTarget() : BaseView
    {
        return _persistStateTarget;
    }

    public function set persistStateTarget(value : BaseView) : void
    {
        _persistStateTarget = value;
        if (_persistStateTarget)
        {
            readPersistedState();
            writeState();
        }
    }
}
}
