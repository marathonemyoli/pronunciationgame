/**
 * Created by alekseykabanov on 03.09.15.
 */
package com.emyoli.phonics.components.avatar {
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.net.UserInfo;

import starling.display.Canvas;

import starling.display.DisplayObject;

import starling.display.Sprite;
import starling.events.KeyboardEvent;
import starling.text.TextField;
import starling.utils.HAlign;
import starling.utils.VAlign;

public class GameAvatar extends Sprite {

    public static const SIZE : int = 112;
    public static const DOTS_NAME : Array = ["avatar_dot1_blue", "avatar_dot2_red", "avatar_dot3_orange", "avatar_dot4_yellow", "avatar_dot5_purpule", "avatar_dot6_green"];

    [Inject]
    public var assetController : AssetController;

    private var _avatar_back_normal : Sprite = null;
    private var _avatar_back_selected : Sprite = null;

    private var _nameTF : TextField = null;
    private var _cardsTF : TextField = null;
    private var _nameSprite : Sprite = null;

    private var _isSelected : Boolean = false;
    private var _user : UserInfo = null;

    private var _dotIndex : int = -1;
    private var _dotSprite : Sprite = null;


    public function GameAvatar(inUser : UserInfo)
    {
        _user = inUser;
    }

    [PostConstruct]
    public function init() : void
    {
        _avatar_back_normal = assetController.getSpriteCloneFrom("avatars", "avatar1_back_normal", true);
        addChild(_avatar_back_normal);
        _avatar_back_normal.y = 195;

        _avatar_back_selected = assetController.getSpriteCloneFrom("avatars", "avatar1_back_selected", true);
        addChild(_avatar_back_selected);
        _avatar_back_selected.y = 195;

        _nameTF = assetController.getTextField(35, 250, 40, 0xFFFFFF, AssetController.FONT_NOVA);
        _nameTF.hAlign = HAlign.LEFT;
        _nameTF.vAlign = VAlign.CENTER;
        _nameTF.x = 18;
        _nameTF.y = -7;
        _nameTF.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];

        _nameSprite = new Sprite();
        _nameSprite.addChild(_nameTF);
        addChild(_nameSprite);
        _nameSprite.y = 154;

        _cardsTF = assetController.getTextField(25, 100, 25, 0xFFFFFF, AssetController.FONT_NOVA);
        addChild(_cardsTF);
        _cardsTF.hAlign = HAlign.CENTER;
        _cardsTF.vAlign = VAlign.CENTER;
        _cardsTF.x = 6;
        _cardsTF.y = 313;
        _nameTF.nativeFilters = [AssetController.REGULAR_LABEL_SHADOW_FILTER];

        card = "2-Cards";

        select = false;

        nickName = _user.nickname;

        var avatar : Sprite = assetController.getAvatar(_user.avatarname, _user.gender);

        var amask:Canvas = new Canvas();
        amask.drawCircle(0, 0, 41);
        amask.x=57;
        amask.y=250;

        addChild(avatar);
        addChild(amask);
        avatar.x = 0;
        avatar.y = 194;
        avatar.scaleX = 0.5;
        avatar.scaleY = 0.5;
        avatar.mask = amask;
    }

    public function set select(inValue : Boolean) : void
    {
        _isSelected = inValue;
        _avatar_back_normal.visible = !inValue;
        _avatar_back_selected.visible = !_avatar_back_normal.visible;
    }

    public function get select() : Boolean
    {
        return _isSelected;
    }

    public function set nickName(inValue : String) : void
    {
        _nameTF.text = inValue;
        refreshTextAlign();
    }

    public function set card(inValue : String) : void
    {
        _cardsTF.text = inValue;
    }

    public function set dot(inValue : int) :void
    {
        _dotIndex = inValue;

        // remove dot color
        if(_dotSprite != null && _nameSprite.contains(_dotSprite))
        {
            _nameSprite.removeChild(_dotSprite);
            _dotSprite = null;
        }

        //add dot color
        if(_dotIndex >= 0 && _dotSprite < DOTS_NAME.length)
        {
            _dotSprite = assetController.getSpriteCloneFrom("avatars", DOTS_NAME[_dotIndex], true);
            _nameSprite.addChild(_dotSprite);
        }else{
            _dotIndex = -1;
        }

        refreshTextAlign();
    }

    private function refreshTextAlign() : void
    {
        var widthDot : int = 21;
        if(_dotSprite == null)
        {
            widthDot = 0;
        }
        _nameSprite.x = SIZE / 2 - (_nameTF.textBounds.width + widthDot) / 2;
    }
}
}
