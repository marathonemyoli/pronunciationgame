/**
 * Created by alekseykabanov on 03.09.15.
 */
package com.emyoli.phonics.components.avatar {
import com.emyoli.phonics.factories.BeanInstanceFactory;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.net.UserInfo;

import starling.display.Sprite;

public class AvatarCarousel extends Sprite{

    [Inject]
    public var factory : BeanInstanceFactory;

    private var _users : Vector.<UserInfo> = null;
    private var _avatars : Vector.<GameAvatar> = new Vector.<GameAvatar>();
    private var _border : int = 10;

    private var _currentSelect : int = 0;

    public function AvatarCarousel (inUsers:Vector.<UserInfo>)
    {
        _users = inUsers;
    }

    [PostConstruct]
    public function init() : void
    {
        if(_users == null) return;

        var aStep:int = (stage.stageWidth - GameAvatar.SIZE * _users.length - _border * 2) / (_users.length + 1);

        for(var index : int = 0; index < _users.length; index++)
        {
            var aGameAvatar : GameAvatar = new GameAvatar(_users[index]);
            _avatars.push(aGameAvatar)
            addChild(aGameAvatar);
            aGameAvatar.dot = index;
            aGameAvatar.x = _border + index * (GameAvatar.SIZE + aStep) + aStep;
        }
        select(0);
    }

    public function select(inValue : int) : void
    {
        _avatars[_currentSelect].select = false;
        _currentSelect = inValue;
        if(_currentSelect >= _avatars.length || _currentSelect <= 0)
        {
            _currentSelect = 0;
        }
        _avatars[_currentSelect].select = true;
    }

    public function selectNext() : void
    {
        select(_currentSelect++);
    }
}
}
