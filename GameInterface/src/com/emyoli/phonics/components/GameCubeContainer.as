package com.emyoli.phonics.components {

import com.emyoli.phonics.controllers.ConfigController;
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.models.recognition.Grapheme;
import com.emyoli.phonics.models.recognition.Phoneme;
import com.emyoli.phonics.models.recognition.Sentence;
import com.emyoli.phonics.models.recognition.Word;

import flash.geom.Rectangle;

import org.gestouch.events.GestureEvent;
import org.gestouch.gestures.TapGesture;

import starling.core.Starling;

import starling.display.Quad;
import starling.display.Sprite;
import starling.display.Sprite3D;
import starling.events.Event;
import starling.events.EventDispatcher;

public class GameCubeContainer extends Sprite3D{
    public static const TOUCH : String = "TOUCH";
    public static const SAY : String = "SAY";
    public static const CLOSED : String = "CLOSED";
    public static const OPENED : String = "OPENED";
    public static const CLOSE_CUBE : String = "CLOSE_CUBE";
    public static const ROLL_CUBE : String = "ROLL_CUBE";

    [Inject]
    public var config : ConfigController;

    public var isMayChoose : Boolean = false;
    public var isMayClick : Boolean = true;

    private var _clickMask : Sprite = null;
    private var _cube : GameCube = null;
    private var _tapGesture : TapGesture = null;

    public var offsetCamX : int = 0;
    public var offsetCamY : int = 0;

    public var indexCube : int = -1;
    public var data : Object;

    private var _enableTouch : Boolean = false;

    private var _isWin : Boolean = false;

    public function GameCubeContainer(inOffsetCamX : int, inOffsetCamY : int, inIndexCube : int, inData : Object)
    {
        offsetCamX = inOffsetCamX;
        offsetCamY = inOffsetCamY;
        indexCube = inIndexCube;
        data = inData;
    }
    [PostConstruct]
    public function init() :void{
        _clickMask = createClickMask();

        _cube = new GameCube(data);
        _cube.z = GameCube.SIZE / 2;

        addChild(_cube);
        addChild(_clickMask);

        _cube.addEventListener(GameCube.EVENT_SELECTED, function (e : Event) : void
        {
            reSizeClickMask();
            dispatchEvent(new Event(OPENED));
        });
        _cube.addEventListener(GameCube.EVENT_DESELECT, function (e : Event) : void
        {
            scaleZ = 0.0001;
            reSizeClickMask();
            dispatchEvent(new Event(CLOSED));
        });
        _cube.addEventListener(GameCube.EVENT_REMOVED, function (e : Event) : void
        {
            scaleZ = 0.0001;
            reSizeClickMask();
            dispatchEvent(new Event(CLOSED));
            removeChild(_cube);
        });
        reSizeClickMask(300);
        _clickMask.addEventListener(GameTouchEvent.TOUCH_BEGIN, handleClick);
        _tapGesture = new TapGesture(_clickMask);
        _tapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, handleTapGesture);
    }

    public function selectCube() : void
    {
        scaleZ = 1;
        _cube.selectCube();
    }

    public function deselect() : void
    {
        _isWin = false;
        _cube.deselectCube();
    }

    public function win() : void
    {
        _isWin = true;
        _cube.winCube();
    }

    public function baybay() : void
    {
        _cube.bay();
    }

    public function rotateLeft() : void
    {
        _cube.rotateLeft();
    }

    public function rotateRight() : void
    {
        _cube.rotateRight();
    }

    public function set enableTouch(inValue : Boolean) : void
    {
        _enableTouch = inValue;
    }

    private function handleTapGesture(event : GestureEvent) : void
    {
        if(isMayChoose && _enableTouch)
        {
            if(!_cube.handleTapGesture(null))
            {
                dispatchEvent(new Event(SAY, false, this));
            }
        }
    }

    private function handleClick(event : Event) : void
    {
        if(isMayClick && _enableTouch && !_isWin)
        {
            dispatchEvent(new Event(TOUCH, false, this));
        }
    }

    private function createClickMask() : Sprite
    {
        var spr : Sprite = new Sprite();
        spr.addChild(new Quad(1, 1, int(Math.random() * 16000000)));
        spr.alpha = 0;
        return spr;
    }

    public function reSizeClickMask(inSize : int = 0) : void
    {
        var aBound : flash.geom.Rectangle;
        if(inSize == 0)
        {
            aBound = _cube.getTrueBounds(_cube);
        }else{
            aBound = new flash.geom.Rectangle(-inSize / 2, -inSize / 2, inSize, inSize);
        }

        _clickMask.x = aBound.x;
        _clickMask.y = aBound.y;
        _clickMask.width = aBound.width;
        _clickMask.height = aBound.height;
    }
}
}
