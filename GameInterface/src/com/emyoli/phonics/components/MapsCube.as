/**
 * Created by alekseykabanov on 04.09.15.
 */
package com.emyoli.phonics.components {
import com.creativebottle.starlingmvc.beans.BeanFactory;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.events.GameTouchEvent;
import com.emyoli.phonics.factories.BeanInstanceFactory;

import flash.events.TimerEvent;

import flash.geom.Rectangle;
import flash.geom.Vector3D;
import flash.utils.Timer;

import org.gestouch.events.GestureEvent;
import org.gestouch.gestures.SwipeGesture;
import org.gestouch.gestures.SwipeGestureDirection;
import org.gestouch.gestures.TapGesture;

import starling.animation.Juggler;

import starling.core.Starling;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.display.Quad;
import starling.display.Sprite;

import starling.display.Sprite;
import starling.display.Sprite3D;
import starling.events.Event;
import starling.events.KeyboardEvent;

public class MapsCube extends Sprite {
    public static const MAP16 : int = 0;
    public static const MAP24 : int = 1;
    public static const MAP36 : int = 2;

    public static const STEP : int = 10;

    [Inject]
    public var assetController : AssetController;

    [Juggler]
    public var juggler : Juggler;

    [Inject]
    public var factory : BeanInstanceFactory;


    private var _type : int = 0;
    private var _mapWidth : int = 4;
    private var _mapHeight : int = 4;

    private var _selected : GameCubeContainer = null;

    private var _swipeGesture : SwipeGesture;

    private var _zDepth : Number = 0.001;

    private var _scale : Number = 1;

    private var _mapCube : Array = new Array();

    private var _currentWords : Array;
    private var _libWords : Array;

    private var _enableTouch : Boolean = false;

    public function MapsCube(typeMap : int, inCurrentWords : Array,  inLibWords : Array)
    {
        _type = typeMap;
        _currentWords = inCurrentWords;
        _libWords = inLibWords;
    }

    [PostConstruct]
    public function init() : void
    {
        // create map cube
        var offsetY : int = 0;
        switch (_type)
        {
            case MAP16:
                _mapHeight = _mapWidth = 4;
                offsetY = 530;
                _scale = 0.5;
                break;
            case MAP24:
                _mapWidth = 4;
                _mapHeight = 6;
                offsetY = 460;
                _scale = 0.4;
                break;
            case MAP36:
                _mapHeight = _mapWidth = 6;
                offsetY = 510;
                _scale = 0.33;
                break;
        }

        var contWidth : Number = ((GameCube.SIZE + STEP) * (_mapWidth - 1) - STEP) * _scale;
        var offsetX : int = (stage.stageWidth - contWidth) / 2 - parent.x;

        var aSpr : Sprite = new Sprite();
        addChild(aSpr);

        for (var aY : int = 0; aY < _mapHeight; aY++)
        {
            for (var aX : int = 0; aX < _mapWidth; aX++)
            {
                var aIndexCube : int = aX + aY * _mapWidth;
                var aGameCubeContainer : GameCubeContainer = new GameCubeContainer(
                          ((GameCube.SIZE + STEP) * aX * _scale + offsetX + parent.x) - stage.stageWidth/2,
                          ((GameCube.SIZE + STEP) * aY * _scale + offsetY + parent.y) - stage.stageHeight/2,
                          aIndexCube,
                          _libWords[_currentWords[aIndexCube]]
                );
                aSpr.addChild(aGameCubeContainer);
                _mapCube.push(aGameCubeContainer);
                aGameCubeContainer.x = contWidth / 2;
                aGameCubeContainer.y = ((GameCube.SIZE + STEP) * (_mapHeight - 1) - STEP) * _scale * 0.5;
                aGameCubeContainer.z = _zDepth;
                aGameCubeContainer.scaleX =aGameCubeContainer.scaleY = _scale;
                aGameCubeContainer.scaleZ = 0.0001;

                aGameCubeContainer.addEventListener(GameCubeContainer.TOUCH, handleTouch);
                aGameCubeContainer.addEventListener(GameCubeContainer.CLOSED, handleClosed);
                aGameCubeContainer.addEventListener(GameCubeContainer.OPENED, handleOpened);
                aGameCubeContainer.addEventListener(GameCubeContainer.SAY, handleSay);
            }
        }
        aSpr.x = offsetX;
        aSpr.y = offsetY;

        _swipeGesture = new SwipeGesture(this);
    }

    private function handleSay(event : Event) : void
    {
        dispatchEvent(new Event(GameCubeContainer.SAY, false, event.data));
    }

    private function handleOpened(event : Event) : void
    {
        _swipeGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, handleSwipeGesture);
        _selected.isMayChoose = true;
        dispatchEvent(new Event(GameCubeContainer.OPENED, false, event.currentTarget));
    }

    private function handleSwipeGesture(event : GestureEvent) : void
    {
        if (_selected != null && _enableTouch)
        {
            const swipeGesture : SwipeGesture = event.currentTarget as SwipeGesture;

            var res = swipeGesture.direction & SwipeGestureDirection.DOWN;

            if (swipeGesture.offsetY > 0 && Math.abs(swipeGesture.offsetY) > Math.abs(swipeGesture.offsetX))
            {
                _selected.isMayChoose = false;
                _swipeGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, handleSwipeGesture);
                _selected.deselect();
                dispatchEvent(new Event(GameCubeContainer.CLOSE_CUBE))
            }
            else if (swipeGesture.offsetX < 0)
            {
                dispatchEvent(new Event(GameCubeContainer.ROLL_CUBE))
                _selected.rotateLeft();
            }
            else if (swipeGesture.offsetX > 0)
            {
                dispatchEvent(new Event(GameCubeContainer.ROLL_CUBE))
                _selected.rotateRight();
            }
        }
    }


    private function handleClosed(event : Event) : void
    {
        (event.currentTarget as GameCubeContainer).parent.setChildIndex((event.currentTarget as GameCubeContainer), 0);
        dispatchEvent(new Event(GameCubeContainer.CLOSED, false, event.currentTarget));
        _selected = null;
    }

    private function handleTouch(event : Event) : void
    {
        if (_selected == null)
        {
            dispatchEvent(new Event(GameCubeContainer.TOUCH, false, event.data));
            _selected = (event.data as GameCubeContainer);

            Starling.current.stage.projectionOffset.x = _selected.offsetCamX;
            Starling.current.stage.projectionOffset.y = _selected.offsetCamY;

            _selected.parent.setChildIndex(_selected, _selected.parent.numChildren);
            _selected.selectCube();
        }
    }

    public function startIntro(inDelay : Number) : void
    {
        for (var aX : int = 0; aX < _mapWidth; aX++)
        {
            for (var aY : int = 0; aY < _mapHeight; aY++)
            {
                juggler.tween(_mapCube[aX + aY * _mapWidth], inDelay, {
                    x : (GameCube.SIZE + STEP) * aX * _scale,
                    y : (GameCube.SIZE + STEP) * aY * _scale});
            }
        }
    }

    public function set enableTouch(inValue : Boolean) : void
    {
        _enableTouch = inValue;
        _mapCube.forEach(function(aGameCubeContainer : GameCubeContainer, ...rest){
            aGameCubeContainer.enableTouch = inValue;
        });
    }

    public function winCube(inIndexCube : int) : void
    {
        (_mapCube[inIndexCube] as GameCubeContainer).win();
    }

    public function removeCube(inIndexCube : int) : void
    {
        (_mapCube[inIndexCube] as GameCubeContainer).baybay();
    }

    public function loseCube(inIndexCube : int) : void
    {
        (_mapCube[inIndexCube] as GameCubeContainer).deselect();
    }

}
}
