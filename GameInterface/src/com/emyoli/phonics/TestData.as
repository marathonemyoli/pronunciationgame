package com.emyoli.phonics {
import com.emyoli.phonics.net.AvailableFriendsList;
import com.emyoli.phonics.net.AvailableUser;
import com.emyoli.phonics.net.AvailableUser;
import com.emyoli.phonics.net.GetAvailableFriendsList;
import com.emyoli.phonics.net.MyScoreBoard;
import com.emyoli.phonics.net.MyScoreBoardInfo;
import com.emyoli.phonics.net.Status;
import com.emyoli.phonics.net.UserInfo;
import com.netease.protobuf.Int64;

public class TestData {

	/**
	 * FRIENDS TEST
	 */
	private static const TEST_FRIENDS : Array = getTestArray(getAvailableUser, 20);
	private static const TEST_MYSCORES : Array = getTestArray(getMyScore, 20);

	private static function getTestArray(func : Function, n : int) : Array
	{
		var array : Array = [];
		for (var i : int = 0; i < n; i++)
		{
			array.push(func());
		}
		return array;
	}

	public static const GET_FRIEND_LIST : AvailableFriendsList = new AvailableFriendsList();
	{
		GET_FRIEND_LIST.data = TEST_FRIENDS;
	}

	public static const GET_MYSCORE_LIST : MyScoreBoard = new MyScoreBoard();
	{
		GET_MYSCORE_LIST.info = TEST_MYSCORES;
	}

	private static function getAvailableUser () : AvailableUser
	{
		var user : AvailableUser = new AvailableUser();

		user.data = new UserInfo();
		user.data.avatarname = "Avatar" + int(Math.random() * 6 + 1);
		user.data.nickname = "Avatar" + int(Math.random() * 6 + 1);
		user.status = int(Math.random() * 3) + 1;

		return user;
	}
	/////////////////////////////////////////////


	private static function getMyScore () : MyScoreBoardInfo
	{
		var user : MyScoreBoardInfo = new MyScoreBoardInfo();

		user.score = Int64.fromNumber(int(Math.random() * 666 + 1));

		user.gametype = int(Math.random() * 6 + 1);

		return user;
	}

}
}
