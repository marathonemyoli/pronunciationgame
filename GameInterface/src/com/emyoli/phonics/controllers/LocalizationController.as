package com.emyoli.phonics.controllers {
import ch.ala.locale.LocaleManager;

public class LocalizationController {

	public static const TEXT_BUNDLE : String = "localization.properties";
	public static const ERORR_BUNDLE : String = "errors.properties";


	private static var _instance : LocaleManager;

	private var _localeManager : LocaleManager;

	public function LocalizationController()
	{

	}

	[PostConstruct]
	public function init () : void
	{
		_localeManager = new LocaleManager();
		_localeManager.localeChain = ["en_US"];
		_localeManager.addRequiredBundles([
			{locale: "en_US", bundleName: TEXT_BUNDLE, useLinebreak: true},
			{locale: "en_US", bundleName: ERORR_BUNDLE, useLinebreak: true}
		], onComplete);

		_instance = _localeManager;
	}

	private function onComplete(success:Boolean):void
	{
		if ( success )
		{
			trace("Required bundles successfully added.");
		}
		else
		{
			trace("Adding required bundles failed.");
		}
	}

	public function get localeManager() : LocaleManager
	{
		return _localeManager;
	}

	public static function get localeManager() : LocaleManager
	{
		return _instance;
	}
}

}
