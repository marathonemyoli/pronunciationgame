/**
 * Created by alekseykabanov on 02.09.15.
 */
package com.emyoli.phonics.controllers
{
import flash.media.Sound;
import flash.media.SoundTransform;
import flash.utils.Dictionary;

public class SoundController
{

    [Inject]
    public var assetsController:AssetController;

    private var _sounds : Dictionary = new Dictionary();
    private var _mute : Boolean = false;

    public function SoundController()
    {
    }

    public function play (soundName : String, volume : Number = 1) : void
    {
        if (_mute)
        {
            return;
        }

        var sound : Sound = _sounds[soundName];
        if (!sound)
        {
            sound = _sounds[soundName] = assetsController.assetsManager.getSound(soundName);
        }
        if(sound != null)
        {
            sound.play(0, 0, new SoundTransform(volume));
        }else{
            trace("Error! Sound not found!", soundName);
        }
    }

    public function mute () : void
    {
        _mute = true;
    }

    public function unMute () : void
    {
        _mute = false;
    }
}}
