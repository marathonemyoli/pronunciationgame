package com.emyoli.phonics.controllers {
import ch.ala.locale.LocaleManager;

import mx.utils.StringUtil;

public class ConfigController {

	public static const CONFIG_BUNDLE : String = "config.properties";
	private var _localeManager : LocaleManager;

	public function ConfigController()
	{

	}

	[PostConstruct]
	public function init () : void
	{
		_localeManager = new LocaleManager();
        _localeManager.localeFolder = "config/";
        _localeManager.localeChain = ["default"];
		_localeManager.addRequiredBundles([
			{locale: "default", bundleName: CONFIG_BUNDLE, useLinebreak: true},
		], onComplete);

	}

	private function onComplete(success:Boolean):void
	{
		if ( success )
		{
			trace("Config successfully added.");
		}
		else
		{
			trace("Config bundles failed.");
		}
	}

	public function getString(key : String) : String
	{
		return _localeManager.getString(CONFIG_BUNDLE, key);
	}

	public function getNumber(key : String) : Number
	{
		return Number(_localeManager.getString(CONFIG_BUNDLE, key));
	}

    public function getArray(key : String) : Array
    {
        var str :String = _localeManager.getString(CONFIG_BUNDLE, key)
        var arr : Array = str.split("[");
        if(arr.length > 1)
        {
            arr = arr[1].split("]")[0].split(",");
            for(var index:int = 0; index < arr.length; index++)
            {
                arr[index] = StringUtil.trim(arr[index]);
            }
            return arr;
        }
        return null;
    }
}

}
