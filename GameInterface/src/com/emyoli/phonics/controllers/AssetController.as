package com.emyoli.phonics.controllers {
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.models.GameModelRepo;
import com.emyoli.phonics.models.GlobalParameters;
import com.emyoli.phonics.net.Gender;
import com.emyoli.phonics.net.UserSetting;
import com.emyoli.phonics.utils.NativeText;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.views.AtlasParser;
import com.emyoli.phonics.views.BaseView;

import flash.display.Bitmap;
import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.events.ErrorEvent;
import flash.events.IOErrorEvent;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.filters.BevelFilter;
import flash.filters.DropShadowFilter;
import flash.filters.GlowFilter;
import flash.system.Capabilities;
import flash.text.Font;
import flash.utils.ByteArray;

import starling.core.Starling;

import starling.display.Image;
import starling.display.Sprite;

import starling.events.Event;

import starling.events.EventDispatcher;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;

import starling.utils.AssetManager;

import starling.utils.formatString;

public class AssetController
{
	public static const DEFAULT_FONT_SIZE : Number = 36;
	public static const DEFAULT_TEXT_WIDTH : Number = 500;
	public static const DEFAULT_TEXT_HEIGHT : Number = 50;
	public static const DEFAULT_FONT_COLOR : uint = 0;

	public static const REGULAR_LABEL_SHADOW_FILTER : DropShadowFilter = new DropShadowFilter(1, 45, 0, 0.20, 2, 2, 4);
	public static const HEADER_LABEL_SHADOW_FILTER : DropShadowFilter = new DropShadowFilter(3, 45, 0, 0.20, 4, 4, 4);
	public static const BUTTON_LABEL_SHADOW_FILTER : DropShadowFilter = new DropShadowFilter(4, 76, 0, 0.20, 4, 4, 5);
	public static const BUTTON_LABEL_GLOW_FILTER : GlowFilter = new GlowFilter(0, 0.10, 10, 10, 12);
	public static const BUTTON_LABEL_BEVEL_FILTER : BevelFilter = new BevelFilter(1,45,0xFFFFFF, 0.50, 0, 0.50, 4, 4, 1);

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	[Inject]
	public var baseView : AtlasParser;

	[Inject (source="gameModel")]
	public var gameModel : GameModelRepo;

	private var scaleFactor : Number = 1;
	private var loaders : Object = {};
	private var callbacks : Object = {};
	private var _assets : AssetManager;

	private static const PROPORTIONS : Array = [1.7, 1.5, 1.3];

	private const TEXTURE_PATH : String = "assetsAndroid/textures/{0}x/";
	private const TEXTURE_WITH_PROPORTIONS_PATH : String = "assetsAndroid/textures/{1}/{0}x/";
    private const AUDIO_PATH : String = "assets/audio/";
    private const JSON_PATH : String = "assets/json/";

	public static const DEFAULT_FONT_TYPE : String = "Verdana";
	public static const FONT_NOVA : String = "Proxima Nova";
	public static const FONT_HOLTWOOD : String = "Holtwood";
	public static const FONT_QUADON : String = "Quadon";


	private var _spritesLibrary : Object = {};
	private var _imagesLibrary : Object = {};
	private var _textFieldsLibrary : Object = {};
	private var _textInputsLibrary : Object = {};

	public function AssetController ()
	{

	}


	public function getTextField(fontSize : Number = DEFAULT_FONT_SIZE, width : Number = DEFAULT_TEXT_WIDTH,
								 height : Number = DEFAULT_TEXT_HEIGHT, color : uint = DEFAULT_FONT_COLOR,
								 ttFont:String = DEFAULT_FONT_TYPE, verticalAlign : Boolean = false) : TextField
	{
		var ttFontSize:int = fontSize * scaleFactor;

		var colorTF : TextField = new TextField(width, height, "", ttFont, ttFontSize);
		colorTF.hAlign = verticalAlign ? "center" : "left";
		colorTF.color = color;
		colorTF.touchable = false;
		return (colorTF);
	}

	/**
	 * Stage text wrapped component. Scale properties depends on Starling scale
	 * @param fontSize
	 * @param width
	 * @param color
	 * @param ttFont
	 * @return
	 */
	public function getTextInput(fontSize : Number = DEFAULT_FONT_SIZE, width : Number = DEFAULT_TEXT_WIDTH,
								 color : uint = DEFAULT_FONT_COLOR,
								 ttFont:String = DEFAULT_FONT_TYPE) : NativeText
	{
		var ttFontSize:int = fontSize;

		var textInput : NativeText = new NativeText();
		textInput.fontSize = ttFontSize;
		textInput.fontWeight = "bold";
		textInput.width = width;
		textInput.borderThickness = 0;
		textInput.borderColor = 0;
		textInput.color = color;
		textInput.fontFamily = DEFAULT_FONT_TYPE;//ttFont;
		textInput.editable = true;
		return (textInput);
	}

	/**
	 * Get Image instance by name
	 * @param image name
	 * @return related Image
	 */
	public function getImage (name : String) : Image
	{
		return new Image(_assets.getTexture(name));
	}

	[PostConstruct]
	public function setupAssetManager () : void
	{
		// Our assets are loaded and managed by the 'AssetManager'. To use that class,
		// we first have to enqueue pointers to all assets we want it to load.

		var appDirListing:Array = File.applicationDirectory.getDirectoryListing();
		var appDir:File = File.applicationDirectory;

		var closestProportions : Number;
		var diff : Number = Number.MAX_VALUE;
		for each (var prop : Number in PROPORTIONS)
		{
			if (Math.abs(GlobalParameters.ASSET_PROPORTION - prop) < diff)
			{
				diff = Math.abs(GlobalParameters.ASSET_PROPORTION - prop);
				closestProportions = prop;
			}
		}

		_assets = new AssetManager(GlobalParameters.ASSET_SCALE_FACTOR);
		_assets.keepAtlasXmls = true;
		_assets.verbose = true; Capabilities.isDebugger;
		_assets.enqueue(
				appDir.resolvePath(AUDIO_PATH),
                appDir.resolvePath(JSON_PATH),
				appDir.resolvePath(formatString(TEXTURE_PATH, GlobalParameters.ASSET_SCALE_FACTOR)),
				/*appDir.resolvePath(formatString("assetsAndroid/textures/{0}x/base", GlobalParameters.ASSET_SCALE_FACTOR)),
				appDir.resolvePath(formatString("assetsAndroid/textures/{0}x/message", GlobalParameters.ASSET_SCALE_FACTOR)),
				appDir.resolvePath(formatString("assetsAndroid/textures/{0}x/cube", GlobalParameters.ASSET_SCALE_FACTOR)),*/
				appDir.resolvePath(formatString(TEXTURE_WITH_PROPORTIONS_PATH, GlobalParameters.ASSET_SCALE_FACTOR, "" + closestProportions))
		);

		_assets.addEventListener(ErrorEvent.ERROR, handleError)

		// Now, while the AssetManager now contains pointers to all the assets, it actually
		// has not loaded them yet. This happens in the "loadQueue" method; and since this
		// will take a while, we'll update the progress bar accordingly.

		_assets.loadQueue(function(ratio:Number):void
		{
			if (ratio == 1)
			{
				baseView.parse("Base");
				dispatcher.dispatchEvent(new GameEvent(GameEvent.ASSETS_LOADED));
			}
		});
	}

	private function handleError(event : ErrorEvent) : void
	{
		trace (event.toString());
	}

	/**
	 * Load specified image and pass in to callback on load, or instantly call callback
	 if the image for specified url exists.
	 * If you try to load an image which is currently loading, one more callback will be added to the queue
	 * @param url
	 * @param callback
	 */
	public function getLoader(url:String, scaleFactor : Number = 1, callback : Function = null) : Loader
	{
		var loader : Loader;
		if (loaders[url])
		{
			loader = loaders[url] as Loader;
			if (callbacks[url])
			{
				addCallbackFor(url, callback);
			}
			else
			{
				callback(loader);
			}
			return loader;
		}

		loader = new Loader();
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
		loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError)
		registerLoader(url, loader, callback);
		//loader.load(new URLRequest(url));

		var bgPath:String = formatString(TEXTURE_PATH + url, scaleFactor);
		var bgFile:File = File.applicationDirectory.resolvePath(bgPath);
		var bytes:ByteArray = new ByteArray();
		var stream:FileStream = new FileStream();
		stream.open(bgFile, FileMode.READ);
		stream.readBytes(bytes, 0, stream.bytesAvailable);
		stream.close();

		loader.loadBytes(bytes);
		loader.scaleX = 1.0 / scaleFactor;
		loader.scaleY = 1.0 / scaleFactor;

		return loader;
	}

	/**
	 * Remove all cached loaders
	 */
	public function cleanUp():void
	{
		for (var url:String in loaders)
		{
			const loader:Loader = loaders[url];
			delete loaders[url];
			delete callbacks[url];
			if (!loader.content)
			{
				loader.close();
				loader.removeEventListener(Event.COMPLETE, onLoadComplete);
			}
		}
	}

	protected function onIOError(event:IOErrorEvent):void
	{
		const contentLoaderInfo:LoaderInfo = event.currentTarget as LoaderInfo;
		doCallbacks(getUrlByLoader(contentLoaderInfo.loader))
	}

	protected function getUrlByLoader(loader:Loader):String
	{
		for (var url:String in loaders)
			if (loaders[url]==loader)
				return url
		return null;
	}

	protected function registerLoader(url:String, loader:Loader, callBack:Function):void
	{
		loaders[url] = loader;
		addCallbackFor(url, callBack);
	}

	protected function addCallbackFor(url:String, callBack:Function):void
	{
		if (!callbacks[url])
		{
			callbacks[url]=new Vector.<Function>;
		}
		const vec:Vector.<Function> = callbacks[url];
		vec.push(callBack);
	}

	protected function onLoadComplete(event:Event):void
	{
		const target : LoaderInfo = event.currentTarget as LoaderInfo;
		target.removeEventListener(Event.COMPLETE, onLoadComplete);
		target.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		(target.content as Bitmap).smoothing = true;
		if (callbacks[target.url])
			doCallbacks(target.url);

	}

	protected function doCallbacks(url:String):void
	{
		for each (var callback:Function in callbacks[url] as Vector.<Function>)
		{
			callback(loaders[url]);
		}
		delete callbacks[url];
	}

	public function get assetsManager() : AssetManager
	{
		return _assets;
	}

	public function addLibrary(key : String, sprites : Object, textFields : Object, textInputs : Object, images : Object) : void
	{
		_spritesLibrary[key] = sprites;
		_imagesLibrary[key] = images;
		_textFieldsLibrary[key] = textFields;
		_textInputsLibrary[key] = textInputs;
	}

	public function getImageFrom (viewId : String, assetId : String) : Image
	{
		if (_imagesLibrary[viewId])
		{
			return _imagesLibrary[viewId][assetId] as Image;
		}else
		{
			try{
				baseView.parse(viewId);
			}catch (e : Error)
			{
				var err = e;
				throw (new Error("Atlas is possibly not exists: " + viewId + "\n" + e.message + "\n" + e.getStackTrace()), e.errorID);
			}

			return getImageFrom(viewId, assetId);
		}

		return null;
	}

	public function getSpriteFrom (viewId : String, assetId : String) : Sprite
	{
		if (_spritesLibrary[viewId])
		{
			return _spritesLibrary[viewId][assetId] as Sprite;
		}else
		{
			try{
				baseView.parse(viewId);
			}catch (e : Error)
			{
				var err = e;
				throw (new Error(e.getStackTrace()), "Atlas does not exist: " + e.errorID);
			}

			return getSpriteFrom(viewId, assetId);
		}

		return null;
	}

	public function getSpriteCloneFrom (viewId : String, assetId : String, zeroCoordinates : Boolean = true, hide : Boolean = false) : Sprite
	{
		var sprite : Sprite = getSpriteFrom(viewId, assetId);
		var image : Image = getImageFrom(viewId, assetId);
		if (sprite && image)
		{
			sprite.visible = !hide;
			var clone : Sprite = new Sprite();
			clone.addChild(new Image(image.texture));
			clone.clipRect = sprite.clipRect;
			clone.x = zeroCoordinates ? 0 : sprite.x;
			clone.y = zeroCoordinates ? 0 : sprite.y;
			clone.pivotX = sprite.pivotX;
			clone.pivotY = sprite.pivotY;
            clone.name = sprite.name;

			return clone;
		}

		return null;
	}

	public function libraryExists(viewId : String) : Boolean
	{
		return Boolean(_spritesLibrary[viewId]);
	}

	public function getAvatar(avatarName : String = "", gender : int = -1) : Sprite
	{
        if(gender == -1)
        {
            gender = gameModel.userSetting.gender;
        }
		avatarName = avatarName ? avatarName : gameModel.userSetting.avatarname;
		avatarName = avatarName && avatarName.search("Avatar") != -1 ? avatarName : "Avatar1";
		const avatar : Sprite = getSpriteCloneFrom(gender == Gender.BOY ? "Avatar_popup" : "Avatar", avatarName, true, false);
		avatar.name = avatarName;
		return avatar;
	}


}
}
