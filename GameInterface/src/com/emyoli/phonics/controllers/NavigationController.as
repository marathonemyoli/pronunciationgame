package com.emyoli.phonics.controllers {
import com.creativebottle.starlingmvc.binding.Bindings;
import com.emyoli.phonics.events.MessageEvent;
import com.emyoli.phonics.factories.BeanInstanceFactory;
import com.emyoli.phonics.models.MessageModel;
import com.emyoli.phonics.views.BaseView;
import com.emyoli.phonics.views.message.MessagePopup;

import flash.utils.Dictionary;

import starling.display.DisplayObject;

import starling.display.DisplayObject;


import starling.display.Sprite;
import starling.filters.BlurFilter;
import starling.filters.FragmentFilter;

public class NavigationController
{
	private var _openedPopups : Vector.<Sprite> = new Vector.<Sprite>();

	private var _createdViews : Vector.<DisplayObject> = new Vector.<DisplayObject>();

	[Inject]
	public var viewManager : FixViewManager;

	[Inject]
	public var factory : BeanInstanceFactory;

	[Bindings]
	public var bindings : Bindings;

	public var _messagePopupMap : Dictionary = new Dictionary(true);

	[EventHandler(event="NavigationEvent.NAVIGATE_TO_VIEW", properties="viewClass, data")]
	public function navigateToView(viewClass : Class, data : Object = null):void
	{
		var existingView : DisplayObject = getExistingView(viewClass);
		if (0)//existingView)
		{
			//TODO: Add [PersistView] metadata tag to keep some views in memory;
		}else
		{
			if (_createdViews.length)
			{
				var viewToRemove : DisplayObject = _createdViews[_createdViews.length - 1];
				clearBindings(viewToRemove);
				viewManager.removeView(viewToRemove, true);
			}
			const view : DisplayObject = viewManager.setView(viewClass, data);
			_createdViews.push(view);
			//TODO: Add [DoNotDispose] to set fals/true values of dispose parameter for setView method
		}
	}

	private function clearBindings(viewToRemove : DisplayObject) : void
	{
		bindings.removeBindingsForTarget(viewToRemove);
	}

	private function getExistingView(viewClass : Class) : DisplayObject
	{
		for each (var view : DisplayObject in _createdViews)
		{
			if (view is viewClass)
			{
				return view;
			}
		}
		return view;
	}


	[EventHandler(event="NavigationEvent.OPEN_POPUP", properties="viewClass, data")]
	public function openPopup(popupClass : Class, constructorObject : Object = null) : DisplayObject
	{
		var popup : DisplayObject = (constructorObject ?  new popupClass(constructorObject) : new popupClass()) as DisplayObject;
		viewManager.addView(popup);
		_openedPopups.push(popup);
		return popup;
	}

	[EventHandler(event="NavigationEvent.CLOSE_POPUP", properties="viewClass, data")]
	public function closePopup(popupClass : Class, data : Object = null) : void
	{
		if (data)
		{
			removePopup(data as Sprite);
			return;
		}

		for each (var popup : Sprite in _openedPopups)
		{
			if (popup is popupClass)
			{
				removePopup(popup);
				return;
			}
		}
	}

	[EventHandler(event="MessageEvent.CREATE_MESSAGE", properties="messageModel, data")]
	public function showMessage (messageModel : MessageModel, data : Object) : void
	{
		const popup : MessagePopup = openPopup(MessagePopup, messageModel) as MessagePopup;
        popup.data = data;
		_messagePopupMap[messageModel] = popup;
	}

	[EventHandler(event="MessageEvent.CLOSE")]
	public function yesMessageHandler (event : MessageEvent) : void
	{
		removePopup(event.data ? event.data as MessagePopup : _messagePopupMap[event.messageModel]);
		delete _messagePopupMap[event.messageModel];
	}

	private function removePopup(sprite : Sprite) : void
	{
		viewManager.removeView(sprite, true);
		_openedPopups.splice(_openedPopups.indexOf(sprite), 1);
	}



}
}
