/**
 * Created by alekseykabanov on 09.09.15.
 */
package com.emyoli.phonics.controllers {
import com.emyoli.phonics.events.MessageEvent;
import com.emyoli.phonics.models.MessageModel;

import flash.events.TimerEvent;

import flash.utils.Dictionary;
import flash.utils.Timer;

import starling.events.EventDispatcher;

public class DelayedMessageController {

    [Dispatcher]
    public var dispatcher : EventDispatcher;

    private var _dictionary : Dictionary = new Dictionary();
    public function DelayedMessageController()
    {
    }

    [EventHandler(event="MessageEvent.ADD_DELAY_MESSAGE", properties="messageModel, data")]
    public function addMessage (messageModel : MessageModel, data : Object) : void
    {
        var aTimer : Timer = _dictionary[messageModel] = new Timer(messageModel.showTime * 1000, 1);
        aTimer.addEventListener(TimerEvent.TIMER, function(e : TimerEvent){
            dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CREATE_MESSAGE, messageModel));
        });
        aTimer.start();
    }
    [EventHandler(event="MessageEvent.ABORT_DELAY_MESSAGE", properties="messageModel, data")]
    public function abortMessage (messageModel : MessageModel, data : Object) : void
    {
        if(_dictionary[messageModel] != null)
        {
            _dictionary[messageModel].stop();
            _dictionary[messageModel] = null;
        }
    }
}
}
