package com.emyoli.phonics.events {
import com.awar.ags.api.Packet;
import com.emyoli.phonics.net.LoginResp;

import starling.events.Event;

public class NetLoginEvent extends Event
{
	public static const LOGIN : String = "LOGIN";

	private var _response : LoginResp;

	public function NetLoginEvent(e : Packet)
	{
		super(LOGIN);

		_response = new LoginResp();
		_response.mergeFrom( e.Data );
	}

	public function get response() : LoginResp
	{
		return _response;
	}
}
}
