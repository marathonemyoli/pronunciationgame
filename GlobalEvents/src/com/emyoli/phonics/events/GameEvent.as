package com.emyoli.phonics.events
{
import starling.events.Event;

public class GameEvent extends Event
{
    public static const GAME_END : String = "GAME_END";
    public static const GAME_INITIALIZED : String = "GAME_INITIALIZED";
	public static const SERVER_CONNECTED : String = "SERVER_CONNECTED";
	public static const ASSETS_LOADED : String = "ASSETS_LOADED";

	public static const AVATAR_SELECTED : String = "AVATAR_SELECTED";
	public static const AVAILABLE_FRIENDS_RECEIVED : String = "AVAILABLE_FRIENDS_RECEIVED";
	public static const MY_SCORE_RECEIVED : String = "MY_SCORE_RECEIVED";

	public static const ITEM_SELECTED : String = "ITEM_SELECTED";
	public static const USER_SETTING_CHANGE : String = "USER_SETTING_CHANGE";

    public static const RECOGNITION_INCOME : String = "RECOGNITION_INCOME";

	public function GameEvent (type:String, bubbles:Boolean = false, data:Object = null)
	{
		super (type, bubbles, data)
	}
}
}
