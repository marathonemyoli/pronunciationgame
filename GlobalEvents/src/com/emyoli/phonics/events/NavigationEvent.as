package com.emyoli.phonics.events
{
import starling.events.Event;

public class NavigationEvent extends Event
{
	public static const NAVIGATE_TO_VIEW : String = "NAVIGATE_TO_VIEW";
	public static const OPEN_POPUP : String = "OPEN_POPUP";
	public static const CLOSE_POPUP : String = "CLOSE_POPUP";


	private var _viewClass : Class;

	public function NavigationEvent(type : String, viewClass : Class, data : Object = null)
	{
		super(type, false, data);
		
		_viewClass = viewClass;
	}

	public function get viewClass() : Class
	{
		return _viewClass;
	}
}
}
