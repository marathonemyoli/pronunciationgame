package com.emyoli.phonics.events {
import starling.events.Touch;
import starling.events.TouchEvent;

public class GameTouchEvent extends TouchEvent {
	public static var CLICK : String = "click";
	public static var TOUCH_BEGIN : String = "touchBegin";

	public function GameTouchEvent(type : String, touches : Vector.<Touch>, shiftKey : Boolean = false, ctrlKey : Boolean = false, bubbles : Boolean = true)
	{
		super(type, touches, shiftKey, ctrlKey, bubbles);
	}

	public static function clickFromTouchEvent(e : TouchEvent) : GameTouchEvent
	{
		const event : GameTouchEvent = new GameTouchEvent(CLICK, e.touches, e.shiftKey, e.ctrlKey, e.bubbles);
		return event;
	}

	public static function downFromTouchEvent(e : TouchEvent) : GameTouchEvent
	{
		const event : GameTouchEvent = new GameTouchEvent(TOUCH_BEGIN, e.touches, e.shiftKey, e.ctrlKey, e.bubbles);
		return event;
	}
}
}
