package com.emyoli.phonics.events {
import com.emyoli.phonics.models.MessageModel;

import starling.events.Event;

public class MessageEvent extends Event {

	public static const ABORT_DELAY_MESSAGE : String = "ABORT_DELAY_MESSAGE";
	public static const ADD_DELAY_MESSAGE : String = "ADD_DELAY_MESSAGE";
	public static const CREATE_MESSAGE : String = "CREATE_MESSAGE";
	public static const YES_SELECTED : String = "YES_SELECTED";
	public static const NO_SELECTED : String = "NO_SELECTED";
	public static const CLOSE : String = "CLOSE";


	private var _messageModel : MessageModel;

	public function MessageEvent(type : String, messageModel : MessageModel = null, data : Object = null)
	{
		_messageModel = messageModel;
		super(type, bubbles, data);
	}

	public function get messageModel () : MessageModel
	{
		return _messageModel;
	}
}
}
