package com.emyoli.phonics.events {
import com.awar.ags.api.Packet;
import com.emyoli.phonics.net.CmdType;
import com.emyoli.phonics.net.LoginResp;
import com.emyoli.phonics.net.SignupResp;

import starling.events.Event;

public class NetSignupEvent extends Event
{
	public static const SIGNUP : String = "SIGNUP";

	private var _response : SignupResp;

	public function NetSignupEvent(e : Packet)
	{
		super(SIGNUP);

		_response = new SignupResp();
		_response.mergeFrom( e.Data );
	}

	public function get response() : SignupResp
	{
		return _response;
	}
}
}
