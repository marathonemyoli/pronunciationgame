package com.emyoli.phonics.events {
import starling.events.Event;

public class DataUpdate extends Event {
	public static const DATA_UPDATE : String = "DataUpdate";

	public function DataUpdate(property : String, data : Object = null)
	{
		super(DATA_UPDATE + "." + property, true, data);
	}
}
}
