package com.emyoli.phonics.net {
import com.awar.ags.api.ConnectionResponse;
import com.awar.ags.api.MessageType;
import com.awar.ags.api.Packet;
import com.awar.ags.api.Ping;
import com.awar.ags.connection.AvailableConnection;
import com.awar.ags.connection.TransportType;
import com.awar.ags.engine.AgsEngine;
import com.awar.ags.engine.Server;
import com.emyoli.phonics.events.NetLoginEvent;
import com.emyoli.phonics.events.NetSignupEvent;
import com.netease.protobuf.Message;

import flash.system.Security;

import starling.events.Event;

import starling.events.EventDispatcher;

public class NetConnector {
	private static const EVENT_MAP : Array = [];
	{
		EVENT_MAP [CmdType.LoginResp] = NetLoginEvent;
		EVENT_MAP [CmdType.LoginErr] = NetLoginEvent;
		EVENT_MAP [CmdType.SignupResp] = NetSignupEvent;
		EVENT_MAP [CmdType.GetAvatarName] = com.emyoli.phonics.events.GameEvent.AVATAR_SELECTED;
		EVENT_MAP [CmdType.AvailableFriendsList] = com.emyoli.phonics.events.GameEvent.AVAILABLE_FRIENDS_RECEIVED;
		EVENT_MAP [CmdType.SetUserSetting] = com.emyoli.phonics.events.GameEvent.USER_SETTING_CHANGE;
		EVENT_MAP [CmdType.MyScoreBoard] = com.emyoli.phonics.events.GameEvent.MY_SCORE_RECEIVED;
	}

	[Dispatcher]
	public var dispatcher : EventDispatcher;

	private var ags : AgsEngine = new AgsEngine();

	public var isconnected : Boolean = false;

	private var idsess : int;

	private var loginn : String;
	private var passs : String;

	public function NetConnector()
	{

	}

	public function connect() : void
	{
//        Security.loadPolicyFile( "xmlsocket://127.0.0.1:1843" );
		Security.loadPolicyFile("xmlsocket://178.62.235.129:1843");

		//listen to key events to know when a connection has succeeded (or failed), and when login has succeeded (or failed)
		ags.addEventListener(MessageType.ConnectionAttemptResponse.name, onConnectionResponse);
		ags.addEventListener(MessageType.ConnectionResponse.name, onConnectionResponse);
		ags.addEventListener(MessageType.Packet.name, onDataReceived);
		ags.addEventListener(MessageType.Ping.name, onPingReceived);

		var server : Server = new Server("server1");
//        var availConn:AvailableConnection = new AvailableConnection("127.0.0.1", 8888, TransportType.TCP);
		var availConn : AvailableConnection = new AvailableConnection("178.62.235.129", 8888, TransportType.TCP);
		server.addAvailableConnection(availConn);

		//register this server with the API
		ags.addServer(server);

		//tell the API to connect. it will cycle through all available connections until one is made
		ags.connect();
	}

	//-----
	private function onConnectionResponse(e : ConnectionResponse) : void
	{
		isconnected = e.successful;

		if (isconnected)
		{
			dispatcher.dispatchEvent(new com.emyoli.phonics.events.GameEvent(com.emyoli.phonics.events.GameEvent.SERVER_CONNECTED));
		}
		else
		{
			trace("ERROR CONNECT");
		}
	}

	private function onPingReceived(e : Ping) : void
	{
		//main.addPing(e.CreationTime);
	}

	//-----
	private function onDataReceived(e : Packet) : void
	{
		var eventType : * = EVENT_MAP[e.Cmd];
		var event : Event;
		if (eventType is Class)
		{
			event = new eventType (e);
		}else
		{
			const commandClass : Class = CmdType.COMMAND_MAP[e.Cmd] as Class;
			const data : Message = new commandClass ();
			data.mergeFrom(e.Data);
			event = new Event(eventType, false, data);
			dispatcher.dispatchEvent(new Event(""+e.Cmd, false, data));
		}
		dispatcher.dispatchEvent(event);
	}

	public function sendMessage (data : Message, commandType : int) : void
	{
		var packet : Packet = new Packet();
		packet.Cmd = commandType;
		data.writeTo(packet.Data);

		ags.send(packet);
	}

}
}
