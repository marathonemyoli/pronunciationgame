/**
 * Created by alekseykabanov on 01.09.15.
 */
package com.emyoli.phonics.models.recognition
{
import com.emyoli.phonics.models.recognition.base.BaseScoreImprovement;

public class Word extends BaseScoreImprovement
{
    private var mGraphemes:Vector.<Grapheme> = new Vector.<Grapheme>();

    public function Word(inData:Object)
    {
        super (inData, "Word");
        if (inData.hasOwnProperty("Graphemes"))
        {
            var arr:Array = inData["Graphemes"];
            for each(var aObj:Object in arr)
            {
                mGraphemes.push(new Grapheme(aObj))
            }
        }
    }

    public function get Graphemes():Vector.<Grapheme>
    {
        return mGraphemes;
    }
}
}
