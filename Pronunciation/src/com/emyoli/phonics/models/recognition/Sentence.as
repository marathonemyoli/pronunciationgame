/**
 * Created by alekseykabanov on 01.09.15.
 */
package com.emyoli.phonics.models.recognition
{
import com.emyoli.phonics.models.recognition.base.BaseScoreImprovement;

public class Sentence extends BaseScoreImprovement
{
    private var mWords:Vector.<Word> = new Vector.<Word>();

    public function Sentence(inData:Object)
    {
        super(inData, "Sentence");
        if (inData.hasOwnProperty("Words"))
        {
            var arr:Array = inData["Words"];
            for each(var aObj:Object in arr)
            {
                mWords.push(new Word(aObj))
            }
        }
    }

    public function get words():Vector.<Word>
    {
        return mWords;
    }
}
}
