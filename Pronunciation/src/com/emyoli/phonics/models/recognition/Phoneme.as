/**
 * Created by alekseykabanov on 01.09.15.
 */
package com.emyoli.phonics.models.recognition
{
import com.emyoli.phonics.models.recognition.base.BaseScoreImprovement;

public class Phoneme extends BaseScoreImprovement
{
    public static const SAME : String = "Same";
    public static const BETTER : String = "Better";
    public static const WORSE : String = "Worse";

    public function Phoneme(inData:Object)
    {
        super (inData, "Phoneme")
    }
}
}
