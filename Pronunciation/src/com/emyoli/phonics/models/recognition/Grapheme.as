/**
 * Created by alekseykabanov on 01.09.15.
 */
package com.emyoli.phonics.models.recognition
{
import com.emyoli.phonics.models.recognition.base.BaseName;

public class Grapheme extends BaseName
{
    private var mPhonemes:Vector.<Phoneme> = new Vector.<Phoneme>();

    public function Grapheme(inData:Object)
    {
        super(inData["Grapheme"]);
        if (inData.hasOwnProperty("Phonemes"))
        {
            var arr:Array = inData["Phonemes"];
            for each(var aObj:Object in arr)
            {
                mPhonemes.push(new Phoneme(aObj))
            }
        }
    }


    public function get Phonemes():Vector.<Phoneme>
    {
        return mPhonemes;
    }
}
}
