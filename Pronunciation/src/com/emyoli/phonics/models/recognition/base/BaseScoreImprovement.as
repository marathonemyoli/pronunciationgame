/**
 * Created by alekseykabanov on 01.09.15.
 */
package com.emyoli.phonics.models.recognition.base
{
public class BaseScoreImprovement extends BaseName
{
    protected var mScore          :Number = 0;
    protected var mImprovement    :String = "";

    public function BaseScoreImprovement(inData:Object, nameClass:String)
    {
        super (inData[nameClass]);
        mImprovement = inData["Improvement"];
        mScore = inData["Score"];
    }

    public function get improvement():String
    {
        return mImprovement;
    }

    public function get score():Number
    {
        return mScore;
    }
}
}
