/**
 * Created by alekseykabanov on 01.09.15.
 */
package com.emyoli.phonics.models.recognition.base
{
public class BaseName
{
    protected var mName:String = "";
    public function BaseName(inName:String)
    {
        mName = inName;
    }
    public function get name():String
    {
        return mName;
    }
}
}
