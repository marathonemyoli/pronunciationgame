/**
 * Created by alekseykabanov on 01.09.15.
 */
package com.emyoli.phonics.net
{
import flash.events.Event;

public class PronunciationServiceEvent extends Event
{
    static public const CONNECTED           :String = "CONNECTED";
    static public const DISCONNECT          :String = "DISCONNECT";
    static public const CONNECTION_ERROR    :String = "CONNECTION_ERROR";
    static public const RECOGNITION_ERROR   :String = "RECOGNITION_ERROR";
    static public const RECOGNITION         :String = "RECOGNITION";
    static public const SCORES              :String = "SCORES";

    private var _data   : Object = null;
    private var _delay  : Number = 0;
    public function PronunciationServiceEvent(type:String, data:Object = null, inDelay:Number = 0, bubbles:Boolean = false, cancelable:Boolean = false)
    {
        super(type, bubbles, cancelable);
        _data = data;
        _delay = inDelay;
    }

    public function get data():Object
    {
        return _data;
    }

    public function get delay():Number
    {
        return _delay;
    }
}
}
