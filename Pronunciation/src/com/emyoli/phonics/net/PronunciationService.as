package com.emyoli.phonics.net
{
import com.emyoli.phonics.models.recognition.Sentence;
import com.sociodox.utils.Base64;

import es.xperiments.media.StageWebViewDisk;
import es.xperiments.media.StageWebviewDiskEvent;
import es.xperiments.media.StageWebViewBridge;
import es.xperiments.media.StageWebViewBridgeEvent;
import flash.display.Stage;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.SampleDataEvent;
import flash.media.Microphone;
import flash.utils.ByteArray;
import flash.utils.getTimer;

public class PronunciationService extends EventDispatcher
{
    private var view        :StageWebViewBridge;
    private var _mic        :Microphone;
    private var isStart     :Boolean = false;
    private var soundBytes  :ByteArray;
    private var isConnected :Boolean = false;
    private var _ltime      :Number;

    public function PronunciationService(inStage:flash.display.Stage)
    {
        trace("PronunciationService init");

        /* init the disk filesystem */
        StageWebViewDisk.addEventListener(StageWebviewDiskEvent.END_DISK_PARSING, onInit );
        StageWebViewDisk.setDebugMode( true );
        StageWebViewDisk.initialize(inStage);
        initMicrophone();
    }

    // PRIVATE ---------------

    /* Fired when StageWebviewDiskEvent cache process finish */
    private function onInit( e:StageWebviewDiskEvent ):void
    {
        trace('PronunciationService END_DISK_PARSING');

        // create the view
        view = new StageWebViewBridge( 0,0, 0,0 );

        // listen StageWebViewBridgeEvent.DEVICE_READY event to be sure the communication is ok
        view.addEventListener(StageWebViewBridgeEvent.DEVICE_READY, onDeviceReady );

        // add a callback method for the function we like to call from Javascript
        view.addCallback('fnCalledFromJS', fnCalledFromJS );

        // load the localfile demo.html ( inside the www dir )
        view.loadLocalURL('applink:/ExampleComplexData.html');
    }

    private function onDeviceReady( e:Event ):void
    {
        connect();
    }

    private function fnCalledFromJS( data:Object /*Object with a "text" prop*/ ):void
    {
        var dTime:Number = 0;
        if(_ltime > 0)
        {
            var dTime:Number = getTimer() - _ltime;
            _ltime = 0;
        }
        if(data["title"] == "connected")
        {
            isConnected = true;
            dispatchEvent(new PronunciationServiceEvent(PronunciationServiceEvent.CONNECTED, null, dTime))
        }else if(data["title"] == "disconnect"){
            isConnected = false;
            dispatchEvent(new PronunciationServiceEvent(PronunciationServiceEvent.DISCONNECT, null, dTime))
        }else if(data["title"] == "error_connect"){
            isConnected = false;
            dispatchEvent(new PronunciationServiceEvent(PronunciationServiceEvent.CONNECTION_ERROR, null, dTime))
        }else if(data["title"] == "result-ready"){
            var aResult:Object = JSON.parse(data.text);
            if(aResult.hasOwnProperty("error"))
            {
                dispatchEvent(new PronunciationServiceEvent(PronunciationServiceEvent.RECOGNITION_ERROR, aResult, dTime))
            }else
            {
                dispatchEvent(new PronunciationServiceEvent(PronunciationServiceEvent.RECOGNITION, new Sentence(aResult["result"]), dTime))
            }
        }else if(data["title"] == "result-score"){
            var aResult:Object = JSON.parse(data.text);
            dispatchEvent(new PronunciationServiceEvent(PronunciationServiceEvent.SCORES, aResult["result"], dTime));
        }
    }

    private function callJavascriptFunction( e:Event ):void
    {
        // call javascript fnCalledFromAs3 function from As3
        //view.call('fnCalledFromAs3',null,{ title:'AS3 Example Basic', text:moreData.text});
    }

    private function initMicrophone():void
    {
        _mic = Microphone.getMicrophone();
        _mic.rate = 44;
        _mic.setUseEchoSuppression(true);
    }

    private function handleMicrophoneData(event : SampleDataEvent):void
    {
        while(event.data.bytesAvailable)
        {
            event.data.readShort();
            var sample:int = event.data.readShort();

            soundBytes.writeInt(sample);
            if (soundBytes.length == 2048)
            {
                const base64 : String = Base64.encode(soundBytes);
                view.call('audioFunction',null, base64)//JSON.stringify(array));
                soundBytes = new ByteArray();
            }
        }
    }

    // PUBLIC ---------------

    /**
     * Start recording for word
     * @param inWord string for pronunciation
     */
    public function startRec(inWord:String):void
    {
        if(!isConnected || isStart) return;
        isStart = true;

        view.call('startFunction',null,inWord);
        soundBytes = new ByteArray();
        _mic.addEventListener(SampleDataEvent.SAMPLE_DATA, handleMicrophoneData)
    }

    /**
     * Stop recording and send to server
     */
    public function stopRec():void
    {
        if(!isConnected || !isStart) return;
        isStart = false;

        _mic.removeEventListener(SampleDataEvent.SAMPLE_DATA, handleMicrophoneData)
        view.call('stopFunction',null,null);
        _ltime = getTimer();
    }


    /**
     * Get scores
     */
    public function getScores():void
    {
        if(!isConnected) return;
        view.call('getScoresFunction',null,null);
    }

    /**
     * Disconnect from server
     */
    public function disconnect():void
    {
        if(!isConnected) return;
        isConnected = false;
        view.call('disconnectFromServer',null,null);
    }

    /**
     * Connect to SERVER
     */
    public function connect():void
    {
        if(isConnected) return;
        view.call('connectToServer',null,null);
    }

}
}
