
package com.emyoli.phonics.models {
public class GlobalParameters {
	public static var SCALE : Number;
	public static var ASSET_SCALE_FACTOR : Number;
	public static var ASSET_PROPORTION : Number = StageHeight / StageWidth;

	public static const AssetWidth:int  = 720;
	public static const AssetHeight:int = 1280;

	public static var StageWidth:int  = AssetWidth;
	public static var StageHeight:int = AssetHeight;

	public static var test : Object;
}
}
