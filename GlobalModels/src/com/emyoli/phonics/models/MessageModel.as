package com.emyoli.phonics.models {
public class MessageModel {

    public static const DEFAULT_HIDE_TIME : Number = 2;
    public static const DEFAULT_SHOW_TIME : Number = 2;

    public static const TYPE_GREEN : String = "message_back_green";
    public static const TYPE_RED : String = "message_back_red";
    public static const TYPE_BLUE : String = "message_back_blue";

	public var headerText : String;
	public var contentText : String;

	public var buttonOkLabel : String;
	public var buttonCancelLabel : String

	public var showCancel : Boolean = false;

    public var hideTime : Number = 0;
    public var showTime : Number = 0;

	public var messageType : String = TYPE_RED;

	public static function buildForNotification(header : String, message : String, okLabel : String = "") : MessageModel
	{
		var model : MessageModel = new MessageModel();
		model.showCancel = false;
		model.headerText = header;
		model.contentText = message;
		model.buttonOkLabel = okLabel;
		return model;
	}

	public static function buildForTutorial(message : String, okLabel : String = "", hideTime : Number = DEFAULT_HIDE_TIME, showTime : Number = DEFAULT_SHOW_TIME) : MessageModel
	{
		var model : MessageModel = new MessageModel();
		model.showCancel = false;
		model.contentText = message;
		model.buttonOkLabel = okLabel;
        model.hideTime = hideTime;
        model.showTime = showTime;
		model.messageType = TYPE_BLUE;
		return model;
	}
}
}
