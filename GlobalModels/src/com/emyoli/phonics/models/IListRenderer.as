package com.emyoli.phonics.models
{
public interface IListRenderer
{
	function set model(value : Object) : void;
	function init() : void;
	function draw() : void;

	function get id() : int;
	function set id(id : int) : void;

	function get y() : Number;
	function set y(y : Number) : void;
	function get x() : Number;
	function set x(x : Number) : void;


}
}
