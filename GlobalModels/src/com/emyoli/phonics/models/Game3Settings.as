/**
 * Created by alekseykabanov on 04.09.15.
 */
package com.emyoli.phonics.models {
public class Game3Settings {
    public var time : int;
    public var reval : int;
    public var cardsIndex : int;

    public function Game3Settings(inTime : int, inReval : int, inCardsIndex:int)
    {
        time = inTime;
        reval = inReval;
        cardsIndex = inCardsIndex;
    }

}
}
