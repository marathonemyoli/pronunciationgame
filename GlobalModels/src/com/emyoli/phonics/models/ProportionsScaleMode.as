package com.emyoli.phonics.models
{
public class ProportionsScaleMode
{
	public static const NONE : int = -1;
	public static const LEFT : int = 0;
	public static const MIDDLE: int = 1;
	public static const RIGHT: int = 2;
}
}
