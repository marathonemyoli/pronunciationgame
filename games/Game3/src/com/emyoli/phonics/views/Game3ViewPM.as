package com.emyoli.phonics.views {
import com.emyoli.phonics.components.GameCubeContainer;
import com.emyoli.phonics.controllers.AssetController;
import com.emyoli.phonics.controllers.ConfigController;
import com.emyoli.phonics.events.GameEvent;
import com.emyoli.phonics.events.MessageEvent;
import com.emyoli.phonics.models.Game3Settings;
import com.emyoli.phonics.models.MessageModel;
import com.emyoli.phonics.models.recognition.Grapheme;
import com.emyoli.phonics.models.recognition.Phoneme;
import com.emyoli.phonics.models.recognition.Sentence;
import com.emyoli.phonics.models.recognition.Word;
import com.emyoli.phonics.net.NetConnector;
import com.emyoli.phonics.net.PronunciationService;
import com.emyoli.phonics.net.PronunciationServiceEvent;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.utils.errorGet;
import com.emyoli.phonics.utils.localeGet;

import flash.events.TimerEvent;

import flash.text.TextField;
import flash.utils.Timer;
import flash.utils.getTimer;

import starling.events.Event;

import starling.events.EventDispatcher;
import starling.events.KeyboardEvent;

public class Game3ViewPM {
    public static const CREATE : String = "CREATE";
    public static const INTRO : String = "INTRO";
    public static const WAIT_TURN : String = "WAIT_TURN";
    public static const MY_TURN : String = "MY_TURN";

    public static const COUNT_CUBE_TYPES : Array = [16, 24, 36];

    [Inject]
    public var assetController : AssetController;

    [Dispatcher]
    public var dispatcher : EventDispatcher;

    [Inject]
    public var config : ConfigController;

    [Inject]
    public var recognitionService : PronunciationService;

    private var _typeGame : int;

    private var _selectIndexCube : int = -1;
    private var _wordWin : Array = new Array();
    private var _lastWin : int = -1;

    private var _view : Game3View = null;
    private var _model : Game3Model = null;

    private var _currentState : String = CREATE;
    private var _targetState : String = WAIT_TURN;

    private var _timer : Number = 0;
    private var _startTimer : Number = 0;
    private var _isFinishTime : Boolean;

    public var libraryWords : Array = null;
    public var selectedWords : Array = new Array();

    private var _gameOverMessage : MessageModel;
    private var _isEnd : Boolean = false;

    private var _sequence : int = 0;
    private var _recognitionTimer : Timer;
    private var _pendingMessage : MessageModel;
    private var _dataRecognition : Sentence;
    private var _recognitionMessage : MessageModel;

    private var _waitMessageModel : MessageModel = null;

    public function Game3ViewPM()
    {
    }

    /**
     * Begin to the start
     */
    public function init(inGame3View : Game3View) : void
    {
        _model = new Game3Model();
        _view = inGame3View;
        _typeGame = (_view.data as Game3Settings).cardsIndex;
        libraryWords = assetController.assetsManager.getObject("Game3Map")["Game_3"];
        randomChooseWords();
        _timer = (_view.data as Game3Settings).time * 1000 * 60;
        _isFinishTime = (_view.data as Game3Settings).time > 0;

        _view.addEventListener(KeyboardEvent.KEY_DOWN, function (e : KeyboardEvent)
        {
            if (e.keyCode == 38)
            {
                winCube();
            }
        })
    }

    [EventHandler(event="MessageEvent.CLOSE", properties="messageModel, data")]
    public function showMessage (messageModel : MessageModel, data : Object) : void
    {
        if(_recognitionMessage == messageModel)
        {
            var sentence : Sentence = _dataRecognition;

            if (sentence != null && _selectIndexCube >= 0)
            {
                if (isWin(sentence))
                {
                    winCube();
                } else
                {
                    resetSequence();
                    loseCube();
                }
            }
        }else if(_recognitionMessage == _gameOverMessage)
        {
            dispatcher.dispatchEvent(new GameEvent(GameEvent.GAME_END));
        }
    }

    private function resetSequence() : void
    {
        _sequence = 0;
    }

    private function loseCube() : void
    {
        _view.loseCube(_selectIndexCube);
        if (_lastWin >= 0)
        {
            _view.loseCube(_lastWin);
            _lastWin = -1;
        }
    }

    private function winCube() : void
    {
        if (_lastWin >= 0)
        {
            addScore(config.getNumber("game3.score_pair_found") * _sequence);
            _wordWin.push(_lastWin);
            _wordWin.push(_selectIndexCube);
            _view.removeCube(_lastWin);
            _view.removeCube(_selectIndexCube);
            _lastWin = -1;

            if(_wordWin.length == selectedWords.length)
            {
                endGame(false);
            }

        } else
        {
            _lastWin = _selectIndexCube;
            _view.winCube(_selectIndexCube);
        }
    }

    private function addScore(inScore : int) : void
    {
        _model.addScore(inScore);
        _view.setScore(_model.score);
    }

    public function isWin(inSentence : Sentence) : Boolean
    {
        var selectWordObject : Object = libraryWords[selectedWords[_selectIndexCube]];
        var aTF:flash.text.TextField = new flash.text.TextField();
        aTF.htmlText = selectWordObject.text;
        var sayWord = aTF.text;

        for each (var word : Word in inSentence.words)
        {
            if (word.name != sayWord) continue;
            for each (var grapheme : Grapheme in word.Graphemes)
            {
                for each (var phoneme : Phoneme in grapheme.Phonemes)
                {
                    if (phoneme.name == selectWordObject["phoneme"])
                    {

                        if (phoneme.improvement != Phoneme.WORSE)
                        {
                            addScore(config.getNumber("game3.score_correct_pronunciation"));
                            return true;
                        } else
                        {
                            addScore(config.getNumber("game3.score_incorrect_pronunciation"));
                            return false;
                        }
                    }
                }
            }
        }
        return false;
    }
    public function stateView(inState : String, inData : Object = null)
    {
        waitMessageModel = null;
        switch (inState)
        {
            case Game3View.STATE_READY_TO_INTRO:
                state = INTRO;
                break;
            case Game3View.STATE_READY_TO_GAME:
                state = WAIT_TURN;
                break;
            case Game3View.STATE_OPEN_WORD:
                var aSelectIndexCube : int = (inData as GameCubeContainer).indexCube;
                if(_lastWin >= 0 && int(selectedWords[aSelectIndexCube] / 2) != int(selectedWords[_lastWin]) / 2)
                {
                    _view.enableTouch = false;

                    goTimer((_view.data as Game3Settings).reval, function(e:TimerEvent){
                        _view.loseCube(_selectIndexCube);
                        _view.loseCube(aSelectIndexCube);
                        _lastWin = _selectIndexCube = -1;
                    });

                }else{
                    waitMessageModel = MessageModel.buildForTutorial(localeGet("game3s.cubicle.faceup.touch"));
                    _selectIndexCube = aSelectIndexCube;
                }
                break;
            case Game3View.STATE_SAY_WORD:
                _view.enableTouch = false;
                dispatcher.addEventListener(GameEvent.RECOGNITION_INCOME, handleRecognitionIncome);
                startRec();
                break;
            case Game3View.STATE_CLOSE_WORD:

                break;
            case Game3View.ROLL_CUBE:
                addScore(config.getNumber("game3.score_each_role_of_the_cube"));
                break;
            case Game3View.CLOSE_CUBE:
                resetSequence();
                break;
        }
    }

    private function set waitMessageModel(inValue : MessageModel) : void
    {
        if(_waitMessageModel != null)
        {
            dispatcher.dispatchEvent(new MessageEvent(MessageEvent.ABORT_DELAY_MESSAGE, _waitMessageModel));
        }
        _waitMessageModel = inValue;
        if(_waitMessageModel != null)
        {
            dispatcher.dispatchEvent(new MessageEvent(MessageEvent.ADD_DELAY_MESSAGE, _waitMessageModel));
        }
    }

    private function startRec() : void
    {
        var selectWordObject : Object = libraryWords[selectedWords[_selectIndexCube]];
        var aTF:flash.text.TextField = new flash.text.TextField();
        aTF.htmlText = selectWordObject.text;
        var sayWord = aTF.text;

        _recognitionTimer = new Timer(config.getNumber("game3.linguistic_record_time"), 1);
        _recognitionTimer.addEventListener(TimerEvent.TIMER_COMPLETE, handleRecognitionTimeOut);
        _recognitionTimer.start();

        recognitionService.startRec(sayWord);
        
    }
    private function handleRecognitionTimeOut(e : TimerEvent) : void
    {
        _recognitionTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, handleRecognitionTimeOut);

        _pendingMessage = new MessageModel();
        _pendingMessage.headerText = 'Please wait';
        _pendingMessage.contentText = 'Waiting for pronunciation server response...';
        _pendingMessage.messageType = MessageModel.TYPE_BLUE;
        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CREATE_MESSAGE, _pendingMessage));
        recognitionService.addEventListener(PronunciationServiceEvent.RECOGNITION, handleRecognized);
        recognitionService.addEventListener(PronunciationServiceEvent.RECOGNITION_ERROR, handleRecognitionError);
        recognitionService.stopRec();
    }

    private function handleRecognitionError(event : PronunciationServiceEvent) : void
    {
        recognitionService.removeEventListener(PronunciationServiceEvent.RECOGNITION, handleRecognized);
        recognitionService.removeEventListener(PronunciationServiceEvent.RECOGNITION_ERROR, handleRecognitionError);
        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CLOSE, _pendingMessage));

        var aMessage : String = errorGet("games.pronunciation.error"+event.data.error.ErrorCode);
        if(aMessage == "")
        {
            aMessage = errorGet("games.pronunciation.error-9000");
        }
        const message : MessageModel = MessageModel.buildForNotification(localeGet("error"), aMessage, localeGet("ok"));
        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CREATE_MESSAGE, message));
        dispatcher.dispatchEvent(new Event(GameEvent.RECOGNITION_INCOME));
    }

    private function handleRecognized(event : PronunciationServiceEvent) : void
    {
        recognitionService.removeEventListener(PronunciationServiceEvent.RECOGNITION, handleRecognized);
        recognitionService.removeEventListener(PronunciationServiceEvent.RECOGNITION_ERROR, handleRecognitionError);
        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CLOSE, _pendingMessage));
        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CLOSE, _pendingMessage));

        const sentence : Sentence =  event.data as Sentence;

        _dataRecognition = event.data as Sentence;

        var selectWordObject : Object = libraryWords[selectedWords[_selectIndexCube]];

        for each (var word : Word in sentence.words)
        {
            for each (var grapheme : Grapheme in word.Graphemes)
            {
                for each (var phoneme : Phoneme in grapheme.Phonemes)
                {
                    if (phoneme.name == selectWordObject["phoneme"])
                    {
                        if (phoneme.improvement != Phoneme.WORSE)
                        {
                            _recognitionMessage = MessageModel.buildForTutorial(localeGet("game3s.pronunciation.good"));
                        } else
                        {
                            _recognitionMessage = MessageModel.buildForTutorial(localeGet("game3s.pronunciation.bad"));
                        }
                        _recognitionMessage.contentText += phoneme.name + " : " + int(phoneme.score * 10)/10 + " score\n";
                    }
                }
            }
        }

        _recognitionMessage.contentText += "Response delay: " + event.delay + "ms\n";

        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CREATE_MESSAGE, _recognitionMessage));
        dispatcher.dispatchEvent(new Event(GameEvent.RECOGNITION_INCOME, false, sentence));
    }

    public function handleRecognitionIncome(e : Event) : void
    {
        dispatcher.removeEventListener(GameEvent.RECOGNITION_INCOME, handleRecognitionIncome);
        _view.enableTouch = true;
    }


    public function goTimer(inDelay : Number, callBack : Function) : void
    {
        var aTimer:Timer = new Timer(inDelay * 1000, 1);
        aTimer.addEventListener(TimerEvent.TIMER, callBack);
        aTimer.start();
    }

    private function set state(value : String) : void
    {
        _currentState = value;
        switch (_currentState)
        {
            case INTRO:
                _view.setClock(_startTimer);
                _view.setScore(0);
                _view.startIntro();
                break;
            case WAIT_TURN:
                waitMessageModel = MessageModel.buildForTutorial(localeGet("game3s.cubicle.facedown.touch"));

                _view.enableTouch = true;
                startTimer();
                break;
        }
    }

    private function startTimer() : void
    {
        _startTimer = getTimer();
        _view.addEventListener(Event.ENTER_FRAME, handleEnterFrame);
    }

    private function handleEnterFrame() : void
    {
        if(!_isFinishTime)
        {
            _view.setClock(getTimer() - _startTimer);
        }else{
            var aTime : Number = getGameTime();
            if(aTime <= 0)
            {
                aTime = 0;
                endGame(true);
            }
            _view.setClock(aTime);
        }
    }

    private function getGameTime() : Number
    {
        return _timer - (getTimer() - _startTimer);
    }

    private function endGame(timeIsOver : Boolean) : void
    {
        if(_isEnd) return;
        _isEnd = true;

        var timeSecMin : Array = Utils.secondsToSeparateSecondsAndMinutesArray(getGameTime());

        _gameOverMessage = timeIsOver
                ? MessageModel.buildForTutorial(localeGet("game3s.gametime.end",[_wordWin.length, _model.score]))
                : MessageModel.buildForTutorial(localeGet("game3s.allpairs.found",
                    [timeSecMin[0], Utils.getNumberPostfix(timeSecMin[0]), timeSecMin[1], Utils.getNumberPostfix(timeSecMin[1]), _model.score]));
        dispatcher.dispatchEvent(new MessageEvent(MessageEvent.CREATE_MESSAGE, _gameOverMessage));

        var sd:NetConnector
    }

    private function randomChooseWords() : void
    {
        // get need card word
        var aIndex : int;
        var aSortArr : Array = [];
        var aFilteredArr : Array = [];
        libraryWords = assetController.assetsManager.getObject("Game3Map")["Game_3"];
        var count : int = COUNT_CUBE_TYPES[_typeGame];

        // Add words into array
        for(aIndex = 0; aIndex < libraryWords.length; aIndex++)
        {
            aSortArr.push(aIndex);
        }

        // randomize selection
        for(aIndex = 0; aIndex < count / 2; aIndex++)
        {
            var index : int = int(aSortArr.length / 2 * Math.random()) * 2;
            aFilteredArr.push(aSortArr[index]);
            aFilteredArr.push(aSortArr[index + 1]);
            trace(aSortArr[index], aSortArr[index + 1]);
            aSortArr.splice(index, 2);
        }

        // get random words
        for(aIndex = 0; aIndex < count; aIndex++)
        {
            var index : int = int(aFilteredArr.length * Math.random());
            selectedWords.push(aFilteredArr[index]);
            aFilteredArr.splice(index, 1);
        }

    }
}
}
