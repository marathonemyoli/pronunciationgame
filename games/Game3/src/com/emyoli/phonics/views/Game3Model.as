/**
 * Created by alekseykabanov on 08.09.15.
 */
package com.emyoli.phonics.views {
public class Game3Model {
    private var _score : int = 0;
    public function Game3Model()
    {
    }

    public function get score() : int
    {
        return _score;
    }

    public function addScore(value : int) : void
    {
        _score += value;
    }
}
}
