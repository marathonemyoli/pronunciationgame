package com.emyoli.phonics.views {
import com.emyoli.phonics.components.GameCube;
import com.emyoli.phonics.components.GameCubeContainer;
import com.emyoli.phonics.components.MapsCube;
import com.emyoli.phonics.components.avatar.AvatarCarousel;
import com.emyoli.phonics.models.Game3Settings;
import com.emyoli.phonics.models.ProportionsScaleMode;
import com.emyoli.phonics.net.UserInfo;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.utils.Utils;
import com.emyoli.phonics.utils.localeGet;

import flash.events.TimerEvent;

import flash.geom.Rectangle;

import flash.geom.Vector3D;
import flash.utils.Timer;

import starling.core.Starling;

import starling.display.DisplayObject;
import starling.display.Image;

import starling.display.Quad;
import starling.display.Sprite;

import starling.display.Sprite3D;
import starling.events.Event;
import starling.events.KeyboardEvent;
import starling.text.TextField;

public class Game3View extends BaseView{

    public static const STATE_READY_TO_INTRO : String = "STATE_READY_TO_INTRO";
    public static const STATE_READY_TO_GAME : String = "STATE_READY_TO_GAME";
    public static const STATE_OPEN_WORD : String = "STATE_OPEN_WORD";
    public static const STATE_SAY_WORD : String = "STATE_SAY_WORD";
    public static const STATE_CLOSE_WORD : String = "STATE_CLOSE_WORD";
    public static const ROLL_CUBE : String = "ROLL_CUBE";
    public static const CLOSE_CUBE : String = "CLOSE_CUBE";
    public static const TOUCH_CUBE : String = "TOUCH_CUBE";

	private const ATLAS_ID : String = "game3";

    [Inject]
    public var pm : Game3ViewPM;


    private var _scoreTF : TextField = null;
    private var _clockTF : TextField = null;

    private var _map : MapsCube = null;

    private var _timer : Timer = new Timer(500, 1);

    private var _enableTouch : Boolean = false;
    var _rollArrow : Sprite;

    var _nowTimer : int = -1;

    public function Game3View()
	{
	}

	[PostConstruct]
	public override function init() : void
	{
		_proportionsScaleMode = ProportionsScaleMode.MIDDLE;
		super.init();

        pm.init(this);

		parseTextures(ATLAS_ID);

        _headerText.text = localeGet("game3s.header");

        _clockTF = getTextField("clock");
        _scoreTF = getTextField("score");

        var aBackGround : Sprite = getSprite("cave_back 1.7");
        var aToolBar : Sprite = getSprite("toolbar_back");
        var aClockIcon : Sprite = getSprite("clock_icon");
        var aScoreIcon : Sprite = getSprite("score_icon");
        _rollArrow = getSprite("rollArrow");

        _scoreTF.text = "9999";
        _clockTF.text = "9999";

        // test avatar--------
        var testUsers : Vector.<UserInfo> = new Vector.<UserInfo>();
        var testUser : UserInfo = new UserInfo();
        testUser.avatarname = "Avatar1";
        testUser.nickname = "Alex";
        for(var a:int=0;a<6;a++)
            testUsers.push(testUser);
        // test avatar--------

        var _avatarCarousel : AvatarCarousel = new AvatarCarousel(testUsers);

        _map = new MapsCube((data as Game3Settings).cardsIndex, pm.selectedWords, pm.libraryWords);
        _map.addEventListener(GameCubeContainer.OPENED, function (e : Event){
            enableTouch = true;
            pm.stateView(STATE_OPEN_WORD, e.data);
        });
        _map.addEventListener(GameCubeContainer.CLOSED, function (e : Event){
            enableTouch = true;
            pm.stateView(STATE_CLOSE_WORD, e.data);
            _rollArrow.visible = false;
        });
        _map.addEventListener(GameCubeContainer.SAY, function (e : Event){
            pm.stateView(STATE_SAY_WORD, e.data);
        });
        _map.addEventListener(GameCubeContainer.TOUCH, function (e : Event){
            enableTouch = false;
            _rollArrow.visible = true;
            _rollArrow.x = 88 + e.data.x;
            _rollArrow.y = 272 + e.data.y;
            pm.stateView(TOUCH_CUBE, e.data);
        });
        _map.addEventListener(GameCubeContainer.CLOSE_CUBE, function (e : Event){
            pm.stateView(CLOSE_CUBE, e.data);
        });
        _map.addEventListener(GameCubeContainer.ROLL_CUBE, function (e : Event){
            pm.stateView(ROLL_CUBE, e.data);
        });

        var arrBG:Array = [aBackGround];
        var arrCL:Array = [_map];
        var arrUP:Array = [aToolBar, aClockIcon, aScoreIcon, _clockTF, _scoreTF, _avatarCarousel, _rollArrow];
        var arrIW:Array = [aBackGround, aToolBar];
        var arrJX:Array = [aClockIcon, aScoreIcon, _clockTF, _scoreTF];

        for each(var aDO : DisplayObject in arrBG)
        {
            _bgLayer.addChild(aDO);
        }
        for each(var aDO : DisplayObject in arrUP)
        {
            _upperLayer.addChild(aDO);
        }
        for each(var aDO : DisplayObject in arrCL)
        {
            _contentLayer.addChild(aDO);
        }
        for each(var aDO : DisplayObject in arrIW)
        {
            Utils.proportionsRelativeIncreaseWidth(aDO);
        }
        for each(var aDO : DisplayObject in arrJX)
        {
            Utils.proportionsRelativeSetJustifyX(aDO);
        }

        Utils.proportionsRelativeMoveCenterX(_avatarCarousel, true);

        getSprite("rollArrow").visible = false;

		_divider.visible = false;

        _timer.addEventListener(TimerEvent.TIMER, handleEnterFrame);
        _timer.start();
    }

    public function setClock(inTimer : Number) : void
    {
        if(_nowTimer != inTimer/1000)
        {
            _nowTimer = inTimer/1000;
            _clockTF.text = Utils.secondsToTimecode(_nowTimer);
        }
    }

    public function setScore(inScore : Number) : void
    {
        _scoreTF.text = ""+inScore;
    }


    private function handleEnterFrame(event : TimerEvent) : void
    {
        _timer.removeEventListener(TimerEvent.TIMER, handleEnterFrame);
        _timer = null;
        pm.stateView(STATE_READY_TO_INTRO);
    }

    public function startIntro() : void
    {
        var delayIntro : Number = 1;
        _map.startIntro(delayIntro);
        _timer = new Timer(delayIntro, 1);
        _timer.addEventListener(TimerEvent.TIMER, function(event : TimerEvent){
            pm.stateView(STATE_READY_TO_GAME);
        });
        _timer.start();
    }

    public function set enableTouch(inValue : Boolean) : void
    {
        _enableTouch = inValue;
        _map.enableTouch = inValue;
    }

    public function winCube(inIndexCube : int) : void
    {
        _map.winCube(inIndexCube);
    }

    public function removeCube(inIndexCube : int) : void
    {
        _rollArrow.visible = false;
        _map.removeCube(inIndexCube);
    }

    public function loseCube(inIndexCube : int) : void
    {
        _map.loseCube(inIndexCube);
    }
}
}
